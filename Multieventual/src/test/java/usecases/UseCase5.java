package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.UserService;
import utilities.AbstractTest;

/**
 * 
 * @author Student
 * Caso de prueba para el caso de uso siguiente:
 * Buscar usuarios por nombre, apellidos y/o direcci�n.
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase5 extends AbstractTest {
	
	// Constructor ------------------------------------
	
	public UseCase5() {
		super();
	}
	
	// Services ----------------------------------------
	
	@Autowired
	private UserService userService;
	
	// Template ----------------------------------------
	
	private void templateSearchUser(String name, String surname, String address, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			userService.getUsersBy(testName, surname, address);
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -------------------------------------------
	
	@Test
	public void testSearch() {
		templateSearchUser("John", "Cena", "", null, "searchUser");
		templateSearchUser("Ignacio", "Al�s", "45243", null, "searchUser");
	}

}
