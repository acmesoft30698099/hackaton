package usecases;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Event;
import domain.utils.EventStatus;

import services.EventService;
import services.UserService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * 
 * @author Student Caso de prueba para el caso de uso siguiente: Hacer lo mismo
 *         que un actor que no est� registrado, excepto por la opci�n de poder
 *         registrarse en el sistema.
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase6 extends AbstractTest {

	// Constructor ------------------------------------

	public UseCase6() {
		super();
	}

	// Services ----------------------------------------

	@Autowired
	private UserService userService;

	@Autowired
	private EventService eventService;

	@Autowired
	private ServicesUtils servicesUtils;

	// Template ----------------------------------------

	private void templateSearchUser(String username, String name, String surname,
			String address, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			userService.getUsersBy(testName, surname, address);
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	private void templateSearchEvent(String username, String keyword, String address,
			Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			Collection<Event> events = eventService
					.getEventsByKeywordAndAddress(keyword, address);
			for (Event event : events) {
				if (keyword != null && address == null) {
					Assert.isTrue(event.getName().toUpperCase()
							.contains(keyword.toUpperCase())
							|| event.getDescription().toUpperCase()
									.contains(keyword.toUpperCase()));
				} else if (address != null && keyword == null) {
					Assert.isTrue(event.getCelebrationAddress().toUpperCase()
							.contains(address.toUpperCase()));
				} else {
					Assert.isTrue((event.getName().toUpperCase()
							.contains(keyword.toUpperCase()) || event
							.getDescription().toUpperCase()
							.contains(keyword.toUpperCase()))
							&& event.getCelebrationAddress().toUpperCase()
									.contains(address.toUpperCase()));
				}
			}
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	private void templateListUsers(String username, Class<?> expected,
			String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			userService.findAll();
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	private void templateListFinished(String username, String category, Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			Collection<Event> events;
			if (category != null) {
				events = eventService.getFinishedEventsWithCategory(category);
			} else {
				events = eventService.getFinishedEvents();
			}

			for (Event event : events) {
				Assert.isTrue(servicesUtils.getEventStatus(event) == EventStatus.ENDED);
			}
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, "listFinished");
	}

	private void templateListProgress(String username, String category, Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			Collection<Event> events;
			if (category != null) {
				events = eventService.getInProgressEventsWithCategory(category);
			} else {
				events = eventService.getInProgressEvents();
			}

			for (Event event : events) {
				Assert.isTrue(servicesUtils.getEventStatus(event) == EventStatus.IN_PROGRESS);
			}
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, "listProgress");
	}

	private void templateListSale(String username, String category, Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			Collection<Event> events;
			if (category != null) {
				events = eventService.getSaleEventsWithCategory(category);
			} else {
				events = eventService.getSaleEvents();
			}

			for (Event event : events) {
				Assert.isTrue(servicesUtils.getEventStatus(event) == EventStatus.PROMOTIONAL
						|| servicesUtils.getEventStatus(event) == EventStatus.SALE);
			}
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, "listSale");
	}

	// Tests -------------------------------------------

	@Test
	public void testSearchUser() {
		templateSearchUser("user2", "Ignacio", "Al�s", "", null, "searchUser");
		templateSearchUser("user2", "Ignacio", "Al�s", "45243", null, "searchUser");
	}

	@Test
	public void testSearchEvent() {
		// templateSearchEvent(null, null, null,"searchEvent1");
		// templateSearchEvent("Name", null, null, "searchEvent2");
		// templateSearchEvent(null, "Desc", null, "searchEvent3");
		// templateSearchEvent("Name", "Desc", null, "searchEvent4");
		// templateSearchEvent("A", null, null, "searchEvent5");
		templateSearchEvent("user2", "a", "a", null, "searchEvent6");
	}

	@Test
	public void testList() {
		templateListUsers("user2", null, "listCorrect");
	}

	// 1. Test correcto de listado de eventos finalizados
	@Test
	public void testFinished() {
		templateListFinished("user2", null, null);
		templateListFinished("user2", "Cat", null);
	}

	// 2. Test correcto de listado de eventos en progreso
	@Test
	public void testInProgress() {
		templateListProgress("user2", null, null);
		templateListProgress("user2", "Cat", null);
	}

	// 3. Test correcto de listado de eventos en promoci�n o venta
	@Test
	public void testSale() {
		templateListSale("user2", null, null);
		templateListSale("user2", "Cat", null);
	}

}
