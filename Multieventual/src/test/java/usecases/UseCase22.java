package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.CategoryService;
import utilities.AbstractTest;
import domain.Category;
import forms.CategoryForm;

/**
 * 
 * @author Student
 * 
 * Caso de prueba correspondiente con el siguiente caso de uso:
 * Gestionar las categor�as de eventos, lo cual incluye listarlas, crearlas, editarlas
y eliminarlas. Una categor�a se puede eliminar siempre y cuando no haya
eventos en ella.
 *
 */

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase22 extends AbstractTest {
	
	// Constructor ---------------------------------
	
	public UseCase22() {
		super();
	}
	
	// Services ------------------------------------
	
	@Autowired
	private CategoryService categoryService;
	
	// Template ------------------------------------
	
	private void templateSaveCategory(String username, CategoryForm categoryForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Category category = categoryService.reconstruct(categoryForm, null);
			categoryService.save(category);
			categoryService.flush();
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	private void templateDeleteCategory(String username, Integer categoryId, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Category category = categoryService.findOne(categoryId);
			categoryService.delete(category);
			categoryService.flush();
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}	
	
	// Tests ----------------------------------------
	
	// 1. Test correcto de guardado de categor�a ----
	
	@Test
	public void testSaveCorrect() {
		CategoryForm categoryForm = new CategoryForm();
		categoryForm.setDescription("Description");
		categoryForm.setName("Name");
		
		templateSaveCategory("admin", categoryForm, null, "saveCorrect");
	}
	
	// 2. Test incorrect de eliminaci�n de categor�a, categor�a con eventos

	@Test
	public void testDeleteIncorrect() {
		templateDeleteCategory("admin", 67, IllegalArgumentException.class, "deleteIncorrect");
	}
}
