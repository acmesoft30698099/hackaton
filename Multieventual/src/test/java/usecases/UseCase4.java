package usecases;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Event;

import services.EventService;
import utilities.AbstractTest;

/**
 * 
 * @author Student
 * Caso de prueba para el caso de uso siguiente:
 * Buscar eventos usando una palabra clave que debe aparecer en el nombre o
	descripci�n. Tambi�n se pueden buscar por la direcci�n donde se celebrar�.
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase4 extends AbstractTest {
	
	// Constructor ------------------------------------
	
	public UseCase4() {
		super();
	}
	
	// Services ----------------------------------------
	
	@Autowired
	private EventService eventService;
	
	// Template ----------------------------------------
	
	private void templateSearchEvent(String keyword, String address, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			Collection<Event> events = eventService.getEventsByKeywordAndAddress(keyword, address);
			for(Event event : events) {
				if(keyword != null && address == null) {
					Assert.isTrue(event.getName().toUpperCase().contains(keyword.toUpperCase()) ||
							event.getDescription().toUpperCase().contains(keyword.toUpperCase()));
				} else if(address != null && keyword == null) {
					Assert.isTrue(event.getCelebrationAddress().toUpperCase().contains(address.toUpperCase()));
				} else {
					Assert.isTrue((event.getName().toUpperCase().contains(keyword.toUpperCase()) ||
							event.getDescription().toUpperCase().contains(keyword.toUpperCase())) && event.getCelebrationAddress().toUpperCase().contains(address.toUpperCase()));
				}
			}
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -------------------------------------------
	
	@Test
	public void testSearch() {
//		templateSearchEvent(null, null, null,"searchEvent1");
//		templateSearchEvent("Name", null, null, "searchEvent2");
//		templateSearchEvent(null, "Desc", null, "searchEvent3");
//		templateSearchEvent("Name", "Desc", null, "searchEvent4");
//		templateSearchEvent("A", null, null, "searchEvent5");
		templateSearchEvent("a", "a", null, "searchEvent6");
	}

}
