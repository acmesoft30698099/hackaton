package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ActorService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Actor;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Visualizar su lista de amigos, la cual se compone de los actores que han
aceptado las solicitudes de amistad enviadas por �l o ella, y de los actores de
los cuales ha aceptado sus solicitudes.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase11 extends AbstractTest {
	
	// Constructor ---------------------------------------
	
	public UseCase11() {
		super();
	}
	
	// Services ------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Template ------------------------------------------
	
	private void templateSaveRequest(String username, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Actor actor = servicesUtils.checkUser();
			//Obtenemos la lista de amigos
			actorService.getFriends(actor.getId());
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------------------
	
	// 1. Test correcto de listado de amigos 
	
	@Test
	public void testSaveCorrect() {
		templateSaveRequest("user3", null, "saveCorrect");
	}
	
}
