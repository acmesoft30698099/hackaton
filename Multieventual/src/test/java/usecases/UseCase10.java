package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.FriendshipRequest;

import forms.FriendshipRequestForm;

import services.ActorService;
import services.FriendshipRequestService;
import utilities.AbstractTest;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Mandar una solicitud de amistad a otro actor del sistema. Cada solicitud tendrá
	un estado de "pendiente", "aceptada" o "rechazada", dependiendo de lo que
	decida el actor que la reciba. Si el actor tiene configurado que no quiere recibir
	solicitudes de amistad, todas las que le lleguen pasarán al estado de
	"rechazada" automáticamente.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase10 extends AbstractTest {
	
	// Constructor ---------------------------------------
	
	public UseCase10() {
		super();
	}
	
	// Services ------------------------------------------
	
	@Autowired
	private FriendshipRequestService friendshipRequestService;
	
	@Autowired
	private ActorService actorService;
	
	// Template ------------------------------------------
	
	private void templateSaveRequest(String username, FriendshipRequestForm form, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			FriendshipRequest friendship = friendshipRequestService.reconstruct(form, null);
			friendshipRequestService.save(friendship);
			friendshipRequestService.flush();
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------------------
	
	// 1. Test correcto de guardado de petición de amistad 
	
	@Test
	public void testSaveCorrect() {
		FriendshipRequestForm form = friendshipRequestService.construct(friendshipRequestService.create());
		form.setReceiver(actorService.findById(15));
		form.setText("Text");
		
		templateSaveRequest("user1", form, null, "saveCorrect");
	}

	// 2. Test incorrecto de guardado de petición de amistad, ya enviada
	
	@Test
	public void testSaveIncorrect() {
		FriendshipRequestForm form = friendshipRequestService.construct(friendshipRequestService.create());
		form.setReceiver(actorService.findOne(17));
		form.setText("Text");
		
		templateSaveRequest("user1", form, IllegalArgumentException.class, "saveIncorrect");
	}
	
}
