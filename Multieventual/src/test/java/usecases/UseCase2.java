package usecases;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Event;
import domain.utils.EventStatus;

import services.EventService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * 
 * @author Student
 * 
 * Caso de prueba correspondiente con el siguiente caso de uso:
 * Visualizar los eventos del sistema y acceder a la informaci�n de los mismos. Se
	deben ofrecer tres listas de eventos distintas: una en la que aparezcan los
	eventos que se encuentran en estado "Finalizado", otra en la que aparezcan los
	que se encuentran en estado "Promocional" o "Venta", y otra con los que est�n
	en estado "En Progreso". Adem�s, se debe poder filtrar por categor�as en cada
	una de ellas.
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase2 extends AbstractTest {
	
	// Constructor -----------------------------------
	
	public UseCase2() {
		super();
	}
	
	// Services --------------------------------------
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Templates -------------------------------------
	
	private void templateListFinished(String category, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			Collection<Event> events;
			if(category != null) {
				events = eventService.getFinishedEventsWithCategory(category);
			} else {
				events = eventService.getFinishedEvents();
			}
			
			for(Event event : events) {
				Assert.isTrue(servicesUtils.getEventStatus(event) == EventStatus.ENDED);
			}
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, "listFinished");
	}
	
	private void templateListProgress(String category, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			Collection<Event> events;
			if(category != null) {
				events = eventService.getInProgressEventsWithCategory(category);
			} else {
				events = eventService.getInProgressEvents();
			}
			
			for(Event event : events) {
				Assert.isTrue(servicesUtils.getEventStatus(event) == EventStatus.IN_PROGRESS);
			}
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, "listProgress");
	}
	
	private void templateListSale(String category, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			Collection<Event> events;
			if(category != null) {
				events = eventService.getSaleEventsWithCategory(category);
			} else {
				events = eventService.getSaleEvents();
			}
			
			for(Event event : events) {
				Assert.isTrue(servicesUtils.getEventStatus(event) == EventStatus.PROMOTIONAL || servicesUtils.getEventStatus(event) == EventStatus.SALE);
			}
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, "listSale");
	}
	
	// Tests ----------------------------------------
	
	// 1. Test correcto de listado de eventos finalizados
	@Test
	public void testFinished() {
		templateListFinished(null, null);
		templateListFinished("Cat", null);
	}
	
	// 2. Test correcto de listado de eventos en progreso
	@Test
	public void testInProgress() {
		templateListProgress(null, null);
		templateListProgress("Cat", null);
	}
	
	// 3. Test correcto de listado de eventos en promoci�n o venta
	@Test
	public void testSale() {
		templateListSale(null, null);
		templateListSale("Cat", null);
	}
}
