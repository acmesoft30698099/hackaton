package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Actor;
import domain.User;

import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Registrarse en el sistema como usuario o patrocinador.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase1 extends AbstractTest {
	
	// Constructor ---------------------------------
	
	public UseCase1() {
		super();
	}
	
	// Services ------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Template ------------------------------------
	
	private void templateLogin(String username, Class<? extends Actor> actor, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			servicesUtils.getRegisteredUser(actor);
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------------
	
	// 1. Test correcto de inicio de sesi�n
	
	@Test
	public void testLoginCorrect() {
		templateLogin("user2", User.class, null, "loginCorrect");
	}
	
	// 2. Test incorrecto de inicio de sesi�n, usuario no v�lido
	
	@Test
	public void testLoginIncorrect() {
		templateLogin("user1238523", User.class, IllegalArgumentException.class, "loginIncorrect");
	}	

}
