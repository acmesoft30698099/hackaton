package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;
import forms.UserAccountForm;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Editar su nombre de usuario y/o contrase�a.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase7 extends AbstractTest {
	
	// Constructor ---------------------------------

	public UseCase7() {
		super();
	}
	
	// Services ------------------------------------
	
	@Autowired
	private UserAccountService userAccountService;
	
	// Template ------------------------------------
	
	private void templateSaveConfiguration(String username, UserAccountForm uaForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			UserAccount ua = userAccountService.reconstruct(uaForm, null);
			ua = userAccountService.save(ua);
			userAccountService.flush();
			super.authenticate(null);
		}
		catch (Throwable t) {
			caught = t.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Test -------------------------------------

	// 1. Test correcto de edici�n de username y password
	
	@Test
	public void testEditCorrect() {
		UserAccount ua = userAccountService.findOne(2);
		UserAccountForm uaForm = userAccountService.construct(ua);
		uaForm.setUsername("manolo");
		uaForm.setPassword("manolete");
		uaForm.setRepeatPassword("manolete");
		
		templateSaveConfiguration("user1", uaForm, null, "editCorrect");
	}
	
	// 2. Test incorrecto de edici�n de username y password, ambos vac�os

	@Test
	public void testEditIncorrect() {
		UserAccount ua = userAccountService.findOne(2);
		UserAccountForm uaForm = userAccountService.construct(ua);
		uaForm.setUsername("");
		uaForm.setPassword("");
		uaForm.setRepeatPassword("");
		
		templateSaveConfiguration("user1", uaForm, NullPointerException.class, "editCorrect");
	}

}
