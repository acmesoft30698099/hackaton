package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ActorService;
import services.CreditCardService;
import utilities.AbstractTest;
import domain.CreditCard;
import domain.Sponsor;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 * Gestionar su tarjeta de cr�dito, lo cual incluye registrarla, editarla y
 * eliminarla. (Sponsor)
 * 
 * @author Student
 * 
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase20 extends AbstractTest {

	// Constructor -------------------

	public UseCase20() {
		super();
	}

	// Services ----------------------------

	@Autowired
	private CreditCardService creditCardService;
	
	@Autowired
	private ActorService actorService;

	// Templates ---------------------------

	private void messageSaveTemplate(String username, String title,
			CreditCard creditCard, Class<?> expected) {
		Class<?> caught = null;

		try {
			authenticate(username);
			// Ejecuta el guardado
			creditCardService.save(creditCard);
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, title);
	}

	private void messageDeleteTemplate(String username, String title,
			Class<?> expected) {
		Class<?> caught = null;

		try {
			authenticate(username);
			// Ejecuta el eliminado
			creditCardService.delete();
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, title);
	}

	// Tests -------------------------------

	// 1. Creaci�n correcta de creditCard
	@Test
	public void testCaseCreateCorrect() {
		messageDeleteTemplate("sponsor1", "validTest", null);
		CreditCard creditCard = creditCardService.create();
		creditCard.setBrand("VISA");
		creditCard.setCvv(987);
		creditCard.setExpirationMonth(12);
		creditCard.setExpirationYear(2020);
		creditCard.setHolder("Holder7");
		creditCard.setNumber("12345678903");

		messageSaveTemplate("sponsor1", "validTest", creditCard, null);
	}

	// 2. Creaci�n incorrecta de creditCard, valores incorrectos
	@Test
	public void testCaseCreateIncorrect() {
		messageDeleteTemplate("sponsor1", "validTest", null);
		CreditCard creditCard = creditCardService.create();
		creditCard.setBrand("asdsad");
		creditCard.setCvv(987);
		creditCard.setExpirationMonth(12);
		creditCard.setExpirationYear(2010);
		creditCard.setHolder("Holder7");
		creditCard.setNumber("12345678905");

		messageSaveTemplate("sponsor1", "invalidTest", creditCard, IllegalArgumentException.class);
	}

	// 3. Edicion correcta de creditCard
	@Test
	public void testCaseEditCorrect() {
		Sponsor sponsor = (Sponsor) actorService.findById(23);
		CreditCard creditCard = sponsor.getCreditCard();
		creditCard.setBrand("VISA");
		creditCard.setCvv(987);
		creditCard.setExpirationMonth(12);
		creditCard.setExpirationYear(2020);
		creditCard.setHolder("Holder7");
		creditCard.setNumber("12345678903");

		messageSaveTemplate("sponsor1", "validTest", creditCard, null);
	}

	// 4. Edicion incorrecta de creditCard, datos incorrectos
	@Test
	public void testCaseEditIncorrect() {
		Sponsor sponsor = (Sponsor) actorService.findById(23);
		CreditCard creditCard = sponsor.getCreditCard();
		creditCard.setBrand("asdadsa");
		creditCard.setCvv(987);
		creditCard.setExpirationMonth(12);
		creditCard.setExpirationYear(2010);
		creditCard.setHolder("Holder7");
		creditCard.setNumber("12345678905");

		messageSaveTemplate("sponsor1", "invalidTest", creditCard, IllegalArgumentException.class);
	}

	// 5. Eliminaci�n correcta de creditCard.
	@Test
	public void testCaseDeleteCorrect() {
		messageDeleteTemplate("sponsor1", "validTest", null);
	}

}
