package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Comment;

import forms.CommentForm;

import services.CommentService;
import utilities.AbstractTest;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Editar el texto de contenido de un comentario o tema de conversación que el
	administrador considere inapropiado. Una vez un texto es editado por un
	administrador, este no puede volver a ser editado por el actor que lo publicó.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase24 extends AbstractTest {

	// Constructor -----------------------------
	
	public UseCase24() {
		super();
	}
	
	// Services --------------------------------
	
	@Autowired
	private CommentService commentService;
	
	// Template --------------------------------
	
	private void templateSaveComment(String username, CommentForm commentForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Comment comment = commentService.reconstruct(commentForm, null);
			comment = commentService.save(comment);
			commentService.flush();
			Assert.isTrue(comment.isModifiedByAdministrator());
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -------------------------------------
	
	// 1. Test correcto de modificación de comentario de usuario
	
	@Test
	public void testEditAdminCorrect() {
		CommentForm commentForm = commentService.construct(commentService.findOne(145));
		
		templateSaveComment("admin", commentForm, null, "editCorrect");
	}
	
	// 2. Test incorrecto de modificación de comentario de usuario, usuario creador no puede editarlo
	
	@Test
	public void testEditAdminInorrect() {
		CommentForm commentForm = commentService.construct(commentService.findOne(144));
		
		templateSaveComment("user2", commentForm, IllegalArgumentException.class, "editIncorrect");
	}	
}
