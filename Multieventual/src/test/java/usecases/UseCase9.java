package usecases;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ActorService;
import services.CommentService;
import services.EventService;
import services.TopicService;
import utilities.AbstractTest;
import domain.Event;
import domain.Topic;
import forms.CommentForm;
import forms.TopicForm;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Publicar temas de conversación en eventos o escribir comentarios en dichos
temas. También pueden editar los textos de contenido de los mismos
posteriormente, pero nunca eliminar el tema o comentario propiamente.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase9 extends AbstractTest {
	
	// Constructor ---------------------------------------
	
	public UseCase9() {
		super();
	}
	
	// Services ------------------------------------------
	
	@Autowired
	public TopicService topicService;
	
	@Autowired
	public CommentService commentService;
	
	@Autowired
	public EventService eventService;
	
	@Autowired
	public ActorService actorService;
	
	// Template ------------------------------------------
	
	private void templateCreateTopic(String username, TopicForm topicForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Topic topic = topicService.createReconstruct(topicForm, null);
			topicService.save(topic);
			topicService.flush();
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------------------
	
	// 1. Test correcto de guardado de tema de conversación 
	
	@Test
	public void testSaveCorrect() {
		TopicForm topicForm = new TopicForm();
		topicForm.setComments(new ArrayList<CommentForm>());
		topicForm.setTitle("Titulo");
		Event event = eventService.findOne(73);
		topicForm.setEvent(event);
		CommentForm commentForm = new CommentForm();
		commentForm.setContentText("Texto");
		topicForm.getComments().add(commentForm);
		
		templateCreateTopic("user1", topicForm, null, "saveCorrect");
	}

	// 2. Test incorrecto de guardado de tema de conversación, evento no publicado
	
	@Test
	public void testSaveIncorrect() {
		TopicForm topicForm = new TopicForm();
		topicForm.setComments(new ArrayList<CommentForm>());
		topicForm.setTitle("Titulo");
		Event event = eventService.findOne(74);
		topicForm.setEvent(event);
		CommentForm commentForm = new CommentForm();
		commentForm.setContentText("Texto");
		topicForm.getComments().add(commentForm); 
		
		templateCreateTopic("user1", topicForm, IllegalArgumentException.class, "saveIncorrect");
	}
	
}
