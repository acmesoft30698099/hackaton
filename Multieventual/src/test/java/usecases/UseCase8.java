package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ActorService;
import services.AdministratorService;
import services.SponsorService;
import services.UserService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Actor;
import domain.Administrator;
import domain.Sponsor;
import domain.User;
import forms.ActorConfigurationForm;
import forms.AdministratorForm;
import forms.SponsorForm;
import forms.UserForm;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Editar la informaci�n en su perfil.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase8 extends AbstractTest {
	
	// Constructor ---------------------------------

	public UseCase8() {
		super();
	}
	
	// Services ------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SponsorService sponsorService;
	
	@Autowired
	private AdministratorService administratorService;
	
	// Template ------------------------------------
	
	private void templateSaveConfiguration(String username, ActorConfigurationForm acForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Actor actor = servicesUtils.checkUser();
			if (actor instanceof User) {
				User user = userService.reconstruct(acForm, null);
				user = userService.save(user);
				userService.flush();
			}
			else if (actor instanceof Sponsor) {
				Sponsor sponsor = sponsorService.reconstruct(acForm, null);
				sponsor = sponsorService.save(sponsor);
				sponsorService.flush();
			}
			else if (actor instanceof Administrator) {
				Administrator admin = administratorService.reconstruct(acForm, null);
				admin = administratorService.save(admin);
				administratorService.flush();
			}
			super.authenticate(null);
		}
		catch (Throwable t) {
			caught = t.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	private void templateSaveUserProfile(String username, UserForm userForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			User user = userService.reconstruct(userForm, null);
			user = userService.save(user);
			userService.flush();
			super.authenticate(null);
		}
		catch (Throwable t) {
			caught = t.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	private void templateSaveUserProfile(String username, SponsorForm sponsorForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Sponsor sponsor = sponsorService.reconstruct(sponsorForm, null);
			sponsor = sponsorService.save(sponsor);
			sponsorService.flush();
			super.authenticate(null);
		}
		catch (Throwable t) {
			caught = t.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	private void templateSaveAdminProfile(String username, AdministratorForm adminForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Administrator admin = administratorService.reconstruct(adminForm, null);
			admin = administratorService.save(admin);
			administratorService.flush();
			super.authenticate(null);
		}
		catch (Throwable t) {
			caught = t.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Test -------------------------------------

	// 1. Test correcto de edici�n de configuraci�n de opciones de usuario
	
	@Test
	public void testUserConfigurationEditCorrect() {
		User user = userService.findOne(16);
		ActorConfigurationForm acForm = actorService.construct(user);
		acForm.setAllowFriendshipRequests("false");
		acForm.setMessagesConfiguration("NOBODY");
		
		templateSaveConfiguration("user1", acForm, null, "userConfigurationEditCorrect");
	}
	
	// 2. Test incorrecto de edici�n de configuraci�n de opciones de usuario, campos vac�os

	@Test
	public void testUserConfigurationEditIncorrect() {
		User user = userService.findOne(16);
		ActorConfigurationForm acForm = actorService.construct(user);
		acForm.setAllowFriendshipRequests("");
		acForm.setMessagesConfiguration("");
		
		templateSaveConfiguration("user1", acForm, NullPointerException.class, "userConfigurationEditIncorrect");
	}
	
	// 3. Test correcto de edici�n de configuraci�n de opciones de patrocinador

	@Test
	public void testSponsorConfigurationEditCorrect() {
		Sponsor sponsor = sponsorService.findOne(23);
		ActorConfigurationForm acForm = actorService.construct(sponsor);
		acForm.setAllowFriendshipRequests("false");
		acForm.setMessagesConfiguration("NOBODY");
		
		templateSaveConfiguration("sponsor1", acForm, null, "sponsorConfigurationEditCorrect");
	}
	
	// 4. Test incorrecto de edici�n de configuraci�n de opciones de patrocinador, campos vac�os

	@Test
	public void testSponsorConfigurationEditIncorrect() {
		Sponsor sponsor = sponsorService.findOne(23);
		ActorConfigurationForm acForm = actorService.construct(sponsor);
		acForm.setAllowFriendshipRequests("");
		acForm.setMessagesConfiguration("");
		
		templateSaveConfiguration("sponsor1", acForm, NullPointerException.class, "sponsorConfigurationEditIncorrect");
	}
	
	// 5. Test correcto de edici�n de configuraci�n de opciones de administrador

	@Test
	public void testAdministratorConfigurationEditCorrect() {
		Administrator admin = administratorService.findOne(15);
		ActorConfigurationForm acForm = actorService.construct(admin);
		acForm.setAllowFriendshipRequests("false");
		acForm.setMessagesConfiguration("NOBODY");
		
		templateSaveConfiguration("admin", acForm, null, "administratorConfigurationEditCorrect");
	}
	
	// 6. Test incorrecto de edici�n de configuraci�n de opciones de administrador, campos vac�os

	@Test
	public void testAdministratorConfigurationEditIncorrect() {
		Administrator admin = administratorService.findOne(15);
		ActorConfigurationForm acForm = actorService.construct(admin);
		acForm.setAllowFriendshipRequests("");
		acForm.setMessagesConfiguration("");
		
		templateSaveConfiguration("admin", acForm, NullPointerException.class, "administratorConfigurationEditIncorrect");
	}
	
	// 7. Test correcto de edici�n de perfil de usuario

	@Test
	public void testUserEditCorrect() {
		User user = userService.findOne(16);
		UserForm uForm = userService.construct(user);
		uForm.setName("pedro");
		uForm.setSurname("pedrosa");
		uForm.setPhone("");
		
		templateSaveUserProfile("user1", uForm, null, "userEditCorrect");
	}
	
	// 8. Test incorrecto de edici�n de perfil de usuario, nombre y apellidos en blanco

	@Test
	public void testUserEditIncorrect() {
		User user = userService.findOne(16);
		UserForm uForm = userService.construct(user);
		uForm.setName("");
		uForm.setSurname("");
		
		templateSaveUserProfile("user1", uForm, NullPointerException.class, "userEditIncorrect");
	}
	
	// 9. Test correcto de edici�n de perfil de patrocinador

	@Test
	public void testSponsorEditCorrect() {
		Sponsor sponsor = sponsorService.findOne(23);
		SponsorForm sForm = sponsorService.construct(sponsor);
		sForm.setName("pedro");
		sForm.setSurname("pedrosa");
		sForm.setPhone("");
		
		templateSaveUserProfile("sponsor1", sForm, null, "sponsorEditCorrect");
	}
	
	// 10. Test incorrecto de edici�n de perfil de patrocinador, nombre y apellidos en blanco

	@Test
	public void testSponsorEditIncorrect() {
		Sponsor sponsor = sponsorService.findOne(23);
		SponsorForm sForm = sponsorService.construct(sponsor);
		sForm.setName("");
		sForm.setSurname("");
		
		templateSaveUserProfile("sponsor1", sForm, NullPointerException.class, "sponsorEditIncorrect");
	}
	
	// 11. Test correcto de edici�n de perfil de administrador

	@Test
	public void testAdminEditCorrect() {
		Administrator admin = administratorService.findOne(15);
		AdministratorForm aForm = administratorService.construct(admin);
		aForm.setName("pedro");
		aForm.setSurname("pedrosa");
		aForm.setPhone("");
		
		templateSaveAdminProfile("admin", aForm, null, "adminEditCorrect");
	}
	
	// 12. Test incorrecto de edici�n de perfil de administrador, nombre y apellidos en blanco

	@Test
	public void testAdminEditIncorrect() {
		Administrator admin = administratorService.findOne(15);
		AdministratorForm aForm = administratorService.construct(admin);
		aForm.setName("");
		aForm.setSurname("");
		
		templateSaveAdminProfile("admin", aForm, NullPointerException.class, "adminEditIncorrect");
	}

}
