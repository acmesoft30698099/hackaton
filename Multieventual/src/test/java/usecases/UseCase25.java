package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.UserService;
import services.utils.BannedUserException;
import utilities.AbstractTest;
import domain.User;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Penalizar a un usuario cuando considere oportuno. A esto se le conoce como "aplicar un Ban".
 * Generalmente se aplica un Ban a un usuario que infringe las normas de la p�gina en repetidas
 * ocasiones. Esta acci�n hace que el usuario pierda la habilidad de crear eventos, publicar
 * temas de conversaci�n o escribir comentarios en los mismos. Tambi�n se le puede quitar esta
 * penalizaci�n posteriormente.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase25 extends AbstractTest {
	
	// Constructor ---------------------------------

	public UseCase25() {
		super();
	}
	
	// Services ------------------------------------
	
	@Autowired
	private UserService userService;
	
	// Test -------------------------------------

	// 1. Test correcto de eliminaci�n de baneado de usuario y permitir acceso
	
	@Test
	public void testUserBanAndAccessDeniedCorrect() {
		Class<?> caught = null;
		super.authenticate("admin");
		User user = userService.findOne(16);
		user.setBan(false);
		userService.save(user);
		userService.flush();
		super.authenticate(null);
		
		super.authenticate("user1");
		try {
			userService.prohibitBannedUser();
		}
		catch (Throwable t) {
			caught = t.getClass();
		}
		super.authenticate(null);
		
		checkExceptions(null, caught, "userBanAccessDeniedCorrect");
	}
	
	// 2. Test incorrecto de baneado de usuario e impedimento de acceso

	@Test
	public void testUserBanAndAccessDeniedIncorrect() {
		Class<?> caught = null;
		super.authenticate("admin");
		User user = userService.findOne(16);
		user.setBan(true);
		userService.save(user);
		userService.flush();
		super.authenticate(null);
		
		super.authenticate("user1");
		try {
			userService.prohibitBannedUser();
		}
		catch (Throwable t) {
			caught = t.getClass();
		}
		super.authenticate(null);
		
		checkExceptions(BannedUserException.class, caught, "userBanAccessDeniedIncorrect");
	}

}
