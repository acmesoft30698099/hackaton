package usecases;

import javax.transaction.Transactional;

import org.joda.time.format.DateTimeFormat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.CategoryService;
import services.EventService;
import services.TicketService;
import utilities.AbstractTest;
import domain.Category;
import domain.Event;
import domain.Ticket;
import forms.EventForm;
import forms.TicketForm;
import forms.TicketFormCreate;

/**
 * 
 * @author Student
 * Caso de prueba para el caso de uso siguiente:
 * Gestionar sus eventos, lo cual incluye listarlos, crearlos, editarlos y eliminarlos.
	Un evento solo se puede editar y/o eliminar mientras sea un "borrador", o se
	encuentre en estado "Promocional". La fecha de inicio y fin del evento deben
	ser posteriores a la fecha de publicaci�n. La fecha de inicio del plazo de
	inscripci�n debe estar comprendida entre el momento de publicarse el evento
	(inclusive) y el inicio del mismo.
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase16 extends AbstractTest {
	
	// Constructor ------------------------------
	
	public UseCase16() {
		super();
	}
	
	// Services ---------------------------------
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private TicketService ticketService;
	
	// Template ---------------------------------
	
	private void templateSaveEvent(String username, EventForm eventForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Event event = eventService.reconstruct(eventForm, null);
			event = eventService.save(event);
			eventService.flush();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	private void templateDeleteEvent(String username, EventForm eventForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Event event = eventService.reconstruct(eventForm, null);
			eventService.delete(event);
			eventService.flush();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	private void templateSaveTicket(String username, TicketForm ticketForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Ticket ticket = ticketService.reconstruct(ticketForm, null);
			ticket = ticketService.save(ticket);
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Test -------------------------------------
	
	// 1. Test correcto de creaci�n de eventos
	
	@Test
	public void testCreateCorrect() {
		EventForm eventForm = eventService.construct(eventService.create());
		Category category = categoryService.findOne(68);
		eventForm.setName("Name");
		eventForm.setCelebrationAddress("Address");
		eventForm.setDescription("Description");
		eventForm.setCategory(category);
		eventForm.getCelebrationCoordinate().setLatitude(2.2);
		eventForm.getCelebrationCoordinate().setLongitude(2.2);
		TicketFormCreate ticket = ticketService.constructCreate(ticketService.create());
		ticket.setDescription("Descr");
		ticket.setName("Name");
		ticket.setPrice(0.0);
		ticket.setRemainingUnits(50);
		eventForm.getTickets().clear();
		eventForm.getTickets().add(ticket);
		
		templateSaveEvent("user2", eventForm, null, "CreateCorrect1");
	}
	
	// 2. Test incorrecto de creaci�n de eventos, usuario inv�lido
	
	@Test
	public void testCreateInCorrect1() {
		EventForm eventForm = eventService.construct(eventService.create());
		Category category = categoryService.findOne(68);
		eventForm.setName("Name");
		eventForm.setCelebrationAddress("Address");
		eventForm.setDescription("Description");
		eventForm.setCategory(category);
		eventForm.getCelebrationCoordinate().setLatitude(2.2);
		eventForm.getCelebrationCoordinate().setLongitude(2.2);
		TicketFormCreate ticket = ticketService.constructCreate(ticketService.create());
		ticket.setDescription("Descr");
		ticket.setName("Name");
		ticket.setPrice(0.0);
		ticket.setRemainingUnits(50);
		eventForm.getTickets().clear();
		eventForm.getTickets().add(ticket);
		
		templateSaveEvent(null, eventForm, IllegalArgumentException.class, "CreateIncorrect2");
		templateSaveEvent("admin", eventForm, IllegalArgumentException.class, "CreateIncorrect3");
	}	

	// 3. Test incorrecto de creaci�n de eventos, fechas incorrectas
	
	@Test
	public void testCreateInCorrect2() {
		EventForm eventForm = eventService.construct(eventService.create());
		Category category = categoryService.findOne(68);
		eventForm.setName("Name");
		eventForm.setCelebrationAddress("Address");
		eventForm.setDescription("Description");
		eventForm.setCategory(category);
		eventForm.getCelebrationCoordinate().setLatitude(2.2);
		eventForm.getCelebrationCoordinate().setLongitude(2.2);
		eventForm.setPublicationDate(DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")
				.parseDateTime("10/02/2018 00:00").toDate());
		eventForm.setSaleStartDate(DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")
				.parseDateTime("15/02/2018 00:00").toDate());
		eventForm.setStartingDate(DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")
				.parseDateTime("13/02/2018 00:00").toDate());
		eventForm.setEndingDate(DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")
				.parseDateTime("25/02/2018 00:05").toDate());		
		TicketFormCreate ticket = ticketService.constructCreate(ticketService.create());
		ticket.setDescription("Descr");
		ticket.setName("Name");
		ticket.setPrice(0.0);
		ticket.setRemainingUnits(50);
		eventForm.getTickets().clear();
		eventForm.getTickets().add(ticket);
		// Se espera NullPointerException ya que intentar� poner un error en el BindingResult, que es null.
		templateSaveEvent("user2", eventForm, NullPointerException.class, "CreateIncorrect4");
	}
	
	// 4. Test inccorrecto de edici�n de evento, no modificable debido a la fecha de publicaci�n y venta.
	
	@Test
	public void testCorrectEdit() {
		Event event = eventService.findOne(75);
		EventForm eventForm = eventService.construct(event);
		
		eventForm.setCelebrationAddress("Different");
		
		templateSaveEvent("user2", eventForm, null, "editCorrect1");
	}
	
	// 5. Test incorrecto de edici�n de evento, usuario no poseedor del evento
	
	@Test
	public void testIncorrectEdit1() {
		Event event = eventService.findOne(80);
		EventForm eventForm = eventService.construct(event);
		
		eventForm.setCelebrationAddress("Different");
		
		templateSaveEvent("user3", eventForm, IllegalArgumentException.class, "editIncorrect1");
	}
	
	// 6. Test correcto de eliminaci�n de eventos
	
	@Test
	public void testCorrectRemove() {
		Event event = eventService.findOne(75);
		EventForm eventForm = eventService.construct(event);
		
		templateDeleteEvent("user2", eventForm, null, "removeCorrect");
	}
	
	// 7. Test incorrecto de eliminaci�n de eventos, usuario incorrecto
	
	@Test
	public void testIncorrectRemove1() {
		Event event = eventService.findOne(80);
		EventForm eventForm = eventService.construct(event);
		
		templateDeleteEvent("user3", eventForm, IllegalArgumentException.class, "removeIncorrect1");
	}
	
	// 8. Test incorrecto de eliminaci�n de eventos, modificaci�n imposible debido a la fecha del evento
	
	@Test
	public void testIncorrectRemove2() {
		Event event = eventService.findOne(80);
		EventForm eventForm = eventService.construct(event);
		
		templateDeleteEvent("user2", eventForm, IllegalArgumentException.class, "removeIncorrect2");
	}
	
	// 9. Test a�adido de entradas correcto
	
	@Test
	public void testCorrectTicket() {
		Event event = eventService.findOne(75);
		Ticket ticket = ticketService.create();
		ticket.setEvent(event);
		TicketForm ticketForm = ticketService.construct(ticket);
		
		ticketForm.setDescription("DEscr");
		ticketForm.setName("NAme");
		ticketForm.setPrice(0.0);
		ticketForm.setRemainingUnits(null);
		ticketForm.setEvent(event);
		
		templateSaveTicket("user2", ticketForm, null, "ticketCreateCorrect1");
	}
	
	// 9. Test a�adido de entradas incorrecto, evento no modificable
	
	@Test
	public void testIncorrectTicket() {
		Event event = eventService.findOne(80);
		Ticket ticket = ticketService.create();
		ticket.setEvent(event);
		TicketForm ticketForm = ticketService.construct(ticket);
		
		ticketForm.setDescription("DEscr");
		ticketForm.setName("NAme");
		ticketForm.setPrice(0.0);
		ticketForm.setRemainingUnits(null);
		ticketForm.setEvent(event);
		
		templateSaveTicket("user2", ticketForm, IllegalArgumentException.class, "ticketCreateIncorrect1");
	}	
}
