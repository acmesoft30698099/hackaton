package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Contract;
import domain.Event;

import forms.ContractForm;

import services.ContractService;
import services.EventService;
import utilities.AbstractTest;

/**
 * 
 * @author Student
 * Caso de prueba para el caso de uso siguiente:
 * Gestionar sus contratos, lo que incluye listarlos y crearlos (proponerlos). Los
	contratos solo se pueden proponer mientras el evento se encuentra en estado
	"Venta". Cada contrato tendr� un estado de "pendiente", "aceptado" o
	"rechazado", dependiendo de lo que decida el usuario que lo recibe. Un mismo
	patrocinador no puede proponer mas de un contrato para el mismo evento
	mientras haya otro en estado de "pendiente" o "aceptado". Si un contrato sigue
	en estado de "pendiente" y el evento pasa a estado "En Progreso", dicho
	contrato pasar� a estado "rechazado" autom�ticamente. Cuando un contrato es
	aceptado, en el perfil del evento debe aparecer el banner de la empresa del
	patrocinador y se procede al cargo del pago ofertado. Si la tarjeta de cr�dito del
	patrocinador no es v�lida, el contrato pasa a un nuevo estado llamado
	"pendiente de pago" y hasta que el patrocinador no solvente el problema no
	aparecer� el banner en el perfil del evento.
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase21 extends AbstractTest {
	
	// Constructor -----------------------------------------
	
	public UseCase21() {
		super();
	}
	
	// Services --------------------------------------------
	
	@Autowired
	private ContractService contractService;
	
	@Autowired
	private EventService eventService;
	
	// Template --------------------------------------------
	
	private void templateSaveContract(String username, ContractForm contractForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Contract contract = contractService.reconstruct(contractForm, null);
			contractService.save(contract);
			contractService.flush();
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -----------------------------------------------
	
	// 1. Test guardado correcto de contrato
	
	@Test
	public void testSaveContractCorrect() {
		ContractForm contractForm = contractService.construct(contractService.create());
		Event event = eventService.findOne(78);
		
		contractForm.setEvent(event);
		contractForm.setOfferedPayment(500.0);
		contractForm.setTermsAndConditions("Terms and conditions");
		
		templateSaveContract("sponsor5", contractForm, null, "testSaveCorrect");
	}
	
	// 2. Test guardado incorrecto de contrato, evento fuera de fase de venta
	
	@Test
	public void testSaveContractIncorrect() {
		ContractForm contractForm = contractService.construct(contractService.create());
		Event event = eventService.findOne(77);
		
		contractForm.setEvent(event);
		contractForm.setOfferedPayment(500.0);
		contractForm.setTermsAndConditions("Terms and conditions");
		
		templateSaveContract("sponsor4", contractForm, IllegalArgumentException.class, "testSaveIncorrect");
	}

}
