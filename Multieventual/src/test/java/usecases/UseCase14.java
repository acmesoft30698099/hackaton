package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.AlertService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Actor;
import domain.Alert;
import domain.utils.AlertStatus;

/**
 * 
 * @author Student Caso de prueba para el caso de uso siguiente: 
 	Gestionar sus
 alertas, lo cual incluye listarlas, marcarlas como le�das y
 eliminarlas.
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase14 extends AbstractTest{

	// Constructor ------------------------------

	public UseCase14() {
		super();
	}

	// Services ---------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;

	@Autowired
	private AlertService alertService;

	// Template ---------------------------------

	private void templateSaveAlert(String username, int id, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			Alert alert = alertService.findOneForEdit(id);
			alert.set_Read(true);
			alert = alertService.save(alert);
			alertService.flush();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	private void templateDeleteAlert(String username, Alert alert, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			alertService.delete(alert);
			alertService.flush();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	private void templateListAlert(String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			Actor actor = servicesUtils.checkUser();
			alertService.getAlertsRead(actor.getId());
			alertService.getAlertsUnRead(actor.getId());
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	private void templateCreateAlert(String username, AlertStatus alertStatus, String link, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			Actor actor = servicesUtils.checkUser();
			alertService.generateAlert(actor, alertStatus, link);
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	// Test -------------------------------------

	// 1. Test correcto de creaci�n de alerta

	@Test
	public void testCreateCorrect() {

		templateCreateAlert("user2", AlertStatus.MESSAGE_RECEIVED, null, null, "CreateCorrect");
	}

	// 2. Test correcto de listado de alerts

	@Test
	public void testListCorrect() {
		
		templateListAlert("user3", null, "ListCorrect");
	}

	// 3. Test correcto de guardado, establecer como leido

	@Test
	public void testSaveCorrect() {
		templateSaveAlert("user3", 61, null ,"SaveCorrect");
	}
	
	// 4. Test incorrecto de guardado, usuario diferente

	@Test
	public void testSaveIncorrect() {
		templateSaveAlert("user2", 61, IllegalArgumentException.class ,"SaveIncorrect");
	}

	// 5. Test correcto de eliminado

	@Test
	public void testDeleteCorrect() {
		Alert alert = alertService.findOne(60);
		templateDeleteAlert("user2", alert, null ,"DeleteCorrect");
	}
	
	// 5. Test incorrecto de eliminado, usuario diferente

	@Test
	public void testDeleteIncorrect() {
		Alert alert = alertService.findOne(65);
		templateDeleteAlert("user2", alert, IllegalArgumentException.class ,"DeleteIncorrect");
	}
}
