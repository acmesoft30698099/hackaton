package usecases;

import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Purchase;
import domain.User;

import services.ActorService;
import services.PurchaseService;
import services.TicketService;
import utilities.AbstractTest;

/**
 * @author Student
 * Caso de prueba para el caso de uso siguiente:
 * Adquirir una entrada para asistir a un evento. Un usuario solo puede realizar
	esta operaci�n mientras el evento se encuentra en estado "Venta". Una entrada
	que sea de unidades limitadas y est� agotada no puede ser adquirida
	posteriormente por ning�n usuario. Un usuario solo puede adquirir una entrada
	(de cualquier tipo) para un mismo evento. Adem�s, el usuario debe haber
	registrado previamente una tarjeta de cr�dito v�lida para poder hacerle el cargo
	de la compra (excepto cuando el precio de la entrada es cero, en cuyo caso no
	es necesario). Evidentemente, el creador de un evento no puede adquirir
	ninguna entrada para su propio evento.
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase18 extends AbstractTest {
	
	// Constructor -------------------------------------
	
	public UseCase18() {
		super();
	}
	
	// Services ----------------------------------------
	
	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private ActorService actorService;
	
	// Template ----------------------------------------
	
	private void templateSavePurchase(String username, Purchase purchase, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			purchaseService.save(purchase);
			purchaseService.flush();
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -------------------------------------------
	
	// 1. Test correcto de guardado de compra ----------
	
	@Test
	public void testSavePurchaseCorrect() {
		Purchase purchase = purchaseService.create();
		
		purchase.setPurchaseDate(new Date());
		purchase.setTicket(ticketService.findOne(98));
		purchase.setUser((User) actorService.findOne(22));
		
		templateSavePurchase("user7", purchase, null, "saveCorrect");
	}
	
	// 2. Test incorrecto de guardado de compra, comprar para otro usuario
	
	@Test
	public void testSavePurchaseIncorrect() {
		Purchase purchase = purchaseService.create();
		
		purchase.setPurchaseDate(new Date());
		purchase.setTicket(ticketService.findOne(93));
		purchase.setUser((User) actorService.findOne(20));
		
		templateSavePurchase("user3", purchase, IllegalArgumentException.class, "saveCorrect");
	}	
}
