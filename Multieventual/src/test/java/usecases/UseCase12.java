package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ActorService;
import services.MessageService;
import utilities.AbstractTest;
import domain.Actor;
import domain.Message;
import forms.MessageForm;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 	Intercambiar mensajes con otros actores del sistema. Un mensaje solo se
puede enviar si el actor receptor tiene en su configuraci�n que quiere recibir
mensajes de cualquier actor del sistema o que quiere recibir mensajes solo de
aquellos actores que est�n en su lista de amigos y el actor que ha enviado el
mensaje aparece en ella
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase12 extends AbstractTest {
	
	// Constructor -------------------------
	
	public UseCase12() {
		super();
	}
	
	// Services ----------------------------
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ActorService actorService;
	
	// Templates ---------------------------
	
	private void saveTemplate(String username, String title, MessageForm messageForm, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			authenticate(username);
			// El usuario accede al formulario de creaci�n de mensaje y introduce los datos.
			// Le da click a guardar, lo que ejecuta el m�todo reconstruct
			Message message = messageService.reconstruct(messageForm, null);
			// Ejecuta el guardado
			messageService.save(message);
			unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, title);
	}
	
	// Tests -------------------------------
	
	// 1. Creaci�n correcta de message.
	@Test
	public void testCaseSaveCorrect() {
		Message msg = messageService.create();
		MessageForm messageForm = messageService.construct(msg);
		Actor receiver = actorService.findById(19);
		
		messageForm.setTitle("T�tulo");
		messageForm.setText("Texto");
		messageForm.setReceiver(receiver);
		
		saveTemplate("user2", "validTest", messageForm, null);
	}
	
	// 2. Creaci�n incorrecta de message, usuario no admite nuevos mensajes
	@Test
	public void testCaseSaveIncorrect() {
		Message msg = messageService.create();
		MessageForm messageForm = messageService.construct(msg);
		Actor receiver = actorService.findById(18);
		
		messageForm.setTitle("T�tulo");
		messageForm.setText("Texto");
		messageForm.setReceiver(receiver);
		
		saveTemplate("user2", "invalidTest", messageForm, IllegalArgumentException.class);
	}

}
