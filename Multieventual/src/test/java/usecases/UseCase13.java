package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.MessageService;
import utilities.AbstractTest;
import domain.Message;
import forms.MessageFormReply;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 	Gestionar sus mensajes, lo cual incluye listarlos, escribirlos y eliminarlos.
Tambi�n se puede responder a un mensaje recibido incluyendo el contenido del
mismo en el nuevo mensaje. N�tese que no es necesario crear una copia del
mensaje enviado para el actor que lo escribi�. Tambi�n se debe mostrar un
aviso de confirmaci�n cuando un actor intente eliminar un mensaje.
 * 
 * @author Student
 * 
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase13 extends AbstractTest {

	// Constructor -------------------

	public UseCase13() {
		super();
	}

	// Services ----------------------------

	@Autowired
	private MessageService messageService;

	// Templates ---------------------------

	private void messageReplyTemplate(String username, String title, Message message, Message original,
			String texto, Class<?> expected) {
		Class<?> caught = null;

		try {
			authenticate(username);
			// El usuario accede al listado de messages y le da click a responder
			// en uno.
			MessageFormReply messageForm = messageService.constructReply(message, original);
			// Rellena los datos y le da click a guardar, lo que ejecuta el
			// m�todo reconstruct.
			Message res = messageService.reconstructReply(messageForm, null);
			// Ejecuta el guardado
			messageService.save(res);
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, title);
	}

	private void messageDeleteTemplate(String username, String title,
			Integer messageId, Class<?> expected) {
		Class<?> caught = null;

		try {
			authenticate(username);
			// El usuario accede al formulario de creaci�n de messages y introduce
			// los datos.
			// Le da click a guardar, lo que ejecuta el m�todo reconstruct
			Message message = messageService.findOne(messageId);
			// Ejecuta el eliminado
			messageService.delete(message);
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, title);
	}

	// Tests -------------------------------

	// 1. Respuesta correcta de message.
	@Test
	public void testCaseReplyCorrect() {
		Message original = messageService.findOne(48);
		Message message = original;
		message.setRepliedText("hola");

		messageReplyTemplate("user2", "validTest", message, original, "Texto", null);
	}

	// 2. Respuesta incorrecta de message, message a responder no le pertenece
	@Test
	public void testCaseSaveIncorrect() {
		Message original = messageService.findOne(45);
		Message message = original;
		message.setRepliedText("hola");

		messageReplyTemplate("user4", "validTest", message, original, "Texto",IllegalArgumentException.class);
	}

	// 1. Eliminaci�n correcta de message.
	@Test
	public void testCaseDeleteCorrect() {
		messageDeleteTemplate("user1", "validTest", 45, null);
	}

	// 2. Eliminaci�n incorrecta de message, usuario no receptor
	@Test
	public void testCaseDeleteIncorrect() {
		messageDeleteTemplate("user3", "invalidTest", 45,IllegalArgumentException.class);
	}

}
