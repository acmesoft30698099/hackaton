package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.CategoryService;
import services.EventService;
import services.SponsorService;
import services.UserService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Administrator;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
/**
 * Casos de prueba para el siguiente requisito funcional
 * 
Visualizar un panel de control (Dashboard) con la siguiente informaci�n:
- N�mero de eventos publicados en total por cada categor�a.
- N�mero de eventos publicados en los �ltimos 7 d�as.
- N�mero de eventos finalizados con mas de 10 asistentes.
- Top 5 de eventos finalizados con mas asistentes y el n�mero de asistentes
de cada uno.
- N�mero de eventos finalizados con una media de puntuaci�n superior o
igual a 5 en los feedback.
- Ratio de usuarios que han publicado al menos un evento.
- Top 5 de usuarios con mas eventos publicados y el n�mero de eventos
publicados de cada uno.
- Top 5 de patrocinadores con m�s contratos aceptados y el n�mero de
contratos aceptados de cada uno.
- N�mero y ratio de usuarios con un Ban aplicado actualmente.
- Por cada categor�a, la media de recaudaci�n en la venta de entradas por
evento de dicha categor�a.
- Usuarios que han realizado un gasto en adquisici�n de entradas superior al
60% de la media del gasto general.
- Por cada categor�a, la media de eventos en estado "Venta" o "En Progreso"
por evento de dicha categor�a.
- Eventos en los que la suma de los pagos del patrocinio superan el 75% de la
media de pagos del patrocinio.
 * @author Student
 *
 */
public class UseCase26 extends AbstractTest {
	
	// Constructor ------------------------
	
	public UseCase26() {
		super();
	}
	
	// Services ---------------------------
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SponsorService sponsorService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Templates --------------------------
	
	private void dashboardTemplate(String username, String testName, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			// El usuario se registra
			authenticate(username);
			Assert.isTrue(servicesUtils.checkUser() instanceof Administrator);
			categoryService.getCategoriesAndTheNumberOfEvents();
			eventService.getEventsPublishedWithin7Days();
			eventService.getFinishedEventsWithMoreThanTenAssistants();
			eventService.getTop5FinishedEventsWithMoreAssistants();
			eventService.getFinishedEventsWithFeedback5OrHigher();
			eventService.getRatioUsersWithEvents();
			userService.getTop5OfUsersByPublishedEvents();
			sponsorService.getTop5SponsorsWithMoreAcceptedContracts();
			userService.getNumberAndAverageOfBannedUsers();
			categoryService.getCategoriesAndTheAverageAmountOfMoneyGained();
			userService.getUsersWithPurchasesHigherThan60Percent();
			categoryService.getAverageOfEventsOnSaleOrOnProgressForCategory();
			eventService.getEventsWithContractsValuedHigherThan75Percent();
			
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	
	
	// Tests ------------------------------
	
	// 1. Aplicaci�n correcta a un Dashboard
	@Test
	public void testCaseCorrect() {
		dashboardTemplate("admin","ValidTest", null);
	}
	
	// 2. Usuario incorrecto
	@Test
	public void testCaseInvalidUser() {
		dashboardTemplate("chorbi1","InvalidTest", IllegalArgumentException.class);
	}

}
