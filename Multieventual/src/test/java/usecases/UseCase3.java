package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.UserService;
import utilities.AbstractTest;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Visualizar el listado de usuarios del sistema y acceder a sus perfiles.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase3 extends AbstractTest {
	
	// Constructor ------------------------------
	
	public UseCase3() {
		super();
	}
	
	// Services ---------------------------------
	
	@Autowired
	private UserService userService;
	
	// Template ---------------------------------
	
	private void templateListUsers(String username, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			userService.findAll();
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ------------------------------------
	
	// 1. Test correcto de listado de usuarios, no existe un caso de prueba incorrecto
	
	@Test
	public void testList() {
		templateListUsers(null, null, "listCorrect");
	}
}
