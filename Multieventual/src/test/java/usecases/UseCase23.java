package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.TopicService;
import utilities.AbstractTest;

/**
 * Caso de prueba para el siguiente caso de uso:
 * Cerrar un tema de conversaci�n, lo cual hace que dicho tema quede bloqueado
	y los actores no puedan escribir nuevos comentarios en el mismo. Este
	mecanismo no afecta a los administradores. Tambi�n es posible volver a abrir
	un tema previamente cerrado.
 * @author Student
 *
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase23 extends AbstractTest {
	
	// Constructor ---------------------------------
	
	public UseCase23() {
		super();
	}
	
	// Services ------------------------------------
	
	@Autowired
	private TopicService topicService;
	
	// Templates -----------------------------------
	
	private void templateCloseTopic(String username, Integer topicId, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			topicService.cerrarTopic(topicId);
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------------
	
	// 1. Test correcto de cerrado de topic
	
	@Test
	public void testCorrectCloseTopic() {
		templateCloseTopic("admin", 126, null, "closeCorrect");
	}

	// 2. Test incorrecto de cerrado de topic, usuario inv�lido
	
	@Test
	public void testIncorrectCloseTopic() {
		templateCloseTopic("user2", 126, IllegalArgumentException.class, "closeIncorrect");
	}
}
