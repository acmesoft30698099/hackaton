package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.ActorService;
import services.EventService;
import services.InvitationService;
import services.PurchaseService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Event;
import domain.Invitation;
import domain.User;
import forms.InvitationForm;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 * Enviar una invitaci�n a otro usuario para que asista como invitado/a a un
 * evento creado por �l o ella. Estas invitaciones solo se pueden enviar
 * mientras el evento se encuentra en estado "Promocional" o "Venta". Cada
 * invitaci�n tendr� un estado de "pendiente", "aceptada" o "rechazada",
 * dependiendo de lo que decida el usuario invitado. Un usuario que ha sido
 * invitado a un evento no necesita la opci�n de adquirir una entrada al mismo.
 * En caso de aceptar una invitaci�n cuando ya hab�a adquirido una entrada, la
 * compra se elimina y se le reembolsa el precio de la entrada si fuese
 * necesario. Un usuario no puede invitarse a si mismo a un evento creado por �l
 * o ella (se entiende que ya es un asistente).
 * 
 * @author Student
 * 
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase17 extends AbstractTest {

	// Constructor -------------------------

	public UseCase17() {
		super();
	}

	// Services ----------------------------

	@Autowired
	private InvitationService invitationService;

	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private ActorService actorService;

	@Autowired
	private EventService eventService;

	@Autowired
	private ServicesUtils servicesUtils;

	// Templates ---------------------------

	private void saveTemplate(String username, String title,
			InvitationForm invitationForm, int actorId, int eventId,
			Class<?> expected) {
		Class<?> caught = null;

		try {
			authenticate(username);
			User user = (User) actorService.findOne(actorId);
			Event event = eventService.findOne(eventId);
			Assert.isTrue(event.getUser().getId() == servicesUtils.checkUser()
					.getId());
			invitationForm.setEvent(event);
			invitationForm.setUser(user);
			// El usuario accede al formulario de creaci�n de invitation y
			// introduce los datos.
			// Le da click a guardar, lo que ejecuta el m�todo reconstruct
			Invitation invitation = invitationService.reconstruct(
					invitationForm, null);
			// Ejecuta el guardado
			invitationService.save(invitation);
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, title);
	}

	private void acceptTemplate(String username, String title,
			int invitationId, Class<?> expected) {
		Class<?> caught = null;

		try {
			authenticate(username);
			Invitation invitation = invitationService.findOne(invitationId);
			// Acepta
			invitationService.acceptInvitation(invitation);

			Assert.isTrue(purchaseService.getPurchaseOfUserToEvent(
					invitation.getUser(), invitation.getEvent()) == null);
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, title);
	}

	private void denyTemplate(String username, String title,
			int invitationId, Class<?> expected) {
		Class<?> caught = null;

		try {
			authenticate(username);
			Invitation invitation = invitationService.findOne(invitationId);
			// Deniega
			invitationService.denyInvitation(invitation);
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, title);
	}

	// Tests -------------------------------

	// 1. Creaci�n correcta de invitation.
	@Test
	public void testCaseSaveCorrect() {
		Invitation inv = invitationService.create();
		InvitationForm invitationForm = invitationService.construct(inv);

		invitationForm.setText("T�tulo");

		saveTemplate("user2", "validTest", invitationForm, 19, 78, null);
	}

	// 2. Creaci�n incorrecta de invitation, usuario creador del evento
	@Test
	public void testCaseSaveIncorrect() {
		Invitation inv = invitationService.create();
		InvitationForm invitationForm = invitationService.construct(inv);

		invitationForm.setText("T�tulo");

		saveTemplate("user2", "validTest", invitationForm, 17, 82,
				IllegalArgumentException.class);
	}

	// 3. Aceptaci�n de invitaci�n.
	@Test
	public void testCaseAccept() {
		acceptTemplate("user1", "validTest", 121, null);
	}

	// 4. Denegaci�n de invitaci�n.
	@Test
	public void testCaseDeny() {
		denyTemplate("user1", "validTest", 121, null);
	}

}
