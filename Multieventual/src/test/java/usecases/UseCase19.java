package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Event;
import domain.Feedback;

import forms.FeedbackForm;

import services.EventService;
import services.FeedbackService;
import utilities.AbstractTest;

/**
 * 
 * @author Student
 * Caso de prueba para el caso de uso siguiente:
 * Publicar un feedback sobre un evento al que ha asistido (tanto por adquisici�n
de entrada como por invitaci�n). Este feedback solo se podr� publicar durante
las dos semanas posteriores al momento en que dicho evento pase a estado de
"Finalizado". El creador del evento no dispone de esta opci�n.
 */
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UseCase19 extends AbstractTest {
	
	// Constructor ----------------------------------
	
	public UseCase19() {
		super();
	}
	
	// Services -------------------------------------
	
	@Autowired
	private FeedbackService feedbackService;
	
	@Autowired
	private EventService eventService;

	// Template -------------------------------------
	
	private void templateSaveFeedback(String username, FeedbackForm feedbackForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Feedback feedback = feedbackService.reconstruct(feedbackForm, null);
			feedbackService.save(feedback);
			feedbackService.flush();
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -----------------------------------------
	
	// 1. Test correcto de guardado de feedback
	
	@Test
	public void testSaveFeedbackCorrect() {
		FeedbackForm feedbackForm = feedbackService.construct(feedbackService.create());
		Event event = eventService.findOne(76);
		feedbackForm.setEvent(event);
		feedbackForm.setScore(10);
		feedbackForm.setText("Text");
		
		templateSaveFeedback("user3", feedbackForm, null, "saveCorrect");
	}
	
	// 2. Test incorrecto de guardado de feedback, usuario no posee entrada ni invitado
	
	@Test
	public void testSaveFeedbackIncorrect() {
		FeedbackForm feedbackForm = feedbackService.construct(feedbackService.create());
		Event event = eventService.findOne(73);
		feedbackForm.setEvent(event);
		feedbackForm.setScore(10);
		feedbackForm.setText("Text");
		
		templateSaveFeedback("user1", feedbackForm, IllegalArgumentException.class, "saveIncorrect");
	}
}
