package utilities;

public class Tuple<A,B> {
	
	// Attributes ---------------------------------------
	
	private A objA;
	private B objB;
	
	// Constructors -------------------------------------
	
	public Tuple() {
		this.objA = null;
		this.objB = null;
	}
	
	public Tuple(A objA, B objB) {
		this.objA = objA;
		this.objB = objB;
	}
	
	// Getters and setters -----------------------------
	
	public A getObjA() {
		return objA;
	}

	public void setObjA(A objA) {
		this.objA = objA;
	}

	public B getObjB() {
		return objB;
	}

	public void setObjB(B objB) {
		this.objB = objB;
	}
}