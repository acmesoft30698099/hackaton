package forms;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;


public class ActorConfigurationForm extends ActorForm {

	// Constructor ---------------------------------

	public ActorConfigurationForm() {
		super();
	}
	
	// Attributes ----------------------------------
	
	private UserAccountForm userAccount;
	private String allowFriendshipRequests;
	private String messagesConfiguration;
	
	@Valid
	public UserAccountForm getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(UserAccountForm userAccount) {
		this.userAccount = userAccount;
	}
	
	@Pattern(regexp = "^(true)|(false)$")
	public String getAllowFriendshipRequests() {
		return allowFriendshipRequests;
	}
	public void setAllowFriendshipRequests(String allowFriendshipRequests) {
		this.allowFriendshipRequests = allowFriendshipRequests;
	}
	
	public String getMessagesConfiguration() {
		return messagesConfiguration;
	}
	public void setMessagesConfiguration(String messagesConfiguration) {
		this.messagesConfiguration = messagesConfiguration;
	}
	
}
