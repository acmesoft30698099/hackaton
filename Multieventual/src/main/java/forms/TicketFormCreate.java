package forms;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

public class TicketFormCreate {

	// Constructor --------------------------------------

	public TicketFormCreate() {
		super();
	}

	// Attributes ------------------------------------------------

	private String name;
	private String description;
	private double price;
	private Integer remainingUnits;

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Min(0)
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Min(0)
	public Integer getRemainingUnits() {
		return remainingUnits;
	}

	public void setRemainingUnits(Integer remainingUnits) {
		this.remainingUnits = remainingUnits;
	}
}
