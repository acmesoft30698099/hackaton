package forms;

import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import domain.Event;

public class ContractForm {
	
	// Constructor --------------------------
	
	public ContractForm() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private String termsAndConditions;
	private double offeredPayment;
	
	@NotBlank
	public String getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	
	@Min(0)
	public double getOfferedPayment() {
		return offeredPayment;
	}
	public void setOfferedPayment(double offeredPayment) {
		this.offeredPayment = offeredPayment;
	}
	
	// Relationships ------------------------------------
	
	private Event event;
	
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	

}
