package forms;

import javax.validation.constraints.Min;

public class FormObject {
	
	// Constructor --------------------------------------

	public FormObject() {
		super();
	}
	
	// Attributes ------------------------------------------------
	
	private int id;

	@Min(0)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
