package forms;

public class UserRegistrationForm extends ActorProfileForm {
	
	// Constructor ---------------------------------

	public UserRegistrationForm() {
		super();
	}
	
	// Attributes ----------------------------------
	
	private UserAccountForm userAccount;
	
	public UserAccountForm getUserAccount() {
		return userAccount;
	}
	
	public void setUserAccount(UserAccountForm userAccount) {
		this.userAccount = userAccount;
	}
	
}
