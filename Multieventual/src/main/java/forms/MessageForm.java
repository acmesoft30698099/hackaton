package forms;

import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import domain.Actor;

public class MessageForm {
	
	// Constructor -------------------------------------
	
	public MessageForm(){
		super();
	}
	
	// Attributes --------------------------------------

	private String title;
	private String text;
	
	@NotBlank
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@NotBlank
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	// Relationships -------------------------------------------

	private Actor receiver;

	@NotNull
	@Valid
	@ManyToOne
	public Actor getReceiver() {
		return receiver;
	}
	public void setReceiver(Actor receiver) {
		this.receiver = receiver;
	}

}
