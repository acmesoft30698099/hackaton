package forms;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import domain.Event;

public class TicketForm extends FormObject {
	
	// Constructor --------------------------------------

	public TicketForm() {
		super();
	}
	
	// Attributes ------------------------------------------------
	
	private String name;
	private String description;
	private double price;
	private Integer remainingUnits;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Min(0)
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Min(0)
	public Integer getRemainingUnits() {
		return remainingUnits;
	}
	public void setRemainingUnits(Integer remainingUnits) {
		this.remainingUnits = remainingUnits;
	}

	// Relationships ------------------------------------------------
	
	private Event event;

	@NotNull
	@Valid
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
}
