package forms;

public class SponsorForm extends ActorProfileForm {
	
	// Constructor ---------------------------------

	public SponsorForm() {
		super();
	}
	
	// Attributes ----------------------------------
	
	private String companyName;
	private String companyWebsite;
	private String banner;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	
	public String getBanner() {
		return banner;
	}
	public void setBanner(String banner) {
		this.banner = banner;
	}
	
}
