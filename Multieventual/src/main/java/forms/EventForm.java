package forms;

import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import domain.Category;
import domain.GeoCoordinate;
import domain.Image;

public class EventForm extends FormObject {
	
	// Constructor --------------------------------------

	public EventForm() {
		super();
	}
	
	// Attributes ------------------------------------------------
	
	private String name;
	private String description;
	private String celebrationAddress;
	private GeoCoordinate celebrationCoordinate;
	private Date publicationDate;
	private Date saleStartDate;
	private Date startingDate;
	private Date endingDate;
	private List<Image> pictures;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@NotBlank
	public String getCelebrationAddress() {
		return celebrationAddress;
	}
	public void setCelebrationAddress(String celebrationAddress) {
		this.celebrationAddress = celebrationAddress;
	}
	
	@NotNull
	@Valid
	public GeoCoordinate getCelebrationCoordinate() {
		return celebrationCoordinate;
	}
	public void setCelebrationCoordinate(GeoCoordinate celebrationCoordinate) {
		this.celebrationCoordinate = celebrationCoordinate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getSaleStartDate() {
		return saleStartDate;
	}
	public void setSaleStartDate(Date saleStartDate) {
		this.saleStartDate = saleStartDate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getStartingDate() {
		return startingDate;
	}
	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getEndingDate() {
		return endingDate;
	}
	public void setEndingDate(Date endingDate) {
		this.endingDate = endingDate;
	}
	
	@Valid
	@ElementCollection
	public List<Image> getPictures() {
		return pictures;
	}
	public void setPictures(List<Image> pictures) {
		this.pictures = pictures;
	}
	
	// Relationships ------------------------------------------------
	
	private List<TicketFormCreate> tickets;
	private Category category;

	@NotNull
	@Valid
	public List<TicketFormCreate> getTickets() {
		return tickets;
	}
	public void setTickets(List<TicketFormCreate> tickets) {
		this.tickets = tickets;
	}
	
	@NotNull
	@Valid
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
}
