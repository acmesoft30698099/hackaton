
package forms;

import domain.Comment;
import domain.Topic;

public class CommentForm {

	private int		id;
	private String	contentText;
	private Topic	topic;
	private Comment	quoted;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContentText() {
		return contentText;
	}

	public void setContentText(String contentText) {
		this.contentText = contentText;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public Comment getQuoted() {
		return quoted;
	}

	public void setQuoted(Comment quoted) {
		this.quoted = quoted;
	}

}
