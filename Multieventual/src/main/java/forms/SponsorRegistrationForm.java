package forms;

public class SponsorRegistrationForm extends SponsorForm {
	
	// Constructor ---------------------------------

	public SponsorRegistrationForm() {
		super();
	}
	
	// Attributes ----------------------------------
	
	private UserAccountForm userAccount;
	
	public UserAccountForm getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(UserAccountForm userAccount) {
		this.userAccount = userAccount;
	}
	
}
