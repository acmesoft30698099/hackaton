package forms;

import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import domain.Actor;

public class FriendshipRequestForm {
	
	// Constructor --------------------------------------

	public FriendshipRequestForm() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private String text;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	// Relationships ------------------------------------
	
	private Actor receiver;

	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Actor getReceiver() {
		return receiver;
	}
	public void setReceiver(Actor receiver) {
		this.receiver = receiver;
	}

}
