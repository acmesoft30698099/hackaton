package forms;

import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import domain.Message;

public class MessageFormReply {
	
	// Constructor -------------------------------------
	
	public MessageFormReply(){
		super();
	}
	
	// Attributes --------------------------------------

	private String repliedText;
	
	@NotNull
	public String getRepliedText() {
		return repliedText;
	}
	public void setRepliedText(String repliedText) {
		this.repliedText = repliedText;
	}
	
	// Relationships -------------------------------------------

	private Message original;

	@NotNull
	@Valid
	@OneToOne(optional = false)
	public Message getOriginal() {
		return original;
	}
	public void setOriginal(Message original) {
		this.original = original;
	}

}
