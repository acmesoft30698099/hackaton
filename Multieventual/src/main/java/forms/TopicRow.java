package forms;

import java.util.Date;

public class TopicRow {
	
	public TopicRow(int id, String title, boolean closed, int idCreator, String nameCreator, String surnameCreator, 
			int idLastComment, String nameLastComment, String surnameLastComment, Date lastCommentDate) {
		super();
		this.id = id;
		this.title = title;
		this.closed = closed;
		this.idCreator = idCreator;
		this.nameCreator = nameCreator+" "+surnameCreator;
		this.idLastComment = idLastComment;
		this.nameLastComment = nameLastComment+" "+surnameLastComment;
		this.lastCommentDate = lastCommentDate;
	}
	
	private int id;
	private String title;
	private boolean closed;
	private int idCreator;
	private String nameCreator;
	private int idLastComment;
	private String nameLastComment;
	private Date lastCommentDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	
	public int getIdCreator() {
		return idCreator;
	}
	public void setIdCreator(int idCreator) {
		this.idCreator = idCreator;
	}
	
	public String getNameCreator() {
		return nameCreator;
	}
	public void setNameCreator(String nameCreator) {
		this.nameCreator = nameCreator;
	}
	
	public int getIdLastComment() {
		return idLastComment;
	}
	public void setIdLastComment(int idLastComment) {
		this.idLastComment = idLastComment;
	}
	
	public String getNameLastComment() {
		return nameLastComment;
	}
	public void setNameLastComment(String nameLastComment) {
		this.nameLastComment = nameLastComment;
	}
	
	public Date getLastCommentDate() {
		return lastCommentDate;
	}
	public void setLastCommentDate(Date lastCommentDate) {
		this.lastCommentDate = lastCommentDate;
	}

}
