package forms;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import domain.Event;

public class TopicForm {
	
	// Constructor ------------------------------------
	
	public TopicForm() {
		super();
	}
	
	// Attributes -------------------------------------

	private int id;
	private String title;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	// Relationships ---------------------------------
	
	private Event event;
	private List<CommentForm> comments;
	
	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
	
	@NotEmpty
	public List<CommentForm> getComments() {
		return comments;
	}

	public void setComments(List<CommentForm> commentForm) {
		this.comments = commentForm;
	}
	
}
