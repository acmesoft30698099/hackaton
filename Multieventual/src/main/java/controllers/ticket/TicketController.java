package controllers.ticket;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestParam;

import services.EventService;
import services.InvitationService;
import services.TicketService;
import services.utils.ServicesUtils;

import controllers.AbstractController;
import domain.Event;
import domain.Ticket;
import domain.User;
import domain.utils.EventStatus;
import forms.TicketForm;

@Controller
@RequestMapping("/ticket")
public class TicketController extends AbstractController {
	
	// Controller -------------------------------------------
	
	public TicketController() {
		super();
	}
	
	// Service ----------------------------------------------
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private InvitationService invitationService;

	// List -------------------------------------------------
	
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		Collection<Ticket> tickets;
		Event event;
		
		try {
			event = eventService.findOne(eventId); 
			tickets = new ArrayList<Ticket>(event.getTickets());
			EventStatus status = servicesUtils.getEventStatus(event); 
			
			result = new ModelAndView("ticket/list");
			
			result.addObject("tickets",tickets);
			result.addObject("requestUri","/ticket/list.do");
			
			try {
				User user = servicesUtils.getRegisteredUser(User.class);
				// Si el usuario es el creador y est� en draft o promocional
				if(event.getUser() == user && (status == EventStatus.DRAFT 
						|| status == EventStatus.PROMOTIONAL)) {
					// Podr� editar el evento y las entradas
					result.addObject("event",event);
					result.addObject("canEdit",true);
				// En caso contrario
				} else {
					// Comprueba que pueda acceder al evento
					Assert.isTrue(status != EventStatus.DRAFT);
					// Si el usuario no ha realizado una compra de una entrada del evento
					if(!(eventService.getIfUserHasPurchaseTicketForEvent(user, event)
							// O no est� invitado
							|| invitationService.getIfUserReceivedInvitationFromEvent(user, event))
							// El usuario no es el creador							
							&& user.getId() != event.getUser().getId()
							// El estado del evento es abierto a compra
							&& status == EventStatus.SALE 
							) {
						result.addObject("canFreeBuy",true);
						// Comprobamos que tenga tarjeta de cr�dito y que sea v�lida
						if((user.getCreditCard() != null && servicesUtils.validCreditCard(user.getCreditCard()))) {
							result.addObject("canBuy", true);
						}
					}
				}
			} catch(Throwable excp) {
				result.addObject("canBuy",false);
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
			
		return result;
	}
	
	// Profile -----------------------------------
	
	@RequestMapping("/profile")
	public ModelAndView profile(@RequestParam(required=true) Integer ticketId) {
		ModelAndView result;
		Ticket ticket;
		
		try {
			ticket = ticketService.findOne(ticketId);
			EventStatus status = servicesUtils.getEventStatus(ticket.getEvent());
			
			result = new ModelAndView("ticket/profile");
			result.addObject("ticket",ticket);
			
			// Comprobamos si est� registrado
			try {
				// Si est� como User
				User user = servicesUtils.getRegisteredUser(User.class);
				
				// Comprobamos que el usuario es el creador
				if(user == ticket.getEvent().getUser() && (status == EventStatus.DRAFT || status == EventStatus.PROMOTIONAL)) {
					// Asignamos permisos de edici�n
					result.addObject("canEdit",true);
				} else { // En caso contrario
					// Comprobamos que sea visible el evento
					Assert.isTrue(status != EventStatus.DRAFT);
					Event event = ticket.getEvent();
					// Si el usuario no ha realizado una compra de una entrada del evento
					if(!(eventService.getIfUserHasPurchaseTicketForEvent(user, event)
							// O no est� invitado
							|| invitationService.getIfUserReceivedInvitationFromEvent(user, event))
							// El usuario no es el creador							
							&& user.getId() != event.getUser().getId()
							// El estado del evento es abierto a compra
							&& status == EventStatus.SALE 
							) {
						result.addObject("canFreeBuy",true);
						// Comprobamos que tenga tarjeta de cr�dito y que sea v�lida
						if((user.getCreditCard() != null && servicesUtils.validCreditCard(user.getCreditCard()))) {
							result.addObject("canBuy", true);
						}
					}
				}
			} catch(Throwable excp) {
				// Es otro usuario o no est� registrado
				Assert.isTrue(status != EventStatus.DRAFT);
				result.addObject("canBuy",false);
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Create ------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		TicketForm ticketForm;
		Event event;
		
		try {
			event = eventService.findOneForEdit(eventId);
			EventStatus status = servicesUtils.getEventStatus(event);
			// Los eventos s�lo podr�n modificarse cuando est�n en Draft o publicados sin venta.
			Assert.isTrue(status == EventStatus.DRAFT || status == EventStatus.PROMOTIONAL);
			
			ticketForm = ticketService.construct(ticketService.create());
			ticketForm.setEvent(event);

			result = createEditModelAndView(ticketForm);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Edit -------------------------------------------
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required=true) Integer ticketId) {
		ModelAndView result;
		TicketForm ticketForm;
		Ticket ticket;
		
		try {
			ticket = ticketService.findOneForEdit(ticketId);
			ticketForm = ticketService.construct(ticket);
			
			result = createEditModelAndView(ticketForm);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Save ----------------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="ticket") TicketForm ticketForm, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()) {
			result = createEditModelAndView(ticketForm);
		} else {
			Ticket ticket = ticketService.reconstruct(ticketForm, binding);
			if(binding.hasErrors()) {
				result = createEditModelAndView(ticketForm);
			} else {
				try {
					ticket = ticketService.save(ticket);
					result = new ModelAndView("redirect:/ticket/profile.do?ticketId="+ticket.getId());
				} catch(Throwable excp) {
					if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
						result = createEditModelAndView(ticketForm, excp.getMessage());
					} else {
						result = createEditModelAndView(ticketForm, "commit.error");
					}
				}
			}
		}
		
		return result;
	}
	
	// Delete --------------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="delete")
	public ModelAndView delete(@ModelAttribute(value="ticket") TicketForm ticketForm, BindingResult binding) {
		ModelAndView result;
		
		try {
			Ticket ticket = ticketService.reconstruct(ticketForm, binding);
			try {
				ticketService.delete(ticket);
				result = new ModelAndView("redirect:/ticket/list.do?eventId="+ticket.getEvent().getId());
			} catch(Throwable excp) {
				if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
					result = createEditModelAndView(ticketForm,excp.getMessage());
				} else {
					result = createEditModelAndView(ticketForm,"commit.error");
				}
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Ancillary methods ---------------------------------
	
	private ModelAndView createEditModelAndView(TicketForm ticketForm) {
		return createEditModelAndView(ticketForm, null);
	}
	
	private ModelAndView createEditModelAndView(TicketForm ticketForm, String message) {
		ModelAndView result;
		
		result = new ModelAndView("ticket/edit");
		result.addObject("ticket",ticketForm);
		result.addObject("lastTicket",ticketForm.getEvent().getTickets().size() == 1);
		result.addObject("message",message);
		
		return result;
	}
}
