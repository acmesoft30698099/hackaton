package controllers.friendship;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import services.ActorService;
import services.AlertService;
import services.FriendshipRequestService;
import services.utils.ServicesUtils;
import controllers.AbstractController;
import domain.Actor;
import domain.FriendshipRequest;
import domain.utils.AlertStatus;
import forms.FriendshipRequestForm;

@Controller
@RequestMapping("/friendshipRequest")
public class FriendshipRequestController extends AbstractController {
	
	// Constructor --------------------------
	
	public FriendshipRequestController() {
		super();
	}
	
	// Services -----------------------------
	
	@Autowired
	private FriendshipRequestService friendshipRequestService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private AlertService alertService;

	@Autowired
	private ServicesUtils servicesUtils;
	
	// List (pending) ---------------------------------
	
	@RequestMapping("/list")
	public ModelAndView listPending() {
		ModelAndView result;
		Collection<FriendshipRequest> friends;
		
		try {
			Actor actor = servicesUtils.checkUser();
			friends = actor.getReceivedFriendshipsRequests();
			friends.addAll(actor.getSentFriendshipsRequests());
			
			result = new ModelAndView("friendshipRequest/list");
			result.addObject("friendships",friends);
			result.addObject("actorId",actor.getId());
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// List (friends) -----------------------------------
	
	@RequestMapping("/friends")
	public ModelAndView listFriends() {
		ModelAndView result;
		Collection<Actor> friends;
		
		try{
			Actor actor = servicesUtils.checkUser();
			friends = actorService.getFriends(actor.getId());
			
			result = new ModelAndView("user/list");
			result.addObject("users",friends);
			result.addObject("requestUri","friendshipRequest/friends.do");
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Create -------------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		FriendshipRequestForm form = friendshipRequestService.construct(friendshipRequestService.create());
		
		result = createEditModelAndView(form);
		
		return result;
	}
	
	// Save ---------------------------------------------
	
	@RequestMapping(value="/create", method=RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("friendship") FriendshipRequestForm form, BindingResult binding) {
		ModelAndView result;
		
		if(!binding.hasErrors()) {
			try {
				FriendshipRequest friendshipRequest = friendshipRequestService.reconstruct(form, binding);
				if(binding.hasErrors()) {
					result = createEditModelAndView(form);
				} else {
					try {
						friendshipRequestService.save(friendshipRequest);
						
						// Creaci�n de la alerta
						try{
							alertService.generateAlert(friendshipRequest.getReceiver(), AlertStatus.FRIENDSHIP_RECEIVED, "http://www.acme.com/friendshipRequest/list.do");
						} catch(Throwable excp) {
							// Fallo silencioso, no implica un fallo en la transacci�n de la petici�n de amistad
						}
						
						result = new ModelAndView("redirect:/friendshipRequest/list.do");
					} catch(Throwable excp) {
						result = createEditModelAndView(form,"commit.error");
					}
				}
			} catch(Throwable excp) {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		} else {
			result = createEditModelAndView(form);
		}
		
		return result;
	}
	
	// Accept -------------------------------------------
	
	@RequestMapping("/accept")
	public ModelAndView accept(@RequestParam(required=true) Integer requestId, RedirectAttributes attr) {
		ModelAndView result;
		FriendshipRequest friendship;
		
		result = new ModelAndView("redirect:/friendshipRequest/list.do");
		
		try {
			friendship = friendshipRequestService.findOneForEdit(requestId);
			
			friendship.setState("ACCEPTED");
			friendshipRequestService.save(friendship);
			
			// Creaci�n de la alerta
			try{
				alertService.generateAlert(friendship.getReceiver(), AlertStatus.FRIENDSHIP_ACCEPTED, "http://www.acme.com/friendshipRequest/list.do");
			} catch(Throwable excp) {
				// Fallo silencioso, no implica un fallo en la transacci�n de la petici�n de amistad
			}
		} catch(Throwable excp) {
			attr.addFlashAttribute("message","commit.error");
		}
		
		return result;
	}
	
	// Deny ----------------------------------------------
	
	@RequestMapping("/deny")
	public ModelAndView deny(@RequestParam(required=true) Integer requestId, RedirectAttributes attr) {
		ModelAndView result;
		FriendshipRequest friendship;
		
		result = new ModelAndView("redirect:/friendshipRequest/list.do");
		
		try {
			friendship = friendshipRequestService.findOneForEdit(requestId);
			
			friendship.setState("DENIED");
			friendshipRequestService.save(friendship);
			
			// Creaci�n de la alerta
			try{
				alertService.generateAlert(friendship.getReceiver(), AlertStatus.FRIENDSHIP_DENIED, "http://www.acme.com/friendshipRequest/list.do");
			} catch(Throwable excp) {
				// Fallo silencioso, no implica un fallo en la transacci�n de la petici�n de amistad
			}
		} catch(Throwable excp) {
			attr.addFlashAttribute("message","commit.error");
		}
		
		return result;
	}

	// Ancillary methods --------------------------------
	
	private ModelAndView createEditModelAndView(FriendshipRequestForm form) {
		return createEditModelAndView(form, null);
	}
	
	private ModelAndView createEditModelAndView(FriendshipRequestForm form, String message) {
		ModelAndView result;
		Actor actor = servicesUtils.checkUser();
		Collection<Actor> actors = friendshipRequestService.getUsersThatAreNotFriendsWith(actor);
		actors.remove(actor);
		
		result = new ModelAndView("friendshipRequest/edit");
		
		result.addObject("friendship", form);
		result.addObject("message", message);
		result.addObject("actors", actors);
		
		return result;
	}

}
