package controllers.event;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Image;
import forms.EventForm;

@Controller
@RequestMapping("/event/pictures")
public class EventPicturesController extends AbstractController {
	
	// Constructor --------------------------------------------
	
	public EventPicturesController() {
		super();
	}
	
	// Services -----------------------------------------------
	
	@Autowired
	private EventController eventController;
	
	// Add picture --------------------------------------------
	
	@RequestMapping(value="/addPicture", method=RequestMethod.POST)
	public ModelAndView addPicture(@ModelAttribute(value="event") EventForm eventForm, BindingResult binding) {
		ModelAndView result;
		
		if(eventForm.getPictures() == null) {
			eventForm.setPictures(new ArrayList<Image>()); // Al convertirse una colecci�n vac�a a null, es necesario inicializarla
		}
		
		eventForm.getPictures().add(new Image());
		
		result = eventController.createEditModelAndView(eventForm);
		
		return result;
	}
	
	// Remove picture -----------------------------------------
	
	@RequestMapping(value="removePicture", method=RequestMethod.POST)
	public ModelAndView removePicture(@ModelAttribute(value="event") EventForm eventForm, BindingResult binding,
			Integer index) {
		ModelAndView result;
		
		eventForm.getPictures().remove((int)index); // Necesario que sea del tipo int, no objeto Integer
		
		result = eventController.createEditModelAndView(eventForm);
		
		return result;
	}	

}
