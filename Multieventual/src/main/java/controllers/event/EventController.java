package controllers.event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AlertService;
import services.CategoryService;
import services.ContractService;
import services.EventService;
import services.TicketService;
import services.UserService;
import services.utils.ServicesUtils;
import controllers.AbstractController;
import domain.Actor;
import domain.Category;
import domain.Event;
import domain.Sponsor;
import domain.User;
import domain.utils.AlertStatus;
import domain.utils.EventStatus;
import forms.EventForm;

@Controller
@RequestMapping("/event")
public class EventController extends AbstractController {

	// Constructor --------------------------------------

	public EventController() {
		super();
	}
	
	// Services ------------------------------------------------
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private ContractService contractService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private AlertService alertService;
	
	// List (finished) ------------------------------------------------
	
	@RequestMapping("/listFinished")
	public ModelAndView listFinished(@RequestParam(required=false) String category) {
		ModelAndView result;
		Collection<Event> events;
		
		if(category != null) {
			events = eventService.getFinishedEventsWithCategory(category);
		} else {
			events = eventService.getFinishedEvents();
		}
		
		result = new ModelAndView("event/list");
		result.addObject("requestUri","event/listFinished.do");
		result.addObject("events",events);
		result.addObject("category",category);
		result.addObject("canFilterCategory",true);
		
		return result;
	}
	
	// List (in progress) ------------------------------------------------

	@RequestMapping("/listInProgress")
	public ModelAndView listInProgress(@RequestParam(required=false) String category) {
		ModelAndView result;
		Collection<Event> events;

		if(category != null) {
			events = eventService.getInProgressEventsWithCategory(category);
		} else {
			events = eventService.getInProgressEvents();
		}
		
		result = new ModelAndView("event/list");
		result.addObject("requestUri", "event/listInProgress.do");
		result.addObject("events", events);
		result.addObject("category",category);
		result.addObject("canFilterCategory",true);

		return result;
	}
	
	// List (on sale and promotion) ----------------------------------------------------
	
	@RequestMapping("/listOnSale")
	public ModelAndView listOnSale(@RequestParam(required=false) String category) {
		ModelAndView result;
		Collection<Event> events;
		
		if(category != null) {
			events = eventService.getSaleEventsWithCategory(category);
		} else {
			events = eventService.getSaleEvents();
		}
		
		result = new ModelAndView("event/list");
		result.addObject("requestUri","event/listOnSale.do");
		result.addObject("events",events);
		result.addObject("category",category);
		result.addObject("canFilterCategory",true);
		
		return result;
	}
	
	// List (owned) ------------------------------------------
	
	@RequestMapping(value="/listOwns")
	public ModelAndView listOwns() {
		ModelAndView result;
		Collection<Event> events;
		
		try {
			User user = servicesUtils.getRegisteredUser(User.class);
			events = new ArrayList<Event>(user.getEvents());
			
			result = new ModelAndView("event/list");
			result.addObject("events",events);
			result.addObject("requestUri","event/listOwns.do");
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Finder ------------------------------------------------
	
	@RequestMapping("/finder")
	public ModelAndView finder(@RequestParam(required=false) String keyword, @RequestParam(required=false) String address) {
		ModelAndView result;
		Collection<Event> events;
		
		events = eventService.getEventsByKeywordAndAddress(keyword,address);
		
		result = new ModelAndView("event/list");
		result.addObject("requestUri","event/finder.do");
		result.addObject("events",events);
		result.addObject("keyword", keyword);
		result.addObject("address", address);
		result.addObject("canSearch", true);
		
		return result;
	}
	
	// Profile -----------------------------------------------
	
	@RequestMapping("/profile")
	public ModelAndView profile(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		Event event;
		EventStatus status;
		
		event = eventService.findOne(eventId);
		
		try {
			
			status = servicesUtils.getEventStatus(event);

			result = new ModelAndView("event/profile");
			event.getPictures().size();
			result.addObject("event",event);
			result.addObject("sponsors",contractService.getImagesFromAcceptedContractsOfEvent(event));
			// Comprobaci�n de usuario
			try {
				Actor actor = servicesUtils.checkUser();
				if(actor instanceof User) {
					User user = servicesUtils.getRegisteredUser(User.class);
					if(event.getUser() == user) {
						result.addObject("canInvite",status == EventStatus.SALE || status == EventStatus.PROMOTIONAL);
						result.addObject("canEdit",status == EventStatus.DRAFT || status == EventStatus.PROMOTIONAL);
						result.addObject("isOwner",true);
					} else {
						result.addObject("canSeeTickets",true);
						Assert.isTrue(status != EventStatus.DRAFT);
					}
				} else if(actor instanceof Sponsor) {
					Assert.isTrue(status == EventStatus.SALE);
					// Comprobamos si tiene un contrato en modo aceptado o pendiente
					Sponsor sponsor = (Sponsor) actor;
					Assert.isTrue(!contractService.getIfExitstPendingOrAcceptedContractsBetweenSponsorAndEvent(sponsor, event));
					result.addObject("canCreateContract",true);
				}
			} catch(Throwable excp) {
				Assert.isTrue(status != EventStatus.DRAFT);
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
		
	// Create ------------------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		EventForm eventForm;
		
		userService.prohibitBannedUser();
		
		eventForm = eventService.construct(eventService.create());
		eventForm.getTickets().add(ticketService.constructCreate(ticketService.create()));
		
		result = createEditModelAndView(eventForm);
		
		return result;
	}
	
	// Edit ---------------------------------------------------
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = true) Integer eventId) {
		ModelAndView result;
		Event event;
		EventForm eventForm;
		
		userService.prohibitBannedUser();
		
		try {
			event = eventService.findOneForEdit(eventId);
			eventForm = eventService.construct(event);
			
			result = createEditModelAndView(eventForm);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Save ---------------------------------------------------
	
	@RequestMapping(value="/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value="event") EventForm eventForm, BindingResult binding) {
		ModelAndView result;
		Event event;
		
		userService.prohibitBannedUser();
		
		if(binding.hasErrors()) { // En el caso de que el valor de entrada est� malformado
			result = createEditModelAndView(eventForm);
		} else {
			try {
				event = eventService.reconstruct(eventForm, binding);
				if(binding.hasErrors()) {
					result = createEditModelAndView(eventForm);
				} else {
					try {
						int eventId = event.getId();
						event = eventService.save(event);
						
						// Creaci�n de la alerta, si el evento se crea y est� publicado al crear
						if(eventId == 0 && event.getPublicationDate().before(new Date())){
							try {
								Collection<Actor> friends = actorService.getFriends(servicesUtils.checkUser().getId());
								for(Actor friend : friends) {
									alertService.generateAlert(friend, AlertStatus.NEW_FRIEND_EVENT, "http://www.acme.com/event/profile.do?eventId="+event.getId());
								}
							} catch(Throwable excp) {
								// El envio incorrecto de la alerta no implica el fallo de la transacci�n.
							}
						}
						
						result = new ModelAndView("redirect:/event/profile.do?eventId="+event.getId());
					} catch (Throwable excp) {
						if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
							result = createEditModelAndView(eventForm, excp.getMessage());
						} else {
							result = createEditModelAndView(eventForm, "commit.error");
						}
					}
				}
			} catch (Throwable excp) {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		}
		
		return result;
	}
	
	// Delete --------------------------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="delete")
	public ModelAndView delete(@ModelAttribute(value="event") EventForm eventForm, BindingResult binding) {
		ModelAndView result;
		Event event;
		
		userService.prohibitBannedUser();
		
		try {
			event = eventService.findOne(eventForm.getId());
			eventService.delete(event);
			result = new ModelAndView("redirect:/event/listOwns.do");
		} catch(Throwable excp) {
			result = createEditModelAndView(eventForm,"commit.error");
		}
		
		return result;
	}
	
	// Anclllary methods --------------------------------------------

	public ModelAndView createEditModelAndView(EventForm eventForm) {
		return createEditModelAndView(eventForm,null);
	}
	
	public ModelAndView createEditModelAndView(EventForm eventForm, String message) {
		ModelAndView result = new ModelAndView("event/edit");
		Collection<Category> categories = categoryService.findAll();
		
		result.addObject("event",eventForm);
		result.addObject("message",message);
		result.addObject("categories",categories);
		
		return result;
	}
	}
