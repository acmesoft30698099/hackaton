package controllers.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.TicketService;
import controllers.AbstractController;
import forms.EventForm;

@Controller
@RequestMapping("/event/tickets")
public class EventTicketsController extends AbstractController {
	
	// Constructor ------------------------------------------
	
	public EventTicketsController() {
		super();
	}
	
	// Services ---------------------------------------------
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private EventController eventController;
	
	// Add ticket -------------------------------------------
	
	@RequestMapping(value="/addTicket", method=RequestMethod.POST)
	public ModelAndView createTicket(@ModelAttribute("event") EventForm eventForm, BindingResult binding) {
		ModelAndView result;
	
		eventForm.getTickets().add(ticketService.constructCreate(ticketService.create())); // Al no permitirse un evento sin tickets, siempre tendr� un elemento
		
		result = eventController.createEditModelAndView(eventForm);
		
		return result;
	}
	
	// Remove ticket ----------------------------------------
	
	@RequestMapping(value="/removeTicket", method=RequestMethod.POST)
	public ModelAndView removeTicket(@ModelAttribute("event") EventForm eventForm, BindingResult binding,
			@RequestParam Integer index) {
		ModelAndView result;
		
		if(eventForm.getTickets().size() == 1) {
			result = new ModelAndView("redirect:/misc/403.do");
		} else {
			eventForm.getTickets().remove((int)index);
			
			result = eventController.createEditModelAndView(eventForm);
		}
		return result;
	}

}
