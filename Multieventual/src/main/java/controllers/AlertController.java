package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import services.AlertService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Alert;

@Controller
@RequestMapping("/alert")
public class AlertController extends AbstractController {

	// Constructor -----------------------------------

	public AlertController() {
		super();
	}

	// Services

	@Autowired
	private AlertService alertService;

	@Autowired
	private ServicesUtils servicesUtils;

	// List -----------------------------

	@RequestMapping("/listread")
	public ModelAndView listread() {
		ModelAndView result;
		Collection<Alert> alerts;

		try {
			Actor actor = servicesUtils.checkUser();

			alerts = alertService.getAlertsRead(actor.getId());

			result = new ModelAndView("alert/listread");

			result.addObject("alerts", alerts);
			result.addObject("read", true);
			result.addObject("requestUri", "alert/listread.do");
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	@RequestMapping("/listunread")
	public ModelAndView listunread() {
		ModelAndView result;
		Collection<Alert> alerts;

		try {
			Actor actor = servicesUtils.checkUser();

			alerts = alertService.getAlertsUnRead(actor.getId());

			result = new ModelAndView("alert/listunread");

			result.addObject("alerts", alerts);
			result.addObject("read", false);
			result.addObject("requestUri", "alert/listunread.do");
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Read ---------------------------------------

	@RequestMapping("/read")
	public ModelAndView read(@RequestParam(required = true) Integer alertId, RedirectAttributes attr) {
		ModelAndView result;
		Alert alert;

		result = new ModelAndView("redirect:/alert/listunread.do");

		try {
			alert = alertService.findOneForEdit(alertId);
			alert.set_Read(true);
			alertService.save(alert);
		} catch (Throwable excp) {
			attr.addFlashAttribute("message", "commit.error");
		}

		return result;
	}

	// Remove ---------------------------------------

	@RequestMapping("/remove")
	public ModelAndView remove(@RequestParam(required = true) Integer alertId,
			RedirectAttributes attr) {
		ModelAndView result;
		Alert alert;

		result = new ModelAndView("redirect:/alert/listread.do");

		try {
			alert = alertService.findOneForEdit(alertId);
			alertService.delete(alert);
		} catch (Throwable excp) {
			attr.addFlashAttribute("message", "commit.error");
		}

		return result;
	}

}
