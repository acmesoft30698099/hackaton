/* AbstractController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 * 
 */

package controllers;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import services.utils.BannedUserException;

@Controller
public class AbstractController {
	
	// Panic handler ----------------------------------------------------------
	
	@ExceptionHandler(Throwable.class)
	public ModelAndView panic(Throwable oops) {
		ModelAndView result;
		
		if(oops instanceof BannedUserException) {
			result = new ModelAndView("user/banned");
		} else {
			result = new ModelAndView("misc/panic");
			result.addObject("name", ClassUtils.getShortName(oops.getClass()));
			result.addObject("exception", oops.getMessage());
			result.addObject("stackTrace", ExceptionUtils.getStackTrace(oops));
		}	
		
		return result;
	}
	
	// Panic handler (Banned) -------------------------------------------------
	
	@ExceptionHandler(BannedUserException.class)
	public ModelAndView panicBanned(BannedUserException excp) {
		ModelAndView result;
		
		result = new ModelAndView("user/banned");
		
		return result;
	}
	
}
