package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AlertService;
import services.MessageService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Message;
import domain.utils.AlertStatus;
import forms.MessageForm;
import forms.MessageFormReply;

@Controller
@RequestMapping("/message")
public class MessageController extends AbstractController {

	// Constructor ----------------------

	public MessageController() {
		super();
	}

	// Services -------------------------

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private AlertService alertService;

	// List -----------------------------

	@RequestMapping("/list")
	public ModelAndView listReceived() {
		ModelAndView result;
		Collection<Message> messages;

		try {
			Actor actor = servicesUtils.checkUser();
			
			messages = actor.getReceivedMessages();

			result = new ModelAndView("message/list");

			result.addObject("messages", messages);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Create normal ---------------------------

	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Message message;
		MessageForm messageForm;

		message = messageService.create();
		messageForm = messageService.construct(message);

		result = createEditModelAndView(messageForm);

		return result;
	}

	// Create reply ----------------------------

	@RequestMapping("/reply")
	public ModelAndView replyCreate(@RequestParam(required = true) Integer messageId) {
		ModelAndView result;
		Message message;
		Message original;
		MessageFormReply messageForm;

		try {
			original = messageService.findOneForEdit(messageId);
			message = messageService.create();
			if(original.getRepliedText()==null){
				messageForm = messageService.constructReply(message, original);

				result = createReplyModelAndView(messageForm);
			}else{
				result = new ModelAndView("redirect:/misc/403.do");
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Edit ------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(
			@ModelAttribute(value = "messageForm") MessageForm messageForm,
			BindingResult binding) {
		ModelAndView result;
		Message message;

		try {
			//Actor act = actorService.findById(actorId);
			//messageForm.setReceiver(act);
			message = messageService.reconstruct(messageForm, binding);
			if (binding.hasErrors()) {
				result = createEditModelAndView(messageForm);
			} else {
				try {
					message = messageService.save(message);
					
					// Envio de alerta
					try {
						Actor receiver = message.getReceiver();
						alertService.generateAlert(receiver, AlertStatus.MESSAGE_RECEIVED, "http://www.acme.com/message/profile.do?messageId="+message.getId());
					} catch(Throwable excp) {
						// No hacemos nada, no supone el fallo de la transacción
					}
					
					result = new ModelAndView("redirect:/message/list.do?messageId="+ message.getId());
				} catch (Throwable excp) {
					result = createEditModelAndView(messageForm,"commit.error");
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	@RequestMapping(value = "/reply", method = RequestMethod.POST, params = "save")
	public ModelAndView saveReply(
			@ModelAttribute(value = "messageFormReply") MessageFormReply messageForm,
			BindingResult binding) {
		ModelAndView result;
		Message message;

		try {
			message = messageService.reconstructReply(messageForm,binding);
			if (binding.hasErrors()) {
				result = createReplyModelAndView(messageForm);
			} else {
				try {
					message = messageService.save(message);
					
					// Envio de alerta
					try {
						Actor receiver = message.getReceiver();
						alertService.generateAlert(receiver, AlertStatus.MESSAGE_RECEIVED, "http://www.acme.com/message/profile.do?messageId="+message.getId());
					} catch(Throwable excp) {
						// No hacemos nada, no supone el fallo de la transacción
					}
					
					result = new ModelAndView("redirect:/message/list.do?messageId="+ message.getId());
				} catch (Throwable excp) {
					if(excp instanceof IllegalArgumentException && excp.getMessage().contains("bussinessRules")){
						result = createReplyModelAndView(messageForm,excp.getMessage());
					}else{
						result = createReplyModelAndView(messageForm,"commit.error");
					}
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}
	
	// Delete ----------------------------------
	
	@RequestMapping("delete")
	public ModelAndView delete(@RequestParam(required=true) Integer messageId) {
		ModelAndView result;
		Message message;
		Actor actor;
		
		try{
			actor = servicesUtils.checkUser();
			message = messageService.findOneForEdit(messageId);
		
			if(message == null || message.getReceiver().getId() != actor.getId()) {
				result = new ModelAndView("redirect:/misc/403.do");
			} else {
				messageService.delete(message);
				result = new ModelAndView("redirect:/message/list.do");
			}
		}catch(Throwable t){
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}

	// Profile ---------------------------------

	@RequestMapping("/profile")
	public ModelAndView profile(@RequestParam(required = true) Integer messageId) {
		ModelAndView result;
		Message message;
		Actor actor = servicesUtils.checkUser();

		message = messageService.findOne(messageId);
		if (actor.getId() != message.getReceiver().getId()) {
			result = new ModelAndView("redirect:/misc/403.do");
		} else {
			result = new ModelAndView("message/profile");
			result.addObject("_message",message);
		}
		result.addObject("requestURI", "message/profile.do");
		
		return result;
	}
	
	// Ancillary methods -----------------------

	public ModelAndView createEditModelAndView(MessageForm messageForm) {
		return createEditModelAndView(messageForm, null);
	}

	public ModelAndView createEditModelAndView(MessageForm messageForm, String message) {
		ModelAndView result = new ModelAndView("message/edit");
		Collection<Actor> actors = actorService.findAll();
		Actor actor = servicesUtils.checkUser();
		
		actors.remove(actor);

		result.addObject("messageForm", messageForm);
		result.addObject("message", message);
		result.addObject("actors", actors);

		return result;
	}

	public ModelAndView createReplyModelAndView(MessageFormReply messageForm) {
		return createReplyModelAndView(messageForm, null);
	}

	public ModelAndView createReplyModelAndView(MessageFormReply messageForm, String message) {
		ModelAndView result = new ModelAndView("message/reply");

		result.addObject("messageFormReply", messageForm);
		result.addObject("message", message);

		return result;
	}
}
