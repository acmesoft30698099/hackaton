package controllers.category;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CategoryService;

import controllers.AbstractController;
import domain.Category;
import forms.CategoryForm;

@Controller
@RequestMapping("/category")
public class CategoryController extends AbstractController {
	
	// Constructor -------------------------------
	
	public CategoryController() {
		super();
	}
	
	// Services ----------------------------------
	
	@Autowired
	private CategoryService categoryService;
	
	// List --------------------------------------
	
	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Category> categories;
		
		categories = categoryService.findAll();
		
		result = new ModelAndView("category/list");
		result.addObject("categories",categories);
		
		return result;
	}
	
	// Create ------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		CategoryForm categoryForm;
		
		categoryForm = categoryService.construct(categoryService.create());
		
		result = createEditModelAndView(categoryForm);
		
		return result;
	}
	
	// Edit --------------------------------------
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam(required=true) Integer categoryId) {
		ModelAndView result;
		CategoryForm categoryForm;
		
		try {
			categoryForm = categoryService.construct(categoryService.findOne(categoryId));
			
			result = createEditModelAndView(categoryForm);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Save --------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("category") CategoryForm categoryForm, BindingResult binding) {
		ModelAndView result;
		Category category;
		
		if(binding.hasErrors()) {
			result = createEditModelAndView(categoryForm);
		} else {
			try {
				category = categoryService.reconstruct(categoryForm, binding);
				if(binding.hasErrors()) {
					result = createEditModelAndView(categoryForm);
				} else {
					try {
						categoryService.save(category);
						result = new ModelAndView("redirect:/category/list.do");
					} catch(Throwable excp) {
						result = createEditModelAndView(categoryForm, "commit.error");
					}
				}
			} catch(Throwable excp) {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		}
		
		return result;
	}
	
	// Delete ------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="delete")
	public ModelAndView delete(@ModelAttribute("category") CategoryForm categoryForm, BindingResult binding) {
		ModelAndView result;
		Category category; 
		
		try {
			category = categoryService.reconstruct(categoryForm, binding);
			try {
				categoryService.delete(category);
				result = new ModelAndView("redirect:/category/list.do");
			} catch(Throwable excp) {
				if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRules")) {
					result = createEditModelAndView(categoryForm, excp.getMessage());
				} else {
					result = createEditModelAndView(categoryForm, "commit.error");
				}
			}
		} catch(Throwable excp) {
			result = createEditModelAndView(categoryForm);
		}
		
		return result;
	}
	
	// Ancillary methods -------------------------
	
	private ModelAndView createEditModelAndView(CategoryForm categoryForm) {
		return createEditModelAndView(categoryForm, null);
	}
	
	private ModelAndView createEditModelAndView(CategoryForm categoryForm, String message) {
		ModelAndView result = new ModelAndView("category/edit");
		
		result.addObject("category",categoryForm);
		result.addObject("message",message);
		
		return result;
	}

}
