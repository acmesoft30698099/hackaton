package controllers.ajax;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import domain.Event;

import services.EventService;

@RestController
@RequestMapping("/rest/event")
public class EventRestController {
	
	// Constructor --------------------------------------
	
	public EventRestController() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private EventService eventService;
	
	// List (all) ---------------------------------------
	
	@RequestMapping("/all")
	public Collection<Event> listAll() {
		Collection<Event> events;
		
		events = eventService.getInProgressEventsAndWithinSevenDays();
		
		return events;
	}

}
