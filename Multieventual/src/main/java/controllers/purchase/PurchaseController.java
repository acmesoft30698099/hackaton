package controllers.purchase;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import services.ActorService;
import services.AlertService;
import services.PurchaseService;
import services.TicketService;
import services.utils.ServicesUtils;

import controllers.AbstractController;
import domain.Actor;
import domain.Purchase;
import domain.Ticket;
import domain.User;
import domain.utils.AlertStatus;

@Controller
@RequestMapping("/purchase")
public class PurchaseController extends AbstractController {
	
	// Constructor -----------------------------------------
	
	public PurchaseController() {
		super();
	}
	
	// Services --------------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private AlertService alertService;
	
	// List ------------------------------------------------
	
	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Purchase> purchases;
		
		try {
			User user = servicesUtils.getRegisteredUser(User.class);
			purchases = user.getPurchases();
			
			result = new ModelAndView("purchase/list");
			result.addObject("purchases",purchases);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Perform purchase --------------------------------------
	
	@RequestMapping("/purchase")
	public ModelAndView purchase(@RequestParam(required=true) Integer ticketId) {
		ModelAndView result;
		
		try {
			User user = servicesUtils.getRegisteredUser(User.class);
			Ticket ticket = ticketService.findOne(ticketId);
			
			Purchase purchase = purchaseService.create();
			purchase.setPurchaseDate(new Date(new Date().getTime() - 1000));
			purchase.setUser(user);
			purchase.setTicket(ticket);
			
			purchase = purchaseService.save(purchase);
			
			// Actualizamos la entrada para que tenga una menos restante en el caso de que tenga un l�mite
			ticketService.updateTicket(ticketId, purchase.getTicket().getRemainingUnits() - 1);
			
			// Enviamos notificaci�n
			try {
				// Primero obtenemos los amigos del usuario
				Collection<Actor> friends = actorService.getFriends(user.getId());
				// Para cada uno de �stos, le enviamos una alerta de compra de eventos
				for(Actor friend : friends) {
					// Envio de la notificaci�n
					alertService.generateAlert(friend, AlertStatus.PURCHASE_FRIEND_EVENT, "http://www.acme.com/event/profile.do?eventId="+purchase.getTicket().getEvent().getId());
				}
			} catch (Throwable excp) {
				// Como no implica un fallo del sistema, no se env�a la notificaci�n s�lamente y se deja la creaci�n de la compra
			}
			
			result = new ModelAndView("redirect:/purchase/list.do");
		} catch(Throwable excp) {
			if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
				result = new ModelAndView("redirect:/ticket/profile.do?eventId="+ticketId);
			}
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Delete ------------------------------------------
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(required=true) Integer purchaseId, RedirectAttributes attr) {
		ModelAndView result;
		result = new ModelAndView("redirect:/purchase/list.do");
		
		try {
			Purchase purchase = purchaseService.findOne(purchaseId);
			purchaseService.delete(purchase);
			
			ticketService.updateTicket(purchase.getTicket().getId(), purchase.getTicket().getRemainingUnits() + 1);
		} catch(Throwable excp) {
			attr.addFlashAttribute("message","commit.error");
		}
		
		return result;
	}

}
