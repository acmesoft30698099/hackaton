package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AlertService;
import services.UserService;
import services.utils.ServicesUtils;
import domain.Administrator;
import domain.User;
import domain.utils.AlertStatus;
import forms.UserForm;
import forms.UserRegistrationForm;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {
	
	// Services ---------------------------------------------------------------

	@Autowired
	private AlertService alertService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Constructors -----------------------------------------------------------
	
	public UserController() {
		super();
	}
	
	// List --------------------------------------------------------------------
	
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(required = false) String name,
			@RequestParam(required = false) String surname,
			@RequestParam(required = false) String address) {
		ModelAndView result;
		Collection<User> users;
		
		result = new ModelAndView("user/list");
		
		if (name == null && surname == null && address == null) {
			users = userService.findAll();
		}
		else {
			users = userService.getUsersBy(name, surname, address);
		}
		
		result.addObject("users", users);
		result.addObject("name", name);
		result.addObject("surname", surname);
		result.addObject("address", address);
		result.addObject("requestUri", "user/list.do");
		
		return result;
	}
		
	// Profile -----------------------------------------------------------------
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(@RequestParam(required = false) Integer id) {
		ModelAndView result = new ModelAndView("user/profile");
		User user;
		
		try {
			if (id == null) {
				user = servicesUtils.checkRegistered(User.class, servicesUtils.checkUser());
				result.addObject("user", user);
				result.addObject("edit", true);
				result.addObject("showBanButton", false);
			}
			else {
				user = userService.findOne(id);
				result.addObject("user", user);
				try {
					user = servicesUtils.checkRegistered(User.class, servicesUtils.checkUser());
					if (user.getId() == id) {
						result.addObject("edit", true);
					}
				}
				catch (Throwable t) {
					// No est� registrado o no es usuario
				}
				try {
					servicesUtils.checkRegistered(Administrator.class, servicesUtils.checkUser());
					result.addObject("showBanButton", true);
				}
				catch (Throwable t) {
					// No est� registrado o no es administrador
					result.addObject("showBanButton", false);
				}
			}
		}
		catch (Throwable t) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		return result;
	}
	
	// Create ------------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		UserRegistrationForm form = userService.constructRegistration(userService.create());
		return createEditModelAndView(form);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@ModelAttribute(value="user") @Valid UserRegistrationForm form, BindingResult binding) {
		ModelAndView result;
		User user = userService.reconstruct(form, binding);
		if (binding.hasErrors()) {
			if (!form.getUserAccount().getPassword().equals(form.getUserAccount().getRepeatPassword())) {
				binding.rejectValue("userAccount.repeatPassword", "password.dontmatch");
				result = createEditModelAndView(form);
			}
			else {
				result = createEditModelAndView(form);
			}
		}
		else {
			try {
				if (!form.getUserAccount().getPassword().equals(form.getUserAccount().getRepeatPassword())) {
					binding.rejectValue("userAccount.repeatPassword", "password.dontmatch");
					result = createEditModelAndView(form);
				}
				else {
					user = userService.save(user);
					result = profile(user.getId());
				}
			}
			catch (DataIntegrityViolationException excp) {
				// Este caso es un caso especial que solo ocurrir� cuando el UserAccount que ha puesto el usuario tiene el mismo 
				// username que uno ya existente, entonces se le informar� de esto
				binding.rejectValue("userAccount.username", "userAccount.already.used");
				result = createEditModelAndView(form);
			}
			catch (Throwable t) {
				result = createEditModelAndView(form, "commit.error");
			}
		}
		return result;
	}
	
	// Edit --------------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		User user = servicesUtils.getRegisteredUser(User.class);
		UserForm form = userService.construct(user);
		return createEditModelAndView(form, user.getId());
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView edit(@ModelAttribute(value="user") UserForm form, BindingResult binding) {
		ModelAndView result;
		User user = userService.reconstruct(form, binding);
		
		if (binding.hasErrors()) {
			result = createEditModelAndView(form, user.getId());
		}
		else {
			try {
				user = userService.save(user);
				result = new ModelAndView("redirect:/user/profile.do");
			}
			catch (Throwable t) {
				result = createEditModelAndView(form, user.getId(), "commit.error");
			}
		}
	
		return result;
	}
	
	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public ModelAndView ban(@RequestParam(required = true) Integer id) {
		ModelAndView result;
		try {
			User user = userService.findOne(id);
			user.setBan(!user.isBan());
			user = userService.save(user);
			if (user.isBan()) {
				alertService.generateAlert(user, AlertStatus.BANNED, null);
			}
			result = profile(user.getId());
		}
		catch (Throwable t) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		return result;
	}
	
	// Ancillary Methods -------------------------------------------------------

	public ModelAndView createEditModelAndView(UserRegistrationForm form) {
		return createEditModelAndView(form, null);
	}
	
	public ModelAndView createEditModelAndView(UserRegistrationForm form, String message) {
		ModelAndView result = new ModelAndView("user/create");
		result.addObject("user", form);
		result.addObject("message", message);
		result.addObject("operation", "create");
		result.addObject("id", 0);

		return result;
	}
	
	public ModelAndView createEditModelAndView(UserForm form, int id) {
		return createEditModelAndView(form, id, null);
	}
	
	public ModelAndView createEditModelAndView(UserForm form, int id, String message) {
		ModelAndView result = new ModelAndView("user/edit");
		result.addObject("user", form);
		result.addObject("message", message);
		result.addObject("operation", "edit");
		result.addObject("id", id);

		return result;
	}
	
}
