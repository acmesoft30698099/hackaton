
package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AlertService;
import services.CommentService;
import services.TopicService;
import services.UserService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Administrator;
import domain.Comment;
import domain.Topic;
import domain.utils.AlertStatus;
import forms.CommentForm;

@Controller
@RequestMapping("comment")
public class CommentController extends AbstractController {
	
	// Constructor ---------------------------------
	
	public CommentController() {
		super();
	}
	
	// Services ------------------------------------

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private TopicService topicService;
	
	@Autowired
	private AlertService alertService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private UserService userService;

	// List ----------------------------------------

	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(required = true) int topicId) {
		ModelAndView res;
		
		try {
			Topic topic = topicService.findOne(topicId);
			
			res = new ModelAndView("comment/list");
			
			res.addObject("comments", commentService.getCommentsOfTopic(topic));
			res.addObject("isClosed", topic.isClosed());
			try {
				Actor actor = servicesUtils.checkUser();
				res.addObject("actor",actor);
				res.addObject("isAdmin", actor instanceof Administrator);
			}
			catch(Throwable excp) {
				res.addObject("actor", null);
				res.addObject("isAdmin", false);
			}
			res.addObject("requestURI", "comment/list.do");
			res.addObject("cancelURL", "topic/list.do?eventId="+topic.getEvent().getId());
			res.addObject("topicId", topicId);
		}
		catch(Throwable excp) {
			res = new ModelAndView("redirect:/misc/403.do");
		}
		
		return res;
	}
	
	// Create (topic) ----------------------------------------

	@RequestMapping(value = "create-from-topic", method = RequestMethod.GET)
	public ModelAndView createFromTopic(@RequestParam(required = true) int topicId) {
		ModelAndView res;
		
		userService.prohibitBannedUser();
		
		try {
			Topic topic = topicService.findOne(topicId);
			// Comprobamos que el topic est� abierto
			Assert.isTrue(!topic.isClosed() || servicesUtils.checkUser() instanceof Administrator);
			
			Comment comment = commentService.create(null, topic);
			CommentForm form = commentService.construct(comment);
			
			res = createEditModelAndView(form);
		} catch(Throwable excp) {
			res = new ModelAndView("redirect:/misc/403.do");
		}
		
		return res;
	}
	
	// Create (quoted) ----------------------------------------

	@RequestMapping(value = "create-from-quoted", method = RequestMethod.GET)
	public ModelAndView createFromComment(@RequestParam(required = true) int quotedId) {
		ModelAndView res;
		
		userService.prohibitBannedUser();
		
		try {
			Comment quoted = commentService.findOne(quotedId);
			// Comprobamos que el comentario no est� en un topic cerrado
			Assert.isTrue(!quoted.getTopic().isClosed() || servicesUtils.checkUser() instanceof Administrator);
			
			Comment comment = commentService.create(quoted, null);
			CommentForm form = commentService.construct(comment);
			
			res = createEditModelAndView(form);
		} catch(Throwable excp) {
			res = new ModelAndView("redirect:/misc/403.do");
		}
		
		return res;
	}
	
	// Edit ---------------------------------------------------

	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = true) int commentId) {
		ModelAndView res;
		
		userService.prohibitBannedUser();
		
		try {
			Comment comment = commentService.findOneForEdit(commentId);
			CommentForm form = commentService.construct(comment);
			
			res = createEditModelAndView(form);
		} catch(Throwable excp) {
			res = new ModelAndView("redirect:/misc/403.do");
		}
		
		return res;
	}
	
	// Save ---------------------------------------------------

	@RequestMapping(value = "edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("commentForm") CommentForm form, BindingResult binding) {
		ModelAndView res;
		
		userService.prohibitBannedUser();
		
		if(binding.hasErrors()) {
			res = createEditModelAndView(form);
		} else {
			try {
				Comment comment = commentService.reconstruct(form, binding);
				if (binding.hasErrors()) {
					res = createEditModelAndView(form);
				} else {
					try {
						commentService.save(comment);
						// Comprobaci�n de modificaci�n por administrador
						// Si el usuario actual es un admin:
						if(servicesUtils.checkUser() instanceof Administrator 
								&& !(comment.getActor() instanceof Administrator)) {
							// Estar� editando el mensaje, enviamos una alerta
							alertService.generateAlert(comment.getActor(), AlertStatus.COMMENT_EDITED, null);
						}
						
						res = new ModelAndView("redirect:list.do?topicId="+comment.getTopic().getId());
						
						/*if(comment.getTopic() != null) {
							res = new ModelAndView("redirect:list.do?topicId="+comment.getTopic().getId());
						} else {
							res = new ModelAndView("redirect:/comment/list-quoted.do?commentId="+comment.getQuoted().getId());
						}*/
					} catch (Throwable t) {
						res = createEditModelAndView(form, "commit.error");
					}
				}
			} catch(Throwable excp) {
				res = new ModelAndView("redirect:/misc/403.do");
			}
		}
		
		return res;
	}

	// Ancillary methods ------------------------------------

	private ModelAndView createEditModelAndView(CommentForm form) {
		return createEditModelAndView(form, null);
	}

	private ModelAndView createEditModelAndView(CommentForm form, String message) {
		ModelAndView res;
		
		res = new ModelAndView("comment/edit");
		
		res.addObject("commentForm", form);
		res.addObject("message", message);
		res.addObject("cancelURL", "comment/list.do?topicId="+form.getTopic().getId());
		/*if(form.getTopic() != null) {
			res.addObject("cancelURL", "comment/list.do?topicId="+form.getTopic().getId());
		} else {
			res.addObject("cancelURL", "comment/list-quoted.do?quotedId="+form.getTopic().getId());
		}*/
		
		return res;
	}

}
