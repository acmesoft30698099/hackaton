/* WelcomeController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 * 
 */

package controllers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import services.utils.ServicesUtils;
import domain.User;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {

	// Constructors -----------------------------------------------------------
	
	public WelcomeController() {
		super();
	}
	
	@Autowired
	private ServicesUtils servicesUtils;
		
	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/index")
	public ModelAndView index(@RequestParam(required=false, defaultValue="John Doe") String name,
			HttpServletRequest httpReq, HttpServletResponse httpRes) {
		ModelAndView result;

		result = new ModelAndView("welcome/index");
		
		// Comprobaci�n del baneado de usuario
		// Si el usuario est� baneado
		
		Cookie cookie = WebUtils.getCookie(httpReq, "isBanned");
		
		try {
			User user = servicesUtils.getRegisteredUser(User.class);
			if(user.isBan()) {
				// Tenemos que a�adirle el atributo de ban, si es que no lo tiene
				// Comprobamos la cookie
				if(cookie == null) {
					cookie = new Cookie("isBanned", "true");
					cookie.setMaxAge(-1);
					httpRes.addCookie(cookie);
					result.addObject("isBanned", true);
				}
			} else {
				// No debe tener el atributo de ban
				if(cookie != null) {
					cookie.setMaxAge(0);
					cookie.setValue("false");
					httpRes.addCookie(cookie);
				}
				result.addObject("isBanned", false);
			}
		} catch(Throwable excp) {
			// El usuario no est� registrado
			// O, se ha deregistrado, en cuyo caso comprobamos la sesi�n para ver si tiene el atributo
			// No debe tener el atributo de ban
			if(cookie != null) {
				cookie.setMaxAge(0);
				cookie.setValue("false");
				httpRes.addCookie(cookie);
			}
			result.addObject("isBanned", false);
		}

		return result;
	}
}