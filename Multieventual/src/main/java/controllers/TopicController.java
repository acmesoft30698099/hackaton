package controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AlertService;
import services.EventService;
import services.TopicService;
import services.UserService;
import domain.Event;
import domain.Topic;
import domain.utils.AlertStatus;
import forms.TopicForm;

@Controller
@RequestMapping("/topic")
public class TopicController extends AbstractController {
	
	// Constructor --------------------------------
	
	public TopicController() {
		super();
	}
	
	// Services -----------------------------------

	@Autowired
	private TopicService topicService;
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AlertService alertService;
	
	// List ---------------------------------------

	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(required = true) int eventId) {
		ModelAndView res;
		Event event;

		try {
			event = eventService.findOne(eventId);
			// Comprobamos que el evento es visible
			Assert.isTrue(event.getPublicationDate().before(new Date()));
			
			res = new ModelAndView("topic/list");
			
			res.addObject("topics", topicService.getTopicsOfEvent(event.getId()));
			res.addObject("eventId", eventId);
		}
		catch(Throwable excp) {
			res = new ModelAndView("redirect:/misc/403.do");
		}

		return res;
	}
	
	// Create ---------------------------------------
	
	@RequestMapping(value = "create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) int eventId) {
		ModelAndView res = null;
		Event event;
		
		userService.prohibitBannedUser();
		
		try {
			event = eventService.findOne(eventId);
			// Comprobamos que el evento est� publicado
			Assert.isTrue(event.getPublicationDate().before(new Date()));
			
			Topic topic = topicService.create(event);
			TopicForm form = topicService.construct(topic);
			
			res = createEditModelAndView(form);
		} catch(Throwable excp) {
			res = new ModelAndView("redirect:/misc/403.do");
		}
		
		return res;
	}
	
	// Save ------------------------------------------

	@RequestMapping(value = "edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value="topicForm") TopicForm form, BindingResult binding) {
		ModelAndView res = null;
		
		userService.prohibitBannedUser();
		
		if(binding.hasErrors()) {
			res = createEditModelAndView(form);
		} else {
			try {
				Topic topic = topicService.createReconstruct(form, binding);
				
				if (binding.hasErrors()) {
					res = createEditModelAndView(form);
				} else {
					try {
						topicService.save(topic);
						res = new ModelAndView("redirect:list.do?eventId="+topic.getEvent().getId());
					} catch (Throwable t) {
						res = createEditModelAndView(form, "commit.error");
					}
				}
			} catch(Throwable excp) {
				res = new ModelAndView("redirect:/misc/403.do");
			}
		}
		
		return res;
	}
	
	// Close ----------------------------------------

	@RequestMapping(value = "close", method = RequestMethod.GET)
	public ModelAndView closeTopic(@RequestParam(required = true) int topicId) {
		ModelAndView result;
		
		try {
			Topic topic = topicService.findOne(topicId);
			topicService.cerrarTopic(topicId);
			
			// Creaci�n de la alerta
			try {
				alertService.generateAlert(topicService.getTopicCreator(topic.getId()), AlertStatus.TOPIC_CLOSED, "http://www.acme.com/topic/list.do?eventId="+topic.getEvent().getId());
			} catch(Throwable excp) {
				// El error del envio no implica un fallo de la transacci�n
			}
			
			result = new ModelAndView("redirect:/topic/list.do?eventId="+topic.getEvent().getId());
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Open ------------------------------------------

	@RequestMapping(value = "open", method = RequestMethod.GET)
	public ModelAndView openTopic(@RequestParam(required = true) int topicId) {
		ModelAndView result;
		
		try {
			Topic topic = topicService.findOne(topicId);
			topicService.abrirTopic(topicId);
			
			// Creaci�n de la alerta
			try {
				alertService.generateAlert(topicService.getTopicCreator(topic.getId()), AlertStatus.TOPIC_OPENED, "http://www.acme.com/topic/list.do?eventId="+topic.getEvent().getId());
			} catch(Throwable excp) {
				// El error del envio no implica un fallo de la transacci�n
			}			
			
			result = new ModelAndView("redirect:/topic/list.do?eventId="+topic.getEvent().getId());
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}

	// Ancillary methods ----------------------------

	private ModelAndView createEditModelAndView(TopicForm form) {
		return createEditModelAndView(form, null);
	}

	private ModelAndView createEditModelAndView(TopicForm form, String message) {
		ModelAndView res = new ModelAndView("topic/edit");
		
		res.addObject("topicForm", form);
		res.addObject("eventId",form.getEvent().getId());
		res.addObject("message", message);
		
		return res;
	}

}
