package controllers.creditCard;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import services.CreditCardService;
import services.utils.ServicesUtils;
import controllers.AbstractController;
import domain.Actor;
import domain.CreditCard;
import domain.Sponsor;
import domain.User;

@Controller
@RequestMapping("/creditCard")
public class CreditCardController extends AbstractController {
	
	// Constructor ---------------------------------------
	
	public CreditCardController() {
		super();
	}
	
	// Services ------------------------------------------
	
	@Autowired
	private CreditCardService creditCardService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Profile -------------------------------------------
	
	@RequestMapping("/profile")
	public ModelAndView profile() {
		ModelAndView result;
		
		try {
			Actor actor = servicesUtils.checkUser();
			result = new ModelAndView("creditCard/profile");
			if(actor instanceof User) {
				User user = (User) actor;
				if(user.getCreditCard() != null) {
					result.addObject("creditCard",user.getCreditCard());
					result.addObject("maskedNumber",servicesUtils.getMaskedCreditCardNumber(user.getCreditCard().getNumber()));
					result.addObject("validity",servicesUtils.validCreditCard(user.getCreditCard()));
				}
			} else {
				Sponsor sponsor = (Sponsor) actor;
				if(sponsor.getCreditCard() != null) {
					result.addObject("creditCard",sponsor.getCreditCard());
					result.addObject("maskedNumber",servicesUtils.getMaskedCreditCardNumber(sponsor.getCreditCard().getNumber()));					
					result.addObject("validity",servicesUtils.validCreditCard(sponsor.getCreditCard()));
				}
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Edit -----------------------------------------------
	
	@RequestMapping("/edit")
	public ModelAndView edit() {
		ModelAndView result;
		
		try {
			Actor actor = servicesUtils.checkUser();
			
			CreditCard creditCard;
			if(actor instanceof User) {
				User user = (User) actor;
				if(user.getCreditCard() == null) {
					creditCard = creditCardService.create();
				} else {
					creditCard = user.getCreditCard();
				}
			} else {
				Sponsor sponsor = (Sponsor) actor;
				if(sponsor.getCreditCard() == null) {
					creditCard = creditCardService.create();
				} else {
					creditCard = sponsor.getCreditCard();
				}
			}
			
			result = createEditModelAndView(creditCard);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Save -----------------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="creditCard") @Valid CreditCard creditCard, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()) {
			result = createEditModelAndView(creditCard);
		} else {
			try {
				creditCardService.save(creditCard);
				result = new ModelAndView("redirect:/creditCard/profile.do");
			} catch(Throwable excp) {
				if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
					result = createEditModelAndView(creditCard, excp.getMessage());
				} else {
					result = createEditModelAndView(creditCard, "commit.error");
				}
			}
		}
		
		return result;
	}
	
	// Delete ---------------------------------------------
	
	@RequestMapping("/delete")
	public ModelAndView delete(RedirectAttributes attr) {
		ModelAndView result;
		result = new ModelAndView("redirect:/creditCard/profile.do");
		
		try {
			creditCardService.delete();
			result = new ModelAndView("redirect:/creditCard/profile.do");
		} catch(Throwable excp) {
			attr.addFlashAttribute("message","commit.error");
		}
		
		return result;
	}
	
	// Ancillary methods ----------------------------------
	
	private ModelAndView createEditModelAndView(CreditCard creditCard) {
		return createEditModelAndView(creditCard,null);
	}
	
	private ModelAndView createEditModelAndView(CreditCard creditCard, String message) {
		ModelAndView result;
		
		result = new ModelAndView("creditCard/edit");
		
		result.addObject("creditCard",creditCard);
		result.addObject("message",message);
		
		return result;
	}

}
