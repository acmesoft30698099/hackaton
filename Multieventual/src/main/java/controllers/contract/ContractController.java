package controllers.contract;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import services.AlertService;
import services.ContractService;
import services.EventService;
import services.utils.ServicesUtils;
import controllers.AbstractController;
import domain.Contract;
import domain.Event;
import domain.Sponsor;
import domain.User;
import domain.utils.AlertStatus;
import forms.ContractForm;

@Controller
@RequestMapping("/contract")
public class ContractController extends AbstractController {
	
	// Constructor ------------------------------------
	
	public ContractController() {
		super();
	}
	
	// Services ---------------------------------------
	
	@Autowired
	private ContractService contractService;
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private AlertService alertService;
	
	// List (Sponsor) ---------------------------------
	
	@RequestMapping("/listSent")
	public ModelAndView listSent() {
		ModelAndView result;
		Collection<Contract> contracts;
		
		try {
			Sponsor sponsor = servicesUtils.getRegisteredUser(Sponsor.class);
			contracts = contractService.getContractsForSponsor(sponsor);
			
			result = new ModelAndView("contract/list");
			result.addObject("contracts",contracts);
			result.addObject("requestUri","/contract/listSent.do");
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// List (User) ------------------------------------
	
	@RequestMapping("/listEvent")
	public ModelAndView listEvent(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		Collection<Contract> contracts;
		
		try {
			Event event = eventService.findOne(eventId);
			// Comprobamos que el usuario tenga permisos de edici�n
			User user = servicesUtils.getRegisteredUser(User.class);
			Assert.isTrue(user.getId() == event.getUser().getId());
			
			contracts = contractService.getContractsForEvent(event);
			
			result = new ModelAndView("contract/list");
			result.addObject("contracts",contracts);
			result.addObject("requestUri","/contract/listEvent.do");
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Create ------------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		ContractForm contractForm;
		
		try {
			Sponsor sponsor = servicesUtils.getRegisteredUser(Sponsor.class);
			Event event = eventService.findOne(eventId);
			
			Assert.isTrue(!contractService.getIfExitstPendingOrAcceptedContractsBetweenSponsorAndEvent(sponsor, event));
			
			contractForm = contractService.construct(contractService.create());
			contractForm.setEvent(event);
			
			result = createEditModelAndView(contractForm);
		} catch(Throwable expc) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Save --------------------------------------------
	
	@RequestMapping(value="/create", method=RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="contract") ContractForm contractForm, BindingResult binding) {
		ModelAndView result;
		
		if(!binding.hasErrors()) {
			try {
				Contract contract = contractService.reconstruct(contractForm, binding);
				if(binding.hasErrors()) {
					result = createEditModelAndView(contractForm);
				} else {
					contractService.save(contract);
					
					// Env�o de alertas
					try {
						alertService.generateAlert(contract.getEvent().getUser(), AlertStatus.CONTRACT_RECEIVED, "http://www.acme.com/contract/listEvent.do?eventId="+contract.getEvent().getId());
					} catch (Throwable excp) {
						
					}
					
					result = new ModelAndView("redirect:/contract/listSent.do");
				}
			} catch (Throwable excp) {
				if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
					result = createEditModelAndView(contractForm, excp.getMessage());
				} else {
					result = createEditModelAndView(contractForm, "commit.error");
				}
			}
		} else {
			result = createEditModelAndView(contractForm);
		}
		
		return result;
	}
	
	// Accept ------------------------------------------
	
	@RequestMapping("/accept")
	public ModelAndView accept(@RequestParam(required=true) Integer contractId, RedirectAttributes attr) {
		ModelAndView result;
		
		try {
			Contract contract = contractService.findOneForEdit(contractId);
			result = new ModelAndView("redirect:/contract/listEvent.do?eventId="+contract.getEvent().getId());
			try {
				contract = contractService.save(contract);

				// Comprobamos que la tarjeta de cr�dito del Sponsor sea correcta
				if(servicesUtils.validCreditCard(contract.getSponsor().getCreditCard())) {
					contract.setState("ACCEPTED");
				} else {
					// El pago estar� en pendiente
					contract.setState("PAYMENT_PENDING");
				}
				
				// Env�o de alertas
				try {
					alertService.generateAlert(contract.getSponsor(), AlertStatus.CONTRACT_ACCEPTED, "http://www.acme.com/contract/listSent.do");
				} catch (Throwable excp) {
					
				}
				
			} catch(Throwable excp) {
				attr.addFlashAttribute("message","commit.error");
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Deny --------------------------------------------
	
	@RequestMapping("/deny")
	public ModelAndView deny(@RequestParam(required=true) Integer contractId, RedirectAttributes attr) {
		ModelAndView result;
		
		try {
			Contract contract = contractService.findOneForEdit(contractId);
			result = new ModelAndView("redirect:/contract/listEvent.do?eventId="+contract.getEvent().getId());
			try {
				// Como intenta guardarlo al objeto en cache, se har� despues el guardado				
				contract = contractService.save(contract);
				contract.setState("DENIED");
				
				// Env�o de alertas
				try {
					alertService.generateAlert(contract.getSponsor(), AlertStatus.CONTRACT_DENIED, "http://www.acme.com/contract/listSent.do");
				} catch (Throwable excp) {
					
				}
				
			} catch(Throwable excp) {
				attr.addFlashAttribute("message","commit.error");
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}	
	
	// Ancillary methods -------------------------------
	
	private ModelAndView createEditModelAndView(ContractForm contractForm) {
		return createEditModelAndView(contractForm, null);
	}
	
	private ModelAndView createEditModelAndView(ContractForm contractForm, String message) {
		ModelAndView result;
		
		result = new ModelAndView("contract/edit");
		result.addObject("contract",contractForm);
		result.addObject("message",message);
		
		return result;
	}
}
