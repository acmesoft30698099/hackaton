package controllers.feedback;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AlertService;
import services.EventService;
import services.FeedbackService;
import services.InvitationService;
import services.PurchaseService;
import services.utils.ServicesUtils;
import controllers.AbstractController;
import domain.Actor;
import domain.Event;
import domain.Feedback;
import domain.User;
import domain.utils.AlertStatus;
import forms.FeedbackForm;

@Controller
@RequestMapping("/feedback")
public class FeedbackController extends AbstractController {
	
	// Constructor -------------------------------------------
	
	public FeedbackController() {
		super();
	}
	
	// Service -----------------------------------------------
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private FeedbackService feedbackService;
	
	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private InvitationService invitationService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private AlertService alertService;
	
	// List --------------------------------------------------
	
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		Event event;
		Collection<Feedback> feedbacks;
		
		try {
			event = eventService.findOne(eventId);
			
			// Comprobamos que el evento haya finalizado
			Assert.isTrue(event.getEndingDate().before(new Date()));
			
			feedbacks = event.getFeedbacks();
			feedbacks.size();

			result = new ModelAndView("feedback/list");
			result.addObject("feedbacks",feedbacks);
			result.addObject("eventId",event.getId());
			try {
				User user = servicesUtils.getRegisteredUser(User.class);
				// Obtenemos el id del feedback realizado por el usuario
				Integer id = feedbackService.getIdOfFeedbackOfUserForEvent(user, eventId);
				// Si no hay, daremos oportunidad a su creaci�n
				if(id == null) {
					// Si tiene una entrada comprada o es un invitado
					Assert.isTrue(purchaseService.getIfUserHasPurchasedTicketFromEvent(user, event) 
							|| invitationService.getIfUserReceivedInvitationFromEvent(user, event));
					// Podr� crear una opini�n
					result.addObject("canCreate",true);
					result.addObject("eventId",event.getId());
				} else {
					// En caso contrario, podr� editar su opini�n.
					result.addObject("editableId",id);
				}
			} catch(Throwable excp) {
				// No hacemos nada, el usuario no est� registrado como User
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Create ------------------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		
		try {
			User user = servicesUtils.getRegisteredUser(User.class);
			Event event = eventService.findOne(eventId);
			
			Assert.isTrue(event.getEndingDate().before(new Date()));
			Assert.isTrue(feedbackService.getIdOfFeedbackOfUserForEvent(user, eventId) == null);
			Assert.isTrue(purchaseService.getIfUserHasPurchasedTicketFromEvent(user, event) 
					|| invitationService.getIfUserReceivedInvitationFromEvent(user, event));
			FeedbackForm feedbackForm = feedbackService.construct(feedbackService.create());
			feedbackForm.setEvent(event);
			
			result = createEditModelAndView(feedbackForm);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Edit ---------------------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required=true) Integer feedbackId) {
		ModelAndView result;
		
		try {
			Feedback feedback = feedbackService.findOneForEdit(feedbackId);
			FeedbackForm feedbackForm = feedbackService.construct(feedback);
			
			result = createEditModelAndView(feedbackForm);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Save --------------------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="feedback") FeedbackForm feedbackForm, BindingResult binding) {
		ModelAndView result;
		
		if(!binding.hasErrors()) {
			try {
				Feedback feedback = feedbackService.reconstruct(feedbackForm, binding);
				if(binding.hasErrors()) {
					result = createEditModelAndView(feedbackForm);
				} else {
					try {
						feedbackService.save(feedback);
						
						// Envio de alertas
						if(feedback.getId() == 0) {
							try {
								Actor actor = servicesUtils.checkUser(); 
								// Primero, obtenemos a los amigos
								Collection<Actor> friends = actorService.getFriends(actor.getId());
								for(Actor friend : friends) {
									alertService.generateAlert(friend, AlertStatus.FEEDBACK_FRIEND, "http://www.acme.com/feedback/list.do?eventId="+feedback.getEvent().getId());
								}
							} catch(Throwable excp) {
								
							}
						}
						
						result = new ModelAndView("redirect:/feedback/list.do?eventId="+feedback.getEvent().getId());
					} catch(Throwable excp) {
						if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
							result = createEditModelAndView(feedbackForm, excp.getMessage());
						} else {
							result = createEditModelAndView(feedbackForm, "commit.error");
						}
					}
				}
			} catch(Throwable excp) {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		} else {
			result = createEditModelAndView(feedbackForm);
		}
		
		return result;
	}
	
	// Delete ------------------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="delete")
	public ModelAndView delete(@ModelAttribute(value="feedback") FeedbackForm feedbackForm, BindingResult binding) {
		ModelAndView result;
		
		try {
			Feedback feedback = feedbackService.findOneForEdit(feedbackForm.getId());
			try {
				feedbackService.delete(feedback);
				result = new ModelAndView("redirect:/feedback/list.do?eventId="+feedback.getEvent().getId());
			} catch(Throwable excp) {
				if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
					result = createEditModelAndView(feedbackForm, excp.getMessage());
				} else {
					result = createEditModelAndView(feedbackForm,"commit.error");
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Ancillary methods -------------------------------------
	
	private ModelAndView createEditModelAndView(FeedbackForm feedbackForm) {
		return createEditModelAndView(feedbackForm, null);
	}
	
	private ModelAndView createEditModelAndView(FeedbackForm feedbackForm, String message) {
		ModelAndView result;
		
		result = new ModelAndView("feedback/edit");
		result.addObject("feedback",feedbackForm);
		result.addObject("message",message);
		
		return result;
	}

}
