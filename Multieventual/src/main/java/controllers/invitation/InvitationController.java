package controllers.invitation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import services.ActorService;
import services.AlertService;
import services.EventService;
import services.InvitationService;
import services.UserService;
import services.utils.ServicesUtils;
import controllers.AbstractController;
import domain.Actor;
import domain.Event;
import domain.Invitation;
import domain.User;
import domain.utils.AlertStatus;
import domain.utils.EventStatus;
import forms.InvitationForm;

@Controller
@RequestMapping("/invitation")
public class InvitationController extends AbstractController {
	
	// Constructor --------------------------------------
	
	public InvitationController() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private InvitationService invitationService;
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AlertService alertService;
	
	@Autowired
	private ActorService actorService;
	
	// List received ------------------------------------
	
	@RequestMapping("/listReceived")
	public ModelAndView listReceived() {
		ModelAndView result;
		Collection<Invitation> invitations;
		
		try {
			User user = servicesUtils.getRegisteredUser(User.class);
			invitations = new ArrayList<Invitation>(user.getInvitations());
			
			result = new ModelAndView("invitation/listReceived");
			result.addObject("invitations",invitations);
			result.addObject("received",true);
			result.addObject("requestUri","/invitation/listReceived.do");
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// List sent ----------------------------------------
	
	@RequestMapping("/listSent")
	public ModelAndView listSent(@RequestParam(required=false) Integer eventId) {
		ModelAndView result;
		
		try {
			User user = servicesUtils.getRegisteredUser(User.class);
			Event event = eventService.findOne(eventId);
			
			Assert.notNull(event);
			Assert.isTrue(event.getPublicationDate().before(new Date()));
			// S�lo el poseedor del evento podr� ver el estado de las invitaciones
			Assert.isTrue(event.getUser().getId() == user.getId());
			
			result = new ModelAndView("invitation/listSent");
			result.addObject("invitations",event.getInvitations());
			result.addObject("eventId",event.getId());
			result.addObject("sent",true);
			result.addObject("requestUri","/invitations/listSent.do");
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Accept -------------------------------------------
	
	@RequestMapping("/accept")
	public ModelAndView accept(@RequestParam(required=true) Integer invitationId,
			RedirectAttributes redirectAttributes) {
		ModelAndView result;
		
		try {
			Invitation invitation = invitationService.findOne(invitationId);
			invitationService.acceptInvitation(invitation);
			
			// Env�o de alertas
			try {
				alertService.generateAlert(invitation.getEvent().getUser(), AlertStatus.INVITATION_ACCEPTED, "http://www.acme.com/invitation/listSent.do?eventId="+invitation.getEvent().getId());
			} catch(Throwable excp) {
				
			}
			try {
				// Enviamos alertas a los amigos del usuario, ya que asistir� el evento
				Collection<Actor> actors = actorService.getFriends(servicesUtils.checkUser().getId());
				for(Actor actor : actors) {
					alertService.generateAlert(actor, AlertStatus.PURCHASE_FRIEND_EVENT, "http://www.acme.com/event/profile.do?eventId="+invitation.getEvent().getId());
				}
			} catch(Throwable excp) {
				// No causar� un error en la transacci�n de la aceptaci�n de la invitaci�n, por lo tanto se ignora el error.
			}
			
			result = new ModelAndView("redirect:/invitation/listReceived.do");
		} catch(Throwable excp) {
			if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
				result = new ModelAndView("redirect:/invitation/listReceived.do");
				// Guardamos un atributo flash, que se mantendr� en el redirect, que indica el error.
				redirectAttributes.addFlashAttribute("message",excp.getMessage());
			} else {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		}
		
		return result;
	}
	
	// Deny ----------------------------------------------------
	
	@RequestMapping("/deny")
	public ModelAndView deny(@RequestParam(required=true) Integer invitationId,
			RedirectAttributes redirectAttributes) {
		ModelAndView result;
		
		try {
			Invitation invitation = invitationService.findOne(invitationId);
			invitationService.denyInvitation(invitation);
			
			// Env�o de alertas
			try {
				alertService.generateAlert(invitation.getEvent().getUser(), AlertStatus.INVITATION_DENIED, "http://www.acme.com/invitation/listSent.do?eventId="+invitation.getEvent().getId());
			} catch(Throwable excp) {
				
			}			
			
			result = new ModelAndView("redirect:/invitation/listReceived.do");
		} catch(Throwable excp) {
			if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
				result = new ModelAndView("redirect:/invitation/listReceived.do");
				// Guardamos un atributo flash, que se mantendr� en el redirect, que indica el error.
				redirectAttributes.addFlashAttribute("message",excp.getMessage());
			} else {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		}
		
		return result;
	}
	
	// Create -------------------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		InvitationForm invitationForm;
		Event event;
		
		try {
			invitationForm = invitationService.construct(invitationService.create());
			
			event = eventService.findOne(eventId);
			Assert.isTrue(servicesUtils.getEventStatus(event) == EventStatus.SALE);
			invitationForm.setEvent(event);
			
			result = createEditModelAndView(invitationForm);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/mics/403.do");
		}
		
		return result;
	}
	
	// Save ---------------------------------------------------
	
	@RequestMapping(value="/edit", method=RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute(value="invitation") InvitationForm invitationForm, BindingResult binding) {
		ModelAndView result;
		
		if(!binding.hasErrors()) {
			try {
				Invitation invitation = invitationService.reconstruct(invitationForm, binding);
				if(!binding.hasErrors()) {
					try {
						invitation = invitationService.save(invitation);
						
						// Env�o de alertas
						try {
							// Enviamos al receptor la invitaci�n
							alertService.generateAlert(invitation.getUser(), AlertStatus.INVITATION_RECEIVED, "http://www.acme.com/invitation/listReceived.do");
						} catch(Throwable excp) {
							
						}
						
						result = new ModelAndView("redirect:/invitation/listSent.do?eventId="+invitation.getEvent().getId());
					} catch(Throwable excp) {
						if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
							result = createEditModelAndView(invitationForm, excp.getMessage());
						} else {
							result = createEditModelAndView(invitationForm, "commit.error");
						}
					}
				} else {
					result = createEditModelAndView(invitationForm);
				}
			} catch (Throwable excp) {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		} else {
			result = createEditModelAndView(invitationForm);
		}
		
		return result; 
	}
	
	
	// Ancillary methods --------------------------------------
	
	private ModelAndView createEditModelAndView(InvitationForm invitationForm) {
		return createEditModelAndView(invitationForm, null);
	}
	
	private ModelAndView createEditModelAndView(InvitationForm invitationForm, String message) {
		ModelAndView result;
		Collection<User> users;
		
		users = userService.getUsersThatHaveNotReceivedInvitationsToEvent(invitationForm.getEvent());
		
		result = new ModelAndView("invitation/edit");
		result.addObject("invitation",invitationForm);
		result.addObject("message",message);
		result.addObject("users",users);
		
		return result;
	}

}
