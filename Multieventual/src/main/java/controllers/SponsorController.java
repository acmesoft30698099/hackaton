package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SponsorService;
import services.utils.ServicesUtils;
import domain.Sponsor;
import forms.SponsorForm;
import forms.SponsorRegistrationForm;

@Controller
@RequestMapping("/sponsor")
public class SponsorController extends AbstractController {
	
	// Services ---------------------------------------------------------------

	@Autowired
	private SponsorService sponsorService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Constructors -----------------------------------------------------------
	
	public SponsorController() {
		super();
	}
		
	// Profile -----------------------------------------------------------------
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(@RequestParam(required = false) Integer id) {
		ModelAndView result = new ModelAndView("sponsor/profile");
		Sponsor sponsor;
		
		try {
			if (id == null) {
				sponsor = servicesUtils.checkRegistered(Sponsor.class, servicesUtils.checkUser());
				result.addObject("sponsor", sponsor);
				result.addObject("edit", true);
			}
			else {
				sponsor = sponsorService.findOne(id);
				result.addObject("sponsor", sponsor);
				try {
					sponsor = servicesUtils.checkRegistered(Sponsor.class, servicesUtils.checkUser());
					if (sponsor.getId() == id) {
						result.addObject("edit", true);
					}
				}
				catch (Throwable t) {
					// Usuario no registrado
				}
			}
		}
		catch (Throwable t) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		return result;
	}
	
	// Create ------------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		SponsorRegistrationForm form = sponsorService.constructRegistration(sponsorService.create());
		return createEditModelAndView(form);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@ModelAttribute(value="sponsor") @Valid SponsorRegistrationForm form, BindingResult binding) {
		ModelAndView result;
		Sponsor sponsor = sponsorService.reconstruct(form, binding);
		if (binding.hasErrors()) {
			if (!form.getUserAccount().getPassword().equals(form.getUserAccount().getRepeatPassword())) {
				binding.rejectValue("userAccount.repeatPassword", "password.dontmatch");
				result = createEditModelAndView(form);
			}
			else {
				result = createEditModelAndView(form);
			}
		}
		else {
			try {
				if (!form.getUserAccount().getPassword().equals(form.getUserAccount().getRepeatPassword())) {
					binding.rejectValue("userAccount.repeatPassword", "password.dontmatch");
					result = createEditModelAndView(form);
				}
				else {
					sponsor = sponsorService.save(sponsor);
					result = profile(sponsor.getId());
				}
			}
			catch (DataIntegrityViolationException excp) {
				// Este caso es un caso especial que solo ocurrir� cuando el UserAccount que ha puesto el usuario tiene el mismo 
				// username que uno ya existente, entonces se le informar� de esto
				binding.rejectValue("userAccount.username", "userAccount.already.used");
				result = createEditModelAndView(form);
			}
			catch (Throwable t) {
				result = createEditModelAndView(form, "commit.error");
			}
		}
		return result;
	}
	
	// Edit --------------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		Sponsor sponsor = servicesUtils.getRegisteredUser(Sponsor.class);
		SponsorForm form = sponsorService.construct(sponsor);
		return createEditModelAndView(form, sponsor.getId());
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView edit(@ModelAttribute(value="sponsor") SponsorForm form, BindingResult binding) {
		ModelAndView result;
		Sponsor sponsor = sponsorService.reconstruct(form, binding);
		
		if (binding.hasErrors()) {
			result = createEditModelAndView(form, sponsor.getId());
		}
		else {
			try {
				sponsor = sponsorService.save(sponsor);
				result = new ModelAndView("redirect:/sponsor/profile.do");
			} 
			catch (Throwable t) {
				result = createEditModelAndView(form, sponsor.getId(), "commit.error");
			}
		}
	
		return result;
	}
	
	// Ancillary Methods -------------------------------------------------------

	public ModelAndView createEditModelAndView(SponsorRegistrationForm form) {
		return createEditModelAndView(form, null);
	}
	
	public ModelAndView createEditModelAndView(SponsorRegistrationForm form, String message) {
		ModelAndView result = new ModelAndView("sponsor/create");
		result.addObject("sponsor", form);
		result.addObject("message", message);
		result.addObject("operation", "create");
		result.addObject("id", 0);

		return result;
	}
	
	public ModelAndView createEditModelAndView(SponsorForm form, int id) {
		return createEditModelAndView(form, id, null);
	}
	
	public ModelAndView createEditModelAndView(SponsorForm form, int id, String message) {
		ModelAndView result = new ModelAndView("sponsor/edit");
		result.addObject("sponsor", form);
		result.addObject("message", message);
		result.addObject("operation", "edit");
		result.addObject("id", id);

		return result;
	}
	
}
