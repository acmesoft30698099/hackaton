package controllers.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.CategoryService;
import services.EventService;
import services.SponsorService;
import services.UserService;
import controllers.AbstractController;

@Controller
@RequestMapping("/dashboard")
public class DashboardController extends AbstractController {
	
	// Constructor -------------------------------------------
	
	public DashboardController() {
		super();
	}
	
	// Services ----------------------------------------------
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private SponsorService sponsorService;
	
	// Dashboard ---------------------------------------------
	
	@RequestMapping("/show")
	public ModelAndView show() {
		ModelAndView result;
		
		result = new ModelAndView("dashboard/show");
		
		result.addObject("categoryAndNumberEvents",categoryService.getCategoriesAndTheNumberOfEvents());
		result.addObject("numberEventsPublishedRecently", eventService.getEventsPublishedWithin7Days());
		result.addObject("numberFinishedEventsWith10Assistants",eventService.getFinishedEventsWithMoreThanTenAssistants());
		result.addObject("top5FinishedEventsByNumberAssistants",eventService.getTop5FinishedEventsWithMoreAssistants());
		result.addObject("numberFinishedEventsWithFeedback5OrHigher",eventService.getFinishedEventsWithFeedback5OrHigher());
		result.addObject("ratioUsersWithOneOrMoreEvents",eventService.getRatioUsersWithEvents());
		result.addObject("top5UsersByEventsPublishedAndTheNumber", userService.getTop5OfUsersByPublishedEvents());
		result.addObject("top5SponsorsByNumberOfAcceptedContracts", sponsorService.getTop5SponsorsWithMoreAcceptedContracts());
		result.addObject("numberAndRatioOfBannedUsers",userService.getNumberAndAverageOfBannedUsers());
		result.addObject("averageRatioOfMoneyGainedForEventsInCategories",categoryService.getCategoriesAndTheAverageAmountOfMoneyGained());
		result.addObject("usersThatSpentMoreThan60PercentInTickets",userService.getUsersWithPurchasesHigherThan60Percent());
		result.addObject("averageEventsOnSaleOrInProgressInCategories",categoryService.getAverageOfEventsOnSaleOrOnProgressForCategory());
		result.addObject("eventsWhoseSumContractsAreHigherThan75Percent",eventService.getEventsWithContractsValuedHigherThan75Percent());
		
		return result;
	}

}
