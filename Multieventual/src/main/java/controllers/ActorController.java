package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import security.UserAccountService;
import services.ActorService;
import services.AdministratorService;
import services.SponsorService;
import services.UserService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Administrator;
import domain.Sponsor;
import domain.User;
import forms.ActorConfigurationForm;

@Controller
@RequestMapping("/actor")
public class ActorController extends AbstractController {
	
	// Services -----------------------------------------

	@Autowired
	private UserAccountService userAccountService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SponsorService sponsorService;
	
	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Constructor --------------------------------------

	public ActorController() {
		super();
	}
	
	// Profile -----------------------------------------------------------------
	
	@RequestMapping(value="/profile", method=RequestMethod.GET)
	public ModelAndView profile(@RequestParam(required = false) Integer id) {
		ModelAndView result;
		Actor actor;
		try {
			if (id == null) {
				actor = servicesUtils.checkUser();
				if (actor instanceof User) {
					result = new ModelAndView("redirect:/user/profile.do");
				}
				else if(actor instanceof Sponsor) {
					result = new ModelAndView("redirect:/sponsor/profile.do");
				}
				else if(actor instanceof Administrator) {
					result = new ModelAndView("redirect:/administrator/profile.do");
				}
				else {
					result = new ModelAndView("redirect:/misc/403.do");
				}
			}
			else {
				actor = actorService.findById(id);
				if (actor instanceof User) {
					result = new ModelAndView("redirect:/user/profile.do?id="+id);
				}
				else if (actor instanceof Sponsor) {
					result = new ModelAndView("redirect:/sponsor/profile.do?id="+id);
				}
				else if (actor instanceof Administrator) {
					result = new ModelAndView("redirect:/administrator/profile.do?id="+id);
				}
				else {
					result = new ModelAndView("redirect:/misc/403.do");
				}
			}
		}
		catch (Throwable t) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		return result;
	}
	
	// Edit --------------------------------------------------------------------
	
	@RequestMapping(value = "/configuration/edit", method = RequestMethod.GET)
	public ModelAndView configurationEdit() {
		Actor actor = actorService.findActorByUserAccount(LoginService.getPrincipal());
		ActorConfigurationForm form = actorService.construct(actor);
		return createEditModelAndView(form);
	}
	
	@RequestMapping(value = "/configuration/edit", method = RequestMethod.POST, params = "save_ua")
	public ModelAndView configurationUASave(@ModelAttribute(value = "configuration") @Valid ActorConfigurationForm form, BindingResult binding) {
		ModelAndView result;
		UserAccount userAccount = userAccountService.reconstruct(form, binding);
		
		if (binding.hasErrors()) {
			if (!form.getUserAccount().getPassword().equals(form.getUserAccount().getRepeatPassword())) {
				binding.rejectValue("userAccount.repeatPassword", "password.dontmatch");
				result = createEditModelAndView(form);
			}
			else {
				result = createEditModelAndView(form);
			}
		}
		else {
			try {
				if (!form.getUserAccount().getPassword().equals(form.getUserAccount().getRepeatPassword())) {
					binding.rejectValue("userAccount.repeatPassword", "password.dontmatch");
					result = createEditModelAndView(form);
				}
				else {
					userAccountService.save(userAccount);
					result = profile(null);
				}
			}
			catch (DataIntegrityViolationException excp) {
				// Este caso es un caso especial que solo ocurrir� cuando el UserAccount que ha puesto el usuario tiene el mismo 
				// username que uno ya existente, entonces se le informar� de esto
				binding.rejectValue("userAccount.username", "userAccount.already.used");
				result = createEditModelAndView(form);
			}
			catch (Throwable t) {
				result = createEditModelAndView(form, "commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/configuration/edit", method = RequestMethod.POST, params = "save_config")
	public ModelAndView configurationSave(@ModelAttribute(value = "configuration") ActorConfigurationForm form, BindingResult binding) {
		ModelAndView result = null;
		Actor actor = servicesUtils.checkUser();

		if (actor instanceof User) {
			User user = userService.reconstruct(form, binding);
			if (binding.hasErrors()) {
				result = createEditModelAndView(form);
			}
			else {
				try {
					user = userService.save(user);
					result = new ModelAndView("redirect:/user/profile.do?id="+user.getId());
				}
				catch (Throwable t) {
					result = createEditModelAndView(form, "commit.error");
				}
			}
		}
		else if (actor instanceof Sponsor) {
			Sponsor sponsor = sponsorService.reconstruct(form, binding);
			if (binding.hasErrors()) {
				result = createEditModelAndView(form);
			}
			else {
				try {
					sponsor = sponsorService.save(sponsor);
					result = new ModelAndView("redirect:/sponsor/profile.do?id="+sponsor.getId());
				}
				catch (Throwable t) {
					result = createEditModelAndView(form, "commit.error");
				}
			}
		}
		else if (actor instanceof Administrator) {
			Administrator administrator = administratorService.reconstruct(form, binding);
			if (binding.hasErrors()) {
				result = createEditModelAndView(form);
			}
			else {
				try {
					administrator = administratorService.save(administrator);
					result = new ModelAndView("redirect:/administrator/profile.do?id="+administrator.getId());
				}
				catch (Throwable t) {
					result = createEditModelAndView(form, "commit.error");
				}
			}
		}
		else {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/configuration/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@ModelAttribute(value = "configuration")ActorConfigurationForm form, BindingResult binding) {
		ModelAndView result;
		Actor actor = servicesUtils.checkUser();
		
		try {
			if (actor instanceof User) {
				userService.delete((User) actor);
			}
			else if (actor instanceof Sponsor) {
				sponsorService.delete((Sponsor) actor);
			}
			else if (actor instanceof Administrator) {
				administratorService.delete((Administrator) actor);
			}
			result = new ModelAndView("redirect:/j_spring_security_logout");
		}
		catch (Throwable oops) {
			result = createEditModelAndView(form, "commit.error");
		}

		return result;
	}
	
	// Ancillary Methods -------------------------------------------------------
	
	public ModelAndView createEditModelAndView(ActorConfigurationForm form) {
		return createEditModelAndView(form, null);
	}
	
	public ModelAndView createEditModelAndView(ActorConfigurationForm form, String message) {
		ModelAndView result = new ModelAndView("actor/configuration/edit");
		result.addObject("configuration", form);
		result.addObject("message", message);

		return result;
	}
	
}
