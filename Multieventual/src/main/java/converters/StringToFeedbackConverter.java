package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.FeedbackRepository;

import domain.Feedback;

@Component
@Transactional
public class StringToFeedbackConverter extends StringToGenericConverter<Feedback, FeedbackRepository>{

}
