package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Topic;

@Component
@Transactional
public class TopicToStringConverter extends GenericToStringConverter<Topic> {

}
