package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Feedback;

@Component
@Transactional
public class FeedbackToStringConverter extends GenericToStringConverter<Feedback>{

}
