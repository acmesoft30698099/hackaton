package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.CategoryRepository;

import domain.Category;

@Component
@Transactional
public class StringToCategoryConverter extends StringToGenericConverter<Category, CategoryRepository>{

}
