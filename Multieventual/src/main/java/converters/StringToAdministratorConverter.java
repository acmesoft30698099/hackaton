package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Administrator;

import repositories.AdministratorRepository;

@Component
@Transactional
public class StringToAdministratorConverter extends StringToGenericConverter<Administrator, AdministratorRepository>{

}
