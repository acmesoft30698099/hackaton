package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.InvitationRepository;

import domain.Invitation;

@Component
@Transactional
public class StringToInvitationConverter extends StringToGenericConverter<Invitation, InvitationRepository>{

}
