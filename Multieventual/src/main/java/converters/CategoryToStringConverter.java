package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Category;

@Component
@Transactional
public class CategoryToStringConverter extends GenericToStringConverter<Category>{

}
