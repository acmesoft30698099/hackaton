package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Ticket;

@Component
@Transactional
public class TicketToStringConverter extends GenericToStringConverter<Ticket>{

}
