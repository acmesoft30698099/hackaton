package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.UserRepository;

import domain.User;

@Component
@Transactional
public class StringToUserConverter extends StringToGenericConverter<User, UserRepository>{

}
