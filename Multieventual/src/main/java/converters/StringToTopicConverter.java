package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.TopicRepository;

import domain.Topic;

@Component
@Transactional
public class StringToTopicConverter extends StringToGenericConverter<Topic, TopicRepository>{

}
