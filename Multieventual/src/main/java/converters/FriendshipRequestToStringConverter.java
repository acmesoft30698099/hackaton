package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.FriendshipRequest;

@Component
@Transactional
public class FriendshipRequestToStringConverter extends GenericToStringConverter<FriendshipRequest>{
	
}
