package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Invitation;

@Component
@Transactional
public class InvitationToStringConverter extends GenericToStringConverter<Invitation>{

}
