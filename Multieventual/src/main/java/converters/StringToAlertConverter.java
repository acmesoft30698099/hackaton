package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Alert;

import repositories.AlertRepository;

@Component
@Transactional
public class StringToAlertConverter extends StringToGenericConverter<Alert, AlertRepository>{

}
