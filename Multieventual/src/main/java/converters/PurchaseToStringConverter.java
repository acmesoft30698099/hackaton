package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Purchase;

@Component
@Transactional
public class PurchaseToStringConverter extends GenericToStringConverter<Purchase>{

}
