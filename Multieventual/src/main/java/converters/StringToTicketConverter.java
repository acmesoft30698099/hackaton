package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.TicketRepository;

import domain.Ticket;

@Component
@Transactional
public class StringToTicketConverter extends StringToGenericConverter<Ticket, TicketRepository>{

}
