package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.EventRepository;
import domain.Event;

@Component
@Transactional
public class StringToEventConverter extends StringToGenericConverter<Event, EventRepository>{

}
