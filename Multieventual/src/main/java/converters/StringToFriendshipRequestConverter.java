package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.FriendshipRequestRepository;

import domain.FriendshipRequest;

@Component
@Transactional
public class StringToFriendshipRequestConverter extends StringToGenericConverter<FriendshipRequest, FriendshipRequestRepository>{

}
