package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.User;

@Component
@Transactional
public class UserToStringConverter extends GenericToStringConverter<User>{

}
