package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Event;

@Component
@Transactional
public class EventToStringConverter extends GenericToStringConverter<Event>{

}
