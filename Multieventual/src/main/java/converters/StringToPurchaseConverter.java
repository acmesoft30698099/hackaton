package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.PurchaseRepository;

import domain.Purchase;

@Component
@Transactional
public class StringToPurchaseConverter extends StringToGenericConverter<Purchase, PurchaseRepository>{

}
