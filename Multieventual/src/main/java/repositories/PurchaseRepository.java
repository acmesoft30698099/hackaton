package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Purchase;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Integer>{
	
	@Query("select purchase from Purchase purchase where purchase.ticket.event.id = ?2 and purchase.user.id = ?1")
	Purchase getPurchaseOfUserToEvent(Integer userId, Integer eventId);
	
	@Query("select case when (count(*) > 0) then true else false end from Purchase purchase join purchase.ticket ticket where ticket.event.id = ?2 and purchase.user.id = ?1")
	Boolean getIfUserHasPurchasedTicketFromEvent(Integer userId, Integer eventId);

}
