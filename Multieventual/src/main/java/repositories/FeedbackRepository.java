package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Feedback;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, Integer>{
	
	@Query("select feedback.id from Feedback feedback where feedback.user.id = ?1 and feedback.event.id = ?2")
	Integer getIdOfFeedbackOfUserForEvent(Integer userId, Integer eventId);

}
