package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer>{
	
	@Query("select actor from Actor actor where actor.userAccount.id = ?1")
	Actor findByUserAccount(int id);
	
	@Query("select a from Actor a where a.id = ?1")
	Actor findById(int id);
	
	@Query("select friendship.receiver from FriendshipRequest friendship where friendship.sender.id = ?1 and friendship.state like 'ACCEPTED'")
	Collection<Actor> friendshipReceived(int id);
	
	@Query("select friendship.sender from FriendshipRequest friendship where friendship.receiver.id = ?1 and friendship.state like 'ACCEPTED'")
	Collection<Actor> friendshipSended(int id);

}
