
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	@Query("select c from Comment c where c.quoted.id=?1")
	Collection<Comment> getCommentsQuoting(int quotedId);
	
	@Query("select c from Comment c join c.topic t where t.id = ?1")
	Collection<Comment> getCommentsOfTopic(Integer topicId);

}
