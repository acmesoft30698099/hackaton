package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Invitation;

@Repository
public interface InvitationRepository extends JpaRepository<Invitation, Integer>{
	
	@Query("select case when(count(*) > 0) then true else false end from Invitation invitation where invitation.user.id = ?1 and invitation.event.id = ?2")
	Boolean getIfUserReceivedInvitationFromEvent(Integer userId, Integer eventId);
	
}
