package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
	
	@Query("select case when (count(*) > 0) then true else false end from Ticket ticket join ticket.purchases purchase where ticket.event.id = ?1 and purchase.user.id = ?2")
	Boolean getIfUserHasPurhcasedTicketFromEvent(Integer eventId, Integer userId);
	
	@Query("select event from Event event where event.endingDate < ?1")
	Collection<Event> getFinishedEvents(Date now);
	
	@Query("select event from Event event where event.endingDate < ?1 and event.category.name like concat('%',?2,'%')")
	Collection<Event> getFinishedEventsWithCategory(Date now, String categoryName);
	
	@Query("select event from Event event where event.startingDate < ?1 and ?1 < event.endingDate")
	Collection<Event> getInProgressEvents(Date now);
	
	@Query("select event from Event event where event.startingDate < ?1 and CURRENT_DATE < event.endingDate and event.category.name like concat('%',?2,'%')")
	Collection<Event> getInProgressEventsWithCategory(Date now, String categoryName);
	
	@Query("select event from Event event where event.publicationDate < ?1 and ?1 < event.startingDate")
	Collection<Event> getSaleEvents(Date now);
	
	@Query("select event from Event event where event.publicationDate < ?1 and ?1 < event.startingDate and event.category.name like concat('%',?2,'%')")
	Collection<Event> getSaleEventsWithCategory(Date now, String categorydName);
	
	@Query("select event from Event event where event.publicationDate < ?1 and (event.name like concat('%',?2,'%') or event.description like concat('%',?2,'%')) and event.celebrationAddress like concat('%',?3,'%'))")
	Collection<Event> getEventsByKeywordAndAddress(Date now, String keyword, String address);
	
	@Query("select event from Event event where (event.startingDate < ?1 and ?1 < event.endingDate) or (event.startingDate > ?1 and event.startingDate < ?2)")
	Collection<Event> getInProgressEventsAndWithinSevenDays(Date now, Date sevenDays);

	@Query("select count(event) from Event event where event.publicationDate < ?1 and event.publicationDate > ?2")
	Integer getEventsPublicatedInSevenDays(Date now, Date sevenDays);
	
	@Query("select count(event) from Event event join event.tickets ticket where (ticket.purchases.size + event.invitations.size) > 10 and event.endingDate > ?1")
	Integer getFinishedEventsWithMoreThanTenAssistants(Date now);
	
	@Query("select event,(ticket.purchases.size + event.invitations.size) from Event event join event.tickets ticket where event.endingDate > ?1 group by event order by (ticket.purchases.size + event.invitations.size)")
	Collection<Object[]> getFinishedEventsWithMoreAssistants(Date now);
	
	@Query("select count(event1) from Event event1 where (select avg(feedback.score) from Feedback feedback where feedback.event.id = event1.id) >= 5 and event1.endingDate < ?1")
	Integer getFinishedEventsWithFeedback5OrMore(Date now);
	
	@Query("select avg(userWithEvent)/(select count(users) from User users) from Event event join event.user userWithEvent")
	Double getRatioUsersWithEvents();
	
	@Query("select event from Event event join event.contracts contract, Contract contractTotal group by event having sum(contract.offeredPayment) >= avg(contractTotal.offeredPayment)")
	Collection<Event> getEventsWithContractsValuedHigherThan75Percent();

}
