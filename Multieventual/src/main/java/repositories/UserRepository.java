package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	@Query("select user from User user where user not in (select invitation.user from Invitation invitation where invitation.event.id = ?1)")
	Collection<User> getUsersThatHaveNotReceivedInvitationsToEvent(Integer eventId);
	
	@Query("select user, count(event) from User user, Event event where event.user.id = user.id and event.publicationDate > ?1 group by user order by count(event) desc")
	Collection<Object[]> getUsersWithPublishedEventsOrdered(Date now);
	
	@Query("select count(user) from User user where user.ban = true")
	Integer getNumberUsersBanned();
	
	@Query("select count(user) from User user")
	Integer getNumberUsers();
	
	@Query("select user from User user join user.purchases purchase join purchase.ticket ticket group by user having sum(ticket.price) > (select (sum(ticket.price)/count(users))*0.6 from User user join user.purchases purchase join purchase.ticket ticket, User users)")
	Collection<User> getUsersWithPurchasesHigherThan60Percent();
	
	@Query("select user from User user where user.name like concat('%',?1,'%') and user.surname like concat('%',?2,'%') and user.postalAddress like concat('%',?3,'%')")
	Collection<User> getUsersBy(String name, String surname, String address);
	
	@Query("select user from User user where user.name like concat('%',?1,'%') and user.surname like concat('%',?2,'%')")
	Collection<User> getUsersBy(String name, String surname);
}
