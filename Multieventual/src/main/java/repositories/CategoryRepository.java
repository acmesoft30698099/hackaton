package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{
	
	@Query("select category, category.events.size from Category category")
	Collection<Object[]> getCategoriesAndTheNumberOfEvents();
	
	@Query("select category, sum(ticket.purchases.size*ticket.price)/category.events.size from Category category join category.events event join event.tickets ticket group by category")
	Collection<Object[]> getCategoriesAndTheAverageAmountOfMoneyGained();
	
	@Query("select category, (count(event)*1.0)/(category.events.size*1.0) from Category category, Event event where event.category.id = category.id and event.saleStartDate < ?1 and event.endingDate > ?1 group by category")
	Collection<Object[]> getAverageOfEventsOnSaleOrOnProgressForCategory(Date now);

	@Query("select case when(count(event) > 0) then true else false end from Event event where event.category.id = ?1")
	Boolean getIfExistsEventsThatDependOn(Integer categoryId);

}
