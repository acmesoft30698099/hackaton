
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Topic;
import forms.TopicRow;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Integer> {

	@Query("select c.actor from Comment c where c.topic.id=?1 and c.publicationDate=(select min(c.publicationDate) from Comment c where c.topic.id=?1)")
	Actor getTopicCreator(int topicId);
	
	@Query("select new forms.TopicRow(t.id, t.title, t.closed, c2.actor.id, c2.actor.name, c2.actor.surname, c.actor.id, c.actor.name, c.actor.surname, c.publicationDate) from Comment c join c.topic t join t.event e, Comment c2 where e.id = ?1 and c.publicationDate = (select max(c3.publicationDate) from Comment c3 join c3.topic t3 where t3.id = t.id) and c2.publicationDate = (select min(c3.publicationDate) from Comment c3 join c3.topic t3 where t3.id = t.id) group by t.id order by c.publicationDate desc")
	Collection<TopicRow> getTopicsOfEvent(int eventId);

}
