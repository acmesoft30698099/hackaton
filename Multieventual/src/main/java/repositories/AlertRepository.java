package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Alert;

@Repository
public interface AlertRepository extends JpaRepository<Alert, Integer>{

	@Query("select alert from Alert alert where alert._Read = true and alert.actor.id = ?1")
	Collection<Alert> getAlertsRead(int actorid);
	
	@Query("select alert from Alert alert where alert._Read = false and alert.actor.id = ?1")
	Collection<Alert> getAlertsUnRead(int actorid);
	
}
