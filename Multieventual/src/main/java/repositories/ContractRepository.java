package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Contract;
import domain.Image;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Integer> {
	
	@Query("select case when (count(contract) > 0) then true else false end from Contract contract where contract.sponsor.id = ?1 and contract.event.id = ?2 and not contract.state like 'DENIED'")
	Boolean getIfExitstPendingOrAcceptedContractsBetweenSponsorAndEvent(Integer sponsorId, Integer eventId);
	
	@Query("select contract from Contract contract where contract.event.id = ?1 and contract.sponsor.id = ?2")
	Contract getExistingContractBetweenSponsorAndEvent(Integer eventId, Integer sponsorId);
	
	@Query("select contract from Contract contract join contract.event event where contract.sponsor.id = ?1 and event.startingDate < ?2 and contract.state like 'PENDING'")
	Collection<Contract> getPendingContractsFromSponsorThatAreInvalid(Integer sponsorId, Date now);
	
	@Query("select contract from Contract contract where contract.sponsor.id = ?1")
	Collection<Contract> getContractsFromSponsor(Integer sponsorId);
	
	@Query("select contract from Contract contract join contract.event event where event.id = ?1 and event.startingDate < ?2 and contract.state like 'PENDING'")
	Collection<Contract> getPendingContractsFromEventThatAreInvalid(Integer eventId, Date now);
	
	@Query("select contract from Contract contract where contract.event.id = ?1")
	Collection<Contract> getContractsFromEvent(Integer eventId);
	
	@Query("select contract.sponsor.banner from Contract contract where contract.event.id = ?1 and contract.state like 'ACCEPTED'")
	Collection<Image> getImagesFromAcceptedContractsOfEvent(Integer eventId);
	
	@Query("select contract from Contract contract where contract.state = 'PENDING_PAYMENT' and contract.sponsor.id = ?1")
	Collection<Contract> getPaymentPendingContractsOfSponsor(Integer sponsorId);
}
