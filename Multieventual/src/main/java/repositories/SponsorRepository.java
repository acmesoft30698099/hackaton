package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Sponsor;

@Repository
public interface SponsorRepository extends JpaRepository<Sponsor, Integer> {
	
	@Query("select sponsor, count(contract) from Sponsor sponsor, Contract contract where contract.sponsor.id = sponsor.id and contract.state like 'ACCEPTED' group by sponsor order by count(contract) desc")
	Collection<Object[]> getSponsorsWithMoreAcceptedContracts();

}
