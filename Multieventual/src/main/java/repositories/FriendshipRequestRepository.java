package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.FriendshipRequest;

@Repository
public interface FriendshipRequestRepository extends JpaRepository<FriendshipRequest, Integer>{
	
	@Query("select case when (count(friendship) > 0) then true else false end from FriendshipRequest friendship where (friendship.receiver.id = ?1 and friendship.sender.id = ?2) or (friendship.receiver.id = ?2 and friendship.sender.id = ?1)")
	Boolean getIfExistsFriendshipRequestBetweenUsers(Integer actorId1, Integer actorId2);
	
	@Query("select request from FriendshipRequest request where request.receiver.id = ?1 and request.state like 'PENDING'")
	Collection<FriendshipRequest> getPendingRequestsFromUser(Integer actorId);
	
	@Query("select request from FriendshipRequest request where (request.receiver.id = ?1 or request.sender.id = ?1) and request.state = 'ACCEPTED'")
	Collection<FriendshipRequest> getAcceptedRequestsFromUser(Integer actorId);
	
	@Query("select friendship.receiver from FriendshipRequest friendship where friendship.sender.id = ?1")
	Collection<Actor> getUsersThatReceivedARequestFromUser(Integer actorId);
	
	@Query("select friendship.sender from FriendshipRequest friendship where friendship.receiver.id = ?1")
	Collection<Actor> getUsersThatSentARequestToUser(Integer actorId);
	
}
