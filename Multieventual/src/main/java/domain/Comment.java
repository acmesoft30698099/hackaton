package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Comment extends DomainEntity{
	
	// Constructor --------------------------------------

	public Comment() {
		super();
	}
	
	// Attributes ------------------------------------------------
	
	private String contentText;
	private Date publicationDate;
	private Date lastEdition;
	private boolean modifiedByAdministrator;
	
	@NotBlank
	public String getContentText() {
		return contentText;
	}
	public void setContentText(String contentText) {
		this.contentText = contentText;
	}
	
	@Past
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}
	
	@Past
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getLastEdition() {
		return lastEdition;
	}
	public void setLastEdition(Date lastEdition) {
		this.lastEdition = lastEdition;
	}
	
	public boolean isModifiedByAdministrator() {
		return modifiedByAdministrator;
	}
	public void setModifiedByAdministrator(boolean modifiedByAdministrator) {
		this.modifiedByAdministrator = modifiedByAdministrator;
	}
	
	// Relationships ------------------------------------------------
	
	private Actor actor;
	private Topic topic;
	private Comment quoted;
	

	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Actor getActor() {
		return actor;
	}
	public void setActor(Actor actor) {
		this.actor = actor;
	}
	
	@NotNull
	@Valid
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	public Topic getTopic() {
		return topic;
	}
	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	
	@Valid
	@ManyToOne(optional = true)
	public Comment getQuoted() {
		return quoted;
	}
	public void setQuoted(Comment quoted) {
		this.quoted = quoted;
	}
}
