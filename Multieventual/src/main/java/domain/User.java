package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor {
	
	// Constructor --------------------------------------

	public User() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private CreditCard creditCard;
	private boolean ban;
	
	@Valid
	public CreditCard getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	
	public boolean isBan() {
		return ban;
	}
	
	public void setBan(boolean ban) {
		this.ban = ban;
	}
	
	// Relationships ------------------------------------
	
	private Collection<Event> events;
	private Collection<Feedback> feedbacks;
	private Collection<Invitation> invitations;
	private Collection<Purchase> purchases;

	@Valid
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	public Collection<Event> getEvents() {
		return events;
	}
	public void setEvents(Collection<Event> events) {
		this.events = events;
	}
	
	@Valid
	@OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
	public Collection<Feedback> getFeedbacks() {
		return feedbacks;
	}
	public void setFeedbacks(Collection<Feedback> feedbacks) {
		this.feedbacks = feedbacks;
	}
	
	@Valid
	@OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
	public Collection<Invitation> getInvitations() {
		return invitations;
	}
	public void setInvitations(Collection<Invitation> invitations) {
		this.invitations = invitations;
	}
	
	@Valid
	@OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
	public Collection<Purchase> getPurchases() {
		return purchases;
	}
	public void setPurchases(Collection<Purchase> purchases) {
		this.purchases = purchases;
	}

}
