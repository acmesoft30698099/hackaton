package domain.utils;

public enum EventStatus {
	DRAFT, PROMOTIONAL, SALE, IN_PROGRESS, ENDED;
}
