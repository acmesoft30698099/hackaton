package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class FriendshipRequest extends DomainEntity {
	
	// Constructor --------------------------------------

	public FriendshipRequest() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private String text;
	private Date sentDate;
	private String state;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	@NotNull
	@Past
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	
	@Pattern(regexp="^(ACCEPTED)|(DENIED)|(PENDING)$")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	// Relationships ------------------------------------
	
	private Actor receiver;
	private Actor sender;

	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Actor getReceiver() {
		return receiver;
	}
	public void setReceiver(Actor receiver) {
		this.receiver = receiver;
	}
	
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Actor getSender() {
		return sender;
	}
	public void setSender(Actor sender) {
		this.sender = sender;
	}
}
