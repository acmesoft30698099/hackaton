package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Access(AccessType.PROPERTY)
public class Event extends DomainEntity {
	
	// Constructor --------------------------------------

	public Event() {
		super();
	}
	
	// Attributes ---------------------------------------

	private String name;
	private String description;
	private String celebrationAddress;
	private GeoCoordinate celebrationCoordinate;
	private Date publicationDate;
	private Date saleStartDate;
	private Date startingDate;
	private Date endingDate;
	private Collection<Image> pictures;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@NotBlank
	public String getCelebrationAddress() {
		return celebrationAddress;
	}
	public void setCelebrationAddress(String celebrationAddress) {
		this.celebrationAddress = celebrationAddress;
	}
	
	@NotNull
	@Valid
	public GeoCoordinate getCelebrationCoordinate() {
		return celebrationCoordinate;
	}
	public void setCelebrationCoordinate(GeoCoordinate celebrationCoordinate) {
		this.celebrationCoordinate = celebrationCoordinate;
	}
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getSaleStartDate() {
		return saleStartDate;
	}
	public void setSaleStartDate(Date saleStartDate) {
		this.saleStartDate = saleStartDate;
	}
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getStartingDate() {
		return startingDate;
	}
	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getEndingDate() {
		return endingDate;
	}
	public void setEndingDate(Date endingDate) {
		this.endingDate = endingDate;
	}
	
	@Valid
	@ElementCollection
	public Collection<Image> getPictures() {
		return pictures;
	}
	public void setPictures(Collection<Image> pictures) {
		this.pictures = pictures;
	}
	
	// Relationships ------------------------------------
	
	private User user;
	private Collection<Feedback> feedbacks;
	private Collection<Invitation> invitations;
	private Collection<Ticket> tickets;
	private Category category;
	private Collection<Topic> topics;
	private Collection<Contract> contracts;

	@JsonIgnore
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@JsonIgnore
	@Valid
	@OneToMany(mappedBy = "event", cascade = CascadeType.REMOVE)
	public Collection<Feedback> getFeedbacks() {
		return feedbacks;
	}
	public void setFeedbacks(Collection<Feedback> feedbacks) {
		this.feedbacks = feedbacks;
	}
	
	@JsonIgnore
	@Valid
	@OneToMany(mappedBy = "event", cascade = CascadeType.REMOVE)
	public Collection<Invitation> getInvitations() {
		return invitations;
	}
	public void setInvitations(Collection<Invitation> invitations) {
		this.invitations = invitations;
	}
	
	@JsonIgnore
	@NotNull
	@Valid
	@OneToMany(mappedBy = "event", cascade = {CascadeType.REMOVE, CascadeType.MERGE})
	public Collection<Ticket> getTickets() {
		return tickets;
	}
	public void setTickets(Collection<Ticket> tickets) {
		this.tickets = tickets;
	}
	
	@JsonIgnore
	@NotNull
	@Valid
	@ManyToOne(optional = false, cascade = CascadeType.REFRESH)
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	@JsonIgnore
	@Valid
	@OneToMany(mappedBy = "event", cascade = CascadeType.REMOVE)
	public Collection<Topic> getTopics() {
		return topics;
	}
	public void setTopics(Collection<Topic> topics) {
		this.topics = topics;
	}
	
	@JsonIgnore
	@Valid
	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "event", fetch = FetchType.LAZY)
	public Collection<Contract> getContracts() {
		return contracts;
	}
	public void setContracts(Collection<Contract> contracts) {
		this.contracts = contracts;
	}
	
}
