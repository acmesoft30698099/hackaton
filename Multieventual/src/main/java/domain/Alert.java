package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Alert extends DomainEntity {
	
	// Constructor --------------------------------------

	public Alert() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private String textInfo;
	private Date generationDate;
	private String link;
	private boolean _read;
	
	@NotBlank
	public String getTextInfo() {
		return textInfo;
	}
	public void setTextInfo(String textInfo) {
		this.textInfo = textInfo;
	}
	
	@NotNull
	@Past
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getGenerationDate() {
		return generationDate;
	}
	public void setGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
	}
	
	@URL
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	public boolean get_Read() {
		return _read;
	}
	public void set_Read(boolean read) {
		this._read = read;
	}
	
	// Relationships ------------------------------------
	
	private Actor actor;

	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Actor getActor() {
		return actor;
	}
	public void setActor(Actor actor) {
		this.actor = actor;
	}

}
