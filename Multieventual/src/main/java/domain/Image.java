package domain;

import javax.persistence.AccessType;
import javax.persistence.Access;
import javax.persistence.Embeddable;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

@Embeddable
@Access(AccessType.PROPERTY)
public class Image {
	
	// Constructor --------------------------------------

	public Image() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private String url;

	@URL
	@NotBlank
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
