package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import security.UserAccount;

@Entity
@Access(AccessType.PROPERTY)
public class Actor extends DomainEntity {

	// Constructor --------------------------------------

	public Actor() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private String name;
	private String surname;
	private String email;
	private String phone;
	private String postalAddress;
	private String profilePicture;
	private boolean allowFriendshipRequests;
	private String messagesConfiguration;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@NotBlank
	@Email
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Pattern(regexp = "^(\\+[0-9]{1,3})?\\s?(\\([0-9]{3}\\)\\s)?([a-zA-Z0-9]){4,}(([\\s\\-])([a-zA-Z0-9]){4,})*?$|(^$)")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	
	@URL
	public String getProfilePicture() {
		return profilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
	
	public boolean isAllowFriendshipRequests() {
		return allowFriendshipRequests;
	}
	public void setAllowFriendshipRequests(boolean allowFriendshipRequests) {
		this.allowFriendshipRequests = allowFriendshipRequests;
	}
	
	@Pattern(regexp = "^(NOBODY)|(ONLY_FRIENDS)|(EVERYBODY)$")
	public String getMessagesConfiguration() {
		return messagesConfiguration;
	}
	public void setMessagesConfiguration(String messagesConfiguration) {
		this.messagesConfiguration = messagesConfiguration;
	}

	// Relationships ------------------------------------
	
	private UserAccount userAccount;
	private Collection<Message> receivedMessages;
	private Collection<FriendshipRequest> receivedFriendshipsRequests;
	private Collection<FriendshipRequest> sentFriendshipsRequests;
	private Collection<Alert> alerts;
	
	@NotNull
	@Valid
	@OneToOne(cascade = CascadeType.ALL)
	public UserAccount getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	@Valid
	@OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	public Collection<Message> getReceivedMessages() {
		return receivedMessages;
	}
	public void setReceivedMessages(Collection<Message> receivedMessages) {
		this.receivedMessages = receivedMessages;
	}
	
	@Valid
	@OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	public Collection<FriendshipRequest> getReceivedFriendshipsRequests() {
		return receivedFriendshipsRequests;
	}
	public void setReceivedFriendshipsRequests(
			Collection<FriendshipRequest> receivedFriendshipsRequests) {
		this.receivedFriendshipsRequests = receivedFriendshipsRequests;
	}
	
	@Valid
	@OneToMany(mappedBy = "sender", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	public Collection<FriendshipRequest> getSentFriendshipsRequests() {
		return sentFriendshipsRequests;
	}
	public void setSentFriendshipsRequests(
			Collection<FriendshipRequest> sentFriendshipsRequests) {
		this.sentFriendshipsRequests = sentFriendshipsRequests;
	}
	
	@Valid
	@OneToMany(mappedBy = "actor", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	public Collection<Alert> getAlerts() {
		return alerts;
	}
	public void setAlerts(Collection<Alert> alerts) {
		this.alerts = alerts;
	}
	
}
