package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Contract extends DomainEntity {
	
	// Constructor --------------------------------------

	public Contract() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private String termsAndConditions;
	private double offeredPayment;
	private String state;
	
	@NotBlank
	public String getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	
	@Min(0)
	public double getOfferedPayment() {
		return offeredPayment;
	}
	public void setOfferedPayment(double offeredPayment) {
		this.offeredPayment = offeredPayment;
	}
	
	@Pattern(regexp = "^(ACCEPTED)|(DENIED)|(PENDING)|(PAYMENT_PENDING)$")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	// Relationships ------------------------------------
	
	private Sponsor sponsor;
	private Event event;
	
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Sponsor getSponsor() {
		return sponsor;
	}
	public void setSponsor(Sponsor sponsor) {
		this.sponsor = sponsor;
	}
	
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}

}
