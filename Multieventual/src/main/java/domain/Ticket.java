package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Ticket extends DomainEntity {
	
	// Constructor --------------------------------------

	public Ticket() {
		super();
	}
	
	// Attributes ---------------------------------------
	
	private String name;
	private String description;
	private double price;
	private Integer remainingUnits;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Min(0)
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Min(0)
	public Integer getRemainingUnits() {
		return remainingUnits;
	}
	public void setRemainingUnits(Integer remainingUnits) {
		this.remainingUnits = remainingUnits;
	}

	// Relationships ------------------------------------
	
	private Collection<Purchase> purchases;
	private Event event;

	@OneToMany(mappedBy = "ticket", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	public Collection<Purchase> getPurchases() {
		return purchases;
	}
	public void setPurchases(Collection<Purchase> purchases) {
		this.purchases = purchases;
	}
	
	@Valid
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
}
