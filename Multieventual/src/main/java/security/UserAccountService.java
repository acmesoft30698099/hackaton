package security;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import services.GenericService;
import forms.ActorConfigurationForm;
import forms.UserAccountForm;

@Service
@Transactional
public class UserAccountService extends GenericService<UserAccountRepository, UserAccount> {

	// Supporting services ----------------------------------------------------


	// Constructor ------------------------------------------------------------

	public UserAccountService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public UserAccount create() {
		UserAccount result = new UserAccount();
		result.setAuthorities(new ArrayList<Authority>());
		return result;
	}
	
	protected UserAccount reglaNegocioSave(UserAccount userAccount) {
		Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
		userAccount.setPassword(md5PasswordEncoder.encodePassword(userAccount.getPassword(), null));
		return userAccount;
	}
	
	@Override
	public void delete(UserAccount userAccount) {
		throw new UnsupportedOperationException();
	}
	
	// Other business methods -------------------------------------------------
	
	public UserAccountForm construct(UserAccount userAccount) {
		UserAccountForm form = new UserAccountForm();
		form.setUsername(userAccount.getUsername());
		form.setPassword("");
		form.setRepeatPassword("");
		return form;
	}
	
	public UserAccount reconstruct(UserAccountForm form, BindingResult binding) {
		UserAccount result = create();
		try {
			UserAccount original = LoginService.getPrincipal();
			result.setId(original.getId());
			result.setVersion(original.getVersion());
			result.setAuthorities(original.getAuthorities());
		}
		catch (Throwable t) {
			// Se est� creando un nuevo actor
		}
		
		result.setUsername(form.getUsername());
		result.setPassword(form.getPassword());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public UserAccount reconstruct(ActorConfigurationForm form, BindingResult binding) {
		UserAccount result = create();
		UserAccount original = LoginService.getPrincipal();
		
		result.setId(original.getId());
		result.setVersion(original.getVersion());
		result.setAuthorities(original.getAuthorities());
		
		result.setUsername(form.getUserAccount().getUsername());
		result.setPassword(form.getUserAccount().getPassword());
		
		binding.setNestedPath("userAccount");
		
		validator.validate(result, binding);
		
		binding.setNestedPath("");
		
		return result;
	}
	
	public UserAccount findByUsername(String username) {
		return repository.findByUsername(username);
	}
	
}
