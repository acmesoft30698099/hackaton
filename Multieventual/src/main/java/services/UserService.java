package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.UserRepository;
import security.Authority;
import security.UserAccountService;
import services.utils.BannedUserException;
import utilities.Tuple;
import domain.Actor;
import domain.Administrator;
import domain.Alert;
import domain.Event;
import domain.Feedback;
import domain.FriendshipRequest;
import domain.Invitation;
import domain.Message;
import domain.Purchase;
import domain.User;
import forms.ActorConfigurationForm;
import forms.UserForm;
import forms.UserRegistrationForm;

@Service
@Transactional
public class UserService extends GenericService<UserRepository, User> {
	
	// Supporting services ----------------------------------------------------
	
	@Autowired
	private UserAccountService userAccountService;

	// Constructor ------------------------------------------------------------

	public UserService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public User create() {
		User result = new User();
		result.setAlerts(new ArrayList<Alert>());
		result.setReceivedFriendshipsRequests(new ArrayList<FriendshipRequest>());
		result.setReceivedMessages(new ArrayList<Message>());
		result.setSentFriendshipsRequests(new ArrayList<FriendshipRequest>());
		result.setAllowFriendshipRequests(true);
		result.setMessagesConfiguration("EVERYBODY");
		result.setUserAccount(userAccountService.create());
		result.setBan(false);
		
		result.setEvents(new ArrayList<Event>());
		result.setPurchases(new ArrayList<Purchase>());
		result.setInvitations(new ArrayList<Invitation>());
		result.setFeedbacks(new ArrayList<Feedback>());
		
		return result;
	}
	
	public User create(User original) {
		User result = new User();		
		result.setId(original.getId());
		result.setVersion(original.getVersion());
		
		result.setName(original.getName());
		result.setSurname(original.getSurname());
		result.setEmail(original.getEmail());
		result.setPhone(original.getPhone());
		result.setPostalAddress(original.getPostalAddress());
		result.setProfilePicture(original.getProfilePicture());
		result.setAllowFriendshipRequests(original.isAllowFriendshipRequests());
		result.setMessagesConfiguration(original.getMessagesConfiguration());
		result.setCreditCard(original.getCreditCard());
		result.setBan(original.isBan());
		
		result.setUserAccount(original.getUserAccount());
		result.setReceivedMessages(original.getReceivedMessages());
		result.setReceivedFriendshipsRequests(original.getReceivedFriendshipsRequests());
		result.setSentFriendshipsRequests(original.getSentFriendshipsRequests());
		result.setAlerts(original.getAlerts());
		result.setEvents(original.getEvents());
		result.setPurchases(original.getPurchases());
		result.setInvitations(original.getInvitations());
		result.setFeedbacks(original.getFeedbacks());
		
		return result;
	}
	
	@Override
	protected User reglaNegocioSave(User user) {
		try {
			servicesUtils.getRegisteredUser(Administrator.class);
		}
		catch (Throwable t) {
			// No se trata de un administrador cambiando el ban del usuario
			
			User registered = null;
			try {
				registered = servicesUtils.getRegisteredUser(User.class);
			}
			catch (Throwable t2) {
				// Se va a guardar un nuevo usuario
				user.setUserAccount(userAccountService.save(user.getUserAccount()));
			}
			if (registered != null) {
				// Si hay un usuario registrado, nos aseguramos de que sea el propio usuario
				Assert.isTrue(registered.getId() == user.getId());
			}
		}
		return user;
	}
	
	@Override
	protected User reglaNegocioDelete(User user) {
		return findOneForEdit(user.getId());
	}
	
	// Queries ----------------------------------------
	
	public Collection<User> getUsersBy(String name, String surname, String address) {
		Collection<User> users;
		
		if(address.isEmpty()) {
			users = repository.getUsersBy(name, surname);
		} else {
			users = repository.getUsersBy(name, surname, address);
		}
		
		return users;
	}
	
	public Collection<User> getUsersThatHaveNotReceivedInvitationsToEvent(Event event) {
		return repository.getUsersThatHaveNotReceivedInvitationsToEvent(event.getId());
	}
	
	public Map<User,Long> getTop5OfUsersByPublishedEvents() {
		Map<User,Long> result = new HashMap<User, Long>();
		Collection<Object[]> query = repository.getUsersWithPublishedEventsOrdered(new Date());
		int max = 5;
		if(query.size() < 5) {
			max = query.size();
		}
		for(Object[] objs : query) {
			if(max == 0){
				break;
			}
			result.put((User) objs[0], (Long) objs[1]);
			max--;
		}
		
		return result;
	}
	
	public Tuple<Integer,Integer> getNumberAndAverageOfBannedUsers() {
		Tuple<Integer,Integer> tuple = new Tuple<Integer, Integer>();
		tuple.setObjA(repository.getNumberUsersBanned());
		tuple.setObjB(repository.getNumberUsersBanned()/repository.getNumberUsers());
		
		return tuple;
	}
	
	public Collection<User> getUsersWithPurchasesHigherThan60Percent() {
		return repository.getUsersWithPurchasesHigherThan60Percent();
	}
	
	
	// Other business methods -------------------------------------------------
	
	public void prohibitBannedUser() {
		boolean isBanned = false;
		// Comprueba si hay un usuario, y si �ste es un usuario baneado
		try {
			Actor actor = servicesUtils.checkUser();
			if(actor instanceof User) {
				
				isBanned = ((User)actor).isBan();
			}
		} catch(Throwable excp) {
			// No hacemos nada, el usuario puede no estar registrado
		}
		// Comprobamos la variable que indica si est� baneado
		if(isBanned) {
			// Lanzamos excepci�n, que la capturar� el AbstractController 
			throw new BannedUserException();
		}
	}
	
	public UserForm construct(User user) {
		UserForm form = new UserForm();
		form.setName(user.getName());
		form.setSurname(user.getSurname());
		form.setEmail(user.getEmail());
		form.setPhone(user.getPhone());
		form.setPostalAddress(user.getPostalAddress());
		form.setProfilePicture(user.getProfilePicture());
		return form;
	}
	
	public UserRegistrationForm constructRegistration(User user) {
		UserRegistrationForm form = new UserRegistrationForm();
		form.setName(user.getName());
		form.setSurname(user.getSurname());
		form.setEmail(user.getEmail());
		form.setPhone(user.getPhone());
		form.setPostalAddress(user.getPostalAddress());
		form.setProfilePicture(user.getProfilePicture());
		form.setUserAccount(userAccountService.construct(user.getUserAccount()));
		return form;
	}
	
	public User reconstruct(UserForm form, BindingResult binding) {
		User result = create(servicesUtils.getRegisteredUser(User.class));
		result.setName(form.getName());
		result.setSurname(form.getSurname());
		result.setEmail(form.getEmail());
		result.setPhone(form.getPhone());
		result.setPostalAddress(form.getPostalAddress());
		result.setProfilePicture(form.getProfilePicture());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public User reconstruct(UserRegistrationForm form, BindingResult binding) {
		User result = create();
		Authority authority = new Authority();
		authority.setAuthority(Authority.USER);
		result.getUserAccount().addAuthority(authority);
		result.getUserAccount().setUsername(form.getUserAccount().getUsername());
		result.getUserAccount().setPassword(form.getUserAccount().getPassword());
		result.setName(form.getName());
		result.setSurname(form.getSurname());
		result.setEmail(form.getEmail());
		result.setPhone(form.getPhone());
		result.setPostalAddress(form.getPostalAddress());
		result.setProfilePicture(form.getProfilePicture());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public User reconstruct(ActorConfigurationForm form, BindingResult binding) {
		User result = create(servicesUtils.getRegisteredUser(User.class));
		
		result.setAllowFriendshipRequests(Boolean.valueOf(form.getAllowFriendshipRequests()));
		result.setMessagesConfiguration(form.getMessagesConfiguration());
		
		validator.validate(result, binding);
		
		return result;
	}
	
}
