package services;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.AdministratorRepository;
import security.UserAccountService;
import domain.Administrator;
import domain.Alert;
import domain.FriendshipRequest;
import domain.Message;
import forms.ActorConfigurationForm;
import forms.AdministratorForm;

@Service
@Transactional
public class AdministratorService extends GenericService<AdministratorRepository, Administrator> {
	
	// Supporting services ----------------------------------------------------

	@Autowired
	private UserAccountService userAccountService;
	
	// Constructor ------------------------------------------------------------

	public AdministratorService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public Administrator create() {
		Administrator result = new Administrator();
		result.setAlerts(new ArrayList<Alert>());
		result.setReceivedFriendshipsRequests(new ArrayList<FriendshipRequest>());
		result.setReceivedMessages(new ArrayList<Message>());
		result.setSentFriendshipsRequests(new ArrayList<FriendshipRequest>());
		result.setUserAccount(userAccountService.create());
		
		return result;
	}
	
	public Administrator create(Administrator original) {
		Administrator result = new Administrator();		
		result.setId(original.getId());
		result.setVersion(original.getVersion());
		
		result.setName(original.getName());
		result.setSurname(original.getSurname());
		result.setEmail(original.getEmail());
		result.setPhone(original.getPhone());
		result.setPostalAddress(original.getPostalAddress());
		result.setProfilePicture(original.getProfilePicture());
		result.setAllowFriendshipRequests(original.isAllowFriendshipRequests());
		result.setMessagesConfiguration(original.getMessagesConfiguration());
		
		result.setUserAccount(original.getUserAccount());
		result.setReceivedMessages(original.getReceivedMessages());
		result.setReceivedFriendshipsRequests(original.getReceivedFriendshipsRequests());
		result.setSentFriendshipsRequests(original.getSentFriendshipsRequests());
		result.setAlerts(original.getAlerts());
		
		return result;
	}
	
	@Override
	protected Administrator reglaNegocioSave(Administrator admin) {
		Administrator registered = servicesUtils.getRegisteredUser(Administrator.class);
		Assert.isTrue(registered.getId() == admin.getId());
		
		return admin;
	}
	
	@Override
	protected Administrator reglaNegocioDelete(Administrator admin) {
		return findOneForEdit(admin.getId());
	}
	
	// Other business methods -------------------------------------------------
	
	public AdministratorForm construct(Administrator admin) {
		AdministratorForm form = new AdministratorForm();
		form.setName(admin.getName());
		form.setSurname(admin.getSurname());
		form.setEmail(admin.getEmail());
		form.setPhone(admin.getPhone());
		form.setPostalAddress(admin.getPostalAddress());
		form.setProfilePicture(admin.getProfilePicture());
		return form;
	}
	
	public Administrator reconstruct(AdministratorForm form, BindingResult binding) {
		Administrator result = create(servicesUtils.getRegisteredUser(Administrator.class));
		
		result.setName(form.getName());
		result.setSurname(form.getSurname());
		result.setEmail(form.getEmail());
		result.setPhone(form.getPhone());
		result.setPostalAddress(form.getPostalAddress());
		result.setProfilePicture(form.getProfilePicture());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Administrator reconstruct(ActorConfigurationForm form, BindingResult binding) {
		Administrator result = create(servicesUtils.getRegisteredUser(Administrator.class));
		result.setAllowFriendshipRequests(Boolean.valueOf(form.getAllowFriendshipRequests()));
		result.setMessagesConfiguration(form.getMessagesConfiguration());
		
		validator.validate(result, binding);
		
		return result;
	}

}
