package services;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.MessageRepository;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Message;
import forms.MessageForm;
import forms.MessageFormReply;

@Service
@Transactional
public class MessageService extends GenericService<MessageRepository, Message> {
	
	// Constructor ---------------------------------------
	
	public MessageService() {
		super();
	}
	
	// Services ------------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private Validator validator;
	
	// CRUD methods --------------------------------------
	
	public Message create() {
		Message message = new Message();
		message.setSentDate(new Date(new Date().getTime()-1000));
		return message;
	}
	
	public Message reglaNegocioGeneral(Message message) {
		// Comprobamos los permisos, el usuario que lo envie o recibe tiene que estar registrado
		Actor actor = servicesUtils.checkUser();
		Assert.isTrue(actor.getId() == message.getSender().getId() || actor.getId() == message.getReceiver().getId());
		if(actor.getId() == message.getSender().getId()){
			Assert.isTrue(actor.getId() != message.getReceiver().getId());
		}
		return message;
	}
	
	public Message reglaNegocioSave(Message message) {
		// Comprobamos que qui�n lo envia debe ser el usuario actual
		Actor actor = servicesUtils.checkUser();
		Assert.isTrue(actor.getId() == message.getSender().getId());
		Assert.isTrue(!message.getReceiver().getMessagesConfiguration().equals("NOBODY"), "bussinessRules.nobody");
		Assert.isTrue(!message.getReceiver().getMessagesConfiguration().equals("ONLY_FRIENDS") || actorService.getFriends(message.getReceiver().getId()).contains(actor), "bussinessRules.nobody");
		return message;
	}
	
	public Message reglaNegocioDelete(Message message) {
		// Comprobamos los permisos, el usuario que lo envie o recibe tiene que estar registrado
		Actor actor = servicesUtils.checkUser();
		Assert.isTrue(actor.getId() == message.getReceiver().getId());
		
		return message;
	}
	
	public Message reglaNegocioEdit(Message message) {
		// Comprobamos los permisos, el usuario que lo envie o recibe tiene que estar registrado
		Actor actor = servicesUtils.checkUser();
		Assert.isTrue(actor.getId() == message.getReceiver().getId());
		
		return message;
	}
	
	// Other methods ------------------------------------------
	
	public MessageForm construct(Message message) {
		MessageForm messageForm = new MessageForm();
		
		messageForm.setText(message.getText());
		messageForm.setTitle(message.getTitle());
		messageForm.setReceiver(message.getReceiver());
		
		return messageForm;
	}
	
	public Message reconstruct(MessageForm messageForm, BindingResult binding) {
		Message message = create();
		
		// No hay edici�n, por lo tanto se crear� desde cero siempre
		message.setTitle(messageForm.getTitle());
		message.setText(messageForm.getText());
		message.setSentDate(new Date(new Date().getTime()-1000));
		message.setReceiver(messageForm.getReceiver());
		// Obtenemos el usuario actual para el sender
		Actor actor = servicesUtils.checkUser();
		message.setSender(actor);
		
		validator.validate(message, binding);
		
		return message;
	}
	
	public MessageFormReply constructReply(Message message, Message original) {
		MessageFormReply messageForm = new MessageFormReply();
		
		//Ya que solo necesito eso, el resto de atributos son del otro mensaje, cuidado con los sender y receiver!
		messageForm.setRepliedText(message.getRepliedText());
		messageForm.setOriginal(original);
		
		return messageForm;
	}
	
	public Message reconstructReply(MessageFormReply mes, BindingResult binding) {
		Message message = create();
		Actor actor = servicesUtils.checkUser();

		Actor receiver = mes.getOriginal().getSender();
		
		Assert.isTrue(actor.getId() == mes.getOriginal().getReceiver().getId());
		
		message.setTitle(mes.getOriginal().getTitle());
		message.setText(mes.getOriginal().getText());
		message.setRepliedText(mes.getRepliedText());
		message.setSentDate(new Date(new Date().getTime()-1000));
		message.setReceiver(receiver);
		message.setSender(actor);
		
		if(message.getRepliedText() == null || message.getRepliedText().isEmpty()) {
			binding.rejectValue("repliedText", "org.hibernate.validator.constraints.NotBlank.message");
		}
		
		validator.validate(message, binding);
		
		return message;
	}
}
