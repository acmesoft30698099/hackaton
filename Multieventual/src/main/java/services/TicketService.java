package services;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.TicketRepository;
import domain.Purchase;
import domain.Ticket;
import domain.User;
import domain.utils.EventStatus;
import forms.TicketForm;
import forms.TicketFormCreate;

@Service
@Transactional
public class TicketService extends GenericService<TicketRepository, Ticket> {
	
	// Constructor --------------------------------------

	public TicketService() {
		super();
	}
	
	// Supporting services ------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	// CRUD methods ------------------------------------------------
	
	public Ticket create() {
		Ticket ticket = new Ticket();
		ticket.setPurchases(new ArrayList<Purchase>());
		
		return ticket;
	}
	
	public Ticket reglaNegocioGeneral(Ticket ticket) {
		// Comprobamos los permisos del usuario actual
		User user = servicesUtils.getRegisteredUser(User.class);
		// Comprobamos que posea el evento del ticket
		Assert.isTrue(user.getId() == ticket.getEvent().getUser().getId());
		// Para poder modificar el evento, debe tener un estado de no publicado o en promoci�n
		EventStatus status = servicesUtils.getEventStatus(ticket.getEvent());
		Assert.isTrue(status == EventStatus.DRAFT || status == EventStatus.PROMOTIONAL,"businessRule.event.promotion_state");
		return ticket;
	}
	
	public Ticket reglaNegocioDelete(Ticket ticket) {
		Assert.isTrue(ticket.getEvent().getTickets().size() > 1, "businessRule.tickets.not.empty");
		
		return ticket;
	}
	
	// Other methods ------------------------------------------------
	
	// Actualiza la entrada para que tenga una unidad menos, en el caso de que exista un l�mite de unidades
	public void updateTicket(Integer ticketId, Integer remainingUnits) {
		Ticket ticket = findOne(ticketId);
		if(ticket.getRemainingUnits() != null) {
			ticket.setRemainingUnits(remainingUnits);
			repository.save(ticket);
		}
	}
	
	public TicketForm construct(Ticket ticket) {
		TicketForm ticketForm = new TicketForm();
		ticketForm.setId(ticket.getId());
		ticketForm.setDescription(ticket.getDescription());
		ticketForm.setEvent(ticket.getEvent());
		ticketForm.setName(ticket.getName());
		ticketForm.setPrice(ticket.getPrice());
		ticketForm.setRemainingUnits(ticket.getRemainingUnits());
		
		return ticketForm;
	}
	
	public TicketFormCreate constructCreate(Ticket ticket) {
		TicketFormCreate ticketForm = new TicketFormCreate();
		ticketForm.setDescription(ticket.getDescription());
		ticketForm.setName(ticket.getName());
		ticketForm.setPrice(ticket.getPrice());
		ticketForm.setRemainingUnits(ticket.getRemainingUnits());
		
		return ticketForm;
	}
	
	public Ticket reconstruct(TicketFormCreate ticketForm, BindingResult binding) {
		Ticket ticket = create();
		
		ticket.setDescription(ticketForm.getDescription());
		ticket.setName(ticketForm.getName());
		ticket.setPrice(ticketForm.getPrice());
		ticket.setRemainingUnits(ticketForm.getRemainingUnits());
		
		return ticket;
	}
	
	public Ticket reconstruct(TicketForm ticketForm, BindingResult binding) {
		Ticket ticket = create();
		if(ticketForm.getId() > 0) {
			Ticket original = findOneForEdit(ticketForm.getId());
			
			ticket.setId(original.getId());
			ticket.setVersion(original.getVersion());
			ticket.setEvent(original.getEvent());
			ticket.setPurchases(original.getPurchases());
			ticket.setId(original.getId());
		} else {
			ticket.setEvent(ticketForm.getEvent());
		}
		
		ticket.setDescription(ticketForm.getDescription());
		ticket.setName(ticketForm.getName());
		ticket.setPrice(ticketForm.getPrice());
		ticket.setRemainingUnits(ticketForm.getRemainingUnits());
		
		validator.validate(ticket, binding);
		
		return ticket;
	}
}
