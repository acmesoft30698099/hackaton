package services;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.AlertRepository;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Alert;
import domain.utils.AlertStatus;


@Service
@Transactional
public class AlertService extends GenericService<AlertRepository, Alert>{
	
	// Constructor
	
	public AlertService(){
		super();
	}
	
	// Services ------------------------------------------

	@Autowired
	private ServicesUtils servicesUtils;
	
	// CRUD methods
	
	public Alert create() {
		Alert alert = new Alert();
		return alert;
	}
	
	public Alert reglaNegocioSave(Alert alert) {
		Assert.notNull(alert);
		return alert;
	}
	
	public Alert reglaNegocioDelete(Alert alert){
		Assert.notNull(alert);
		Actor actor = servicesUtils.checkUser();
		Assert.isTrue(alert.getActor().getId()==actor.getId());
		Assert.isTrue(alert.get_Read());
		return alert;
	}
	
	public Alert reglaNegocioEdit(Alert alert){
		Assert.notNull(alert);
		Actor actor = servicesUtils.checkUser();
		Assert.isTrue(alert.getActor().getId()==actor.getId());
		return alert;
	}
	
	// Other methods
	
	/**
	 * Genera una alerta/notificación para el actor que se introduce en los parámetros de entrada.
	 * @param actor Es el actor que recibirá la notificación.
	 * @param alertStatus Es el tipo de notificación específico.
	 * @param link atributo opcional que se introducirá solo en aquellas notificaciones que sean necesarias. (por defecto introducir null)
	 * @return La notificación creada.
	 */
	public Alert generateAlert(Actor actor, AlertStatus alertStatus, String link) {
		Alert result = create();
		result.set_Read(false);
		result.setGenerationDate(new Date(new Date().getTime() - 1000));
		Assert.notNull(alertStatus);
		Assert.notNull(actor);
		result.setActor(actor);
		result.setTextInfo(alertStatus.toString());
		result.setLink(link);

		result = save(result);
		return result;
	}
	
	public Collection<Alert> getAlertsRead (int actorid){
		return repository.getAlertsRead(actorid);
	}
	
	public Collection<Alert> getAlertsUnRead (int actorid){
		return repository.getAlertsUnRead(actorid);
	}
	
}