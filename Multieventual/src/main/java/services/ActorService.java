package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import repositories.ActorRepository;
import security.UserAccount;
import security.UserAccountService;
import domain.Actor;
import forms.ActorConfigurationForm;

@Service
@Transactional
public class ActorService extends GenericService<ActorRepository, Actor> {

	// Supporting services ----------------------------------------------------
	
	@Autowired
	private UserAccountService userAccountService;
	
	// Constructor ------------------------------------------------------------

	public ActorService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------

	
	// Other business methods -------------------------------------------------
	
	public ActorConfigurationForm construct(Actor actor) {
		ActorConfigurationForm form = new ActorConfigurationForm();
		form.setUserAccount(userAccountService.construct(actor.getUserAccount()));
		form.setAllowFriendshipRequests(String.valueOf(actor.isAllowFriendshipRequests()));
		form.setMessagesConfiguration(actor.getMessagesConfiguration());
		return form;
	}
	
	/**
	 * Busca al Actor a trav�s de su UserAccount.
	 * @param user El UserAccount del Actor
	 * @return El Actor al que pertenece la UserAccount.
	 */
	public Actor findActorByUserAccount(UserAccount user){
		return repository.findByUserAccount(user.getId());
	}
	
	/**
	 * M�todo alternativo a findOne para encontrar a un Actor, ya que el otro
	 * puede dar problemas al tratar con clases abstractas.
	 * @param id ID del Actor.
	 * @return El Actor correspondiente a la ID.
	 */
	public Actor findById(int id) {
		servicesUtils.checkId(id);
		return repository.findById(id);
	}
	
	
	public Collection<Actor> getFriends(int actorid){
		Collection<Actor> friends = new ArrayList<Actor>();
		friends.addAll(repository.friendshipReceived(actorid));
		friends.addAll(repository.friendshipSended(actorid));
		return friends;
	}
}
