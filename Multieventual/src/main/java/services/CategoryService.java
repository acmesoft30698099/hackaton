package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.CategoryRepository;
import domain.Administrator;
import domain.Category;
import domain.Event;
import forms.CategoryForm;

@Service
@Transactional
public class CategoryService extends GenericService<CategoryRepository, Category> {
	
	// Constructor ------------------------------
	
	public CategoryService() {
		super();
	}
	
	// Supporting services ----------------------

	// CRUD methods -----------------------------
	
	public Category create() {
		Category category = new Category();
		category.setEvents(new ArrayList<Event>());
		
		return category;
	}
	
	public Category reglaNegocioSave(Category category) {
		// Comprobamos que el usuario es admin
		servicesUtils.getRegisteredUser(Administrator.class);
		
		return category;
	}
	
	public Category reglaNegocioDelete(Category category) {
		// Comprobamos si existen eventos que dependen de  esta
		Assert.isTrue(!repository.getIfExistsEventsThatDependOn(category.getId()), "businessRules.category_with_events");
		
		return category;
	}
	
	// Queries ------------------------------------------
	
	public Map<Category,Integer> getCategoriesAndTheNumberOfEvents() {
		Collection<Object[]> query = repository.getCategoriesAndTheNumberOfEvents();
		Map<Category,Integer> map = new HashMap<Category, Integer>();
		for(Object[] objs : query) {
			map.put((Category) objs[0], (Integer) objs[1]);
		}
		return map;
	}
	
	public Map<Category,Double> getCategoriesAndTheAverageAmountOfMoneyGained() {
		Map<Category,Double> map = new HashMap<Category,Double>();
		Collection<Object[]> query = repository.getCategoriesAndTheAverageAmountOfMoneyGained();
		for(Object[] objs : query) {
			map.put((Category) objs[0], (Double) objs[1]);
		}
		
		return map;
	}
	
	public Map<Category,Double> getAverageOfEventsOnSaleOrOnProgressForCategory() {
		Map<Category,Double> map = new HashMap<Category, Double>();
		Collection<Object[]> query = repository.getAverageOfEventsOnSaleOrOnProgressForCategory(new Date());
		for(Object[] objs : query) {
			map.put((Category) objs[0],(Double) objs[1]);
		}
		
		return map;
	}	
	
	// Other methods ----------------------------
	
	public CategoryForm construct(Category category) {
		CategoryForm categoryForm = new CategoryForm();
		categoryForm.setId(category.getId());
		categoryForm.setDescription(category.getDescription());
		categoryForm.setName(category.getName());

		return categoryForm;
	}
	
	public Category reconstruct(CategoryForm categoryForm, BindingResult binding) {
		Category category = create();
		
		if(categoryForm.getId() > 0) {
			// Obtenemos el objeto original
			Category original = findOneForEdit(categoryForm.getId());
			
			// Asignamos valores immutables
			category.setId(original.getId());
			category.setVersion(original.getVersion());
			category.setEvents(original.getEvents());
		}
		
		category.setName(categoryForm.getName());
		category.setDescription(categoryForm.getDescription());
		
		validator.validate(category, binding);
		
		return category;
	}
}
