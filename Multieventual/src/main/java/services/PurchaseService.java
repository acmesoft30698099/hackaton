package services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.PurchaseRepository;
import services.utils.ServicesUtils;
import domain.Event;
import domain.Purchase;
import domain.User;
import domain.utils.EventStatus;


@Service
@Transactional
public class PurchaseService extends GenericService<PurchaseRepository, Purchase>{

	// Constructor --------------------------------------
	
	public PurchaseService() {
		super();
	}

	// Supporting services ------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private InvitationService invitationService;
	
	@Autowired
	private TicketService ticketService;

	// CRUD methods -------------------------------------
	
	public Purchase create() {
		Purchase purchase = new Purchase();
		
		return purchase;
	}
	
	public Purchase reglaNegocioDelete(Purchase purchase) {
		// Obtenemos el usuario actual
		User user = servicesUtils.getRegisteredUser(User.class);
		// Comprobamos que es el poseedor del evento
		Assert.isTrue(purchase.getUser().getId() == user.getId());
		// Comprobamos que el evento sigue en estado de compra.
		Assert.isTrue(servicesUtils.getEventStatus(purchase.getTicket().getEvent()) == EventStatus.SALE);
		
		return purchase;
	}
	
	public Purchase reglaNegocioSave(Purchase purchase) {
		// Comprobamos primero que el usuario sea el que compre el ticket
		User user = servicesUtils.getRegisteredUser(User.class);
		Assert.isTrue(user.getId() == purchase.getUser().getId());
		// Comprobamos que el usuario actual no es el creador, ya que se asume que est� invitado al evento
		Assert.isTrue(purchase.getTicket().getEvent().getUser().getId() != user.getId());
		// Comprobamos que el evento est� en la fase correcta
		Assert.isTrue(servicesUtils.getEventStatus(purchase.getTicket().getEvent()) == EventStatus.SALE);
		// Comprobamos que el ticket tenga unidades restantes
		Assert.isTrue(purchase.getTicket().getRemainingUnits() == null || purchase.getTicket().getRemainingUnits() > 0);
		// Comprobamos que el usuario no haya comprado o haya sido invitado al evento
		Assert.isTrue(!(getIfUserHasPurchasedTicketFromEvent(user, purchase.getTicket().getEvent()) ||
				invitationService.getIfUserReceivedInvitationFromEvent(user, purchase.getTicket().getEvent())));
		// Si el ticket no es gratuito:
		if(purchase.getTicket().getPrice() != 0d) {
			// El usuario debe tener una tarjeta de cr�dito y �sta debe ser v�lida
			Assert.isTrue(user.getCreditCard() != null && servicesUtils.validCreditCard(user.getCreditCard()));
		}
		
		return purchase;
	}
	
	// Queries ------------------------------------------
	
	public Boolean getIfUserHasPurchasedTicketFromEvent(User user, Event event) {
		return repository.getIfUserHasPurchasedTicketFromEvent(user.getId(), event.getId());
	}
	
	public Purchase getPurchaseOfUserToEvent(User user, Event event) {
		return repository.getPurchaseOfUserToEvent(user.getId(), event.getId());
	}	
	
	// Other methods ------------------------------------
	
	// Este es un m�todo especial usado para la eliminaci�n de la compra al invitar al usuario
	public void deleteForInvitation(Purchase purchase) {
		repository.delete(purchase);
		// Si se efectua sin errores
		// Incrementamos el ticket y la cantidad en venta
		ticketService.updateTicket(purchase.getTicket().getId(), purchase.getTicket().getRemainingUnits() + 1);
	}
	
}
