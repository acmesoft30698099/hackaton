package services;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import domain.Contract;
import domain.Event;
import domain.Image;
import domain.Sponsor;
import domain.User;
import domain.utils.EventStatus;
import forms.ContractForm;

import repositories.ContractRepository;
import services.utils.ServicesUtils;

@Service
@Transactional
public class ContractService extends GenericService<ContractRepository, Contract>{
	
	// Constructor ----------------------------------------
	
	public ContractService() {
		super();
	}
	
	// Services -------------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// CRUD methods ---------------------------------------
	
	public Contract create() {
		Contract contract = new Contract();
		
		return contract;
	}
	
	public Contract reglaNegocioSave(Contract contract) {
		// Si se est� creando
		if(contract.getId() == 0) {
			// Obtenemos el usuario
			Sponsor sponsor = servicesUtils.getRegisteredUser(Sponsor.class);
			// Vemos que es el creador
			Assert.isTrue(contract.getSponsor().getId() == sponsor.getId());
			// Comprobamos que el evento objetivo del contrato es v�lido
			Assert.isTrue(servicesUtils.getEventStatus(contract.getEvent()) == EventStatus.SALE);
			// Comprobamos que no exista un contrato del usuario en modo pendiente o aceptado
			Assert.isTrue(!repository.getIfExitstPendingOrAcceptedContractsBetweenSponsorAndEvent(sponsor.getId(), contract.getEvent().getId()));
		} else {
			// Se est� editando
			User user = servicesUtils.getRegisteredUser(User.class);
			// Comprobamos que el usuario actual es el poseedor del evento
			Assert.isTrue(user.getId() == contract.getEvent().getUser().getId());
			// Comprobamos que el contrato original ...
			Contract original = findOne(contract.getId());
			// Estaba en estado pendiente
			Assert.isTrue(original.getState().equals("PENDING"));
			// Y el evento sigue en venta
			if(servicesUtils.getEventStatus(original.getEvent()) != EventStatus.SALE) {
				// Si no lo est�, aprovechamos y lo rechazamos
				original.setState("DENIED");
				// Rechazamos el cambio tambi�n.
				Assert.isTrue(false);
			}
		}
		
		return contract;
	}
	
	public Contract reglaNegocioEdit(Contract contract) {
		// Como s�lo se pueden plantear contratos, se usar� este m�todo para restringir al poseedor del evento
		User user = servicesUtils.getRegisteredUser(User.class);
		// El usuario debe poseer el evento
		Assert.isTrue(user.getId() == contract.getEvent().getUser().getId());
		// El contrato est� en modo pendiente
		Assert.isTrue(contract.getState().equals("PENDING"));
		// Y el evento sigue en venta
		if(servicesUtils.getEventStatus(contract.getEvent()) != EventStatus.SALE) {
			// Ajustamos el estado, que seguramente proviene de la BD y por lo tanto est� el objeto en cach�
			contract = findOne(contract.getId());
			contract.setState("DENIED");
		}
		
		return contract;
	}
	
	// Queries --------------------------------------------
	
	public Boolean getIfExitstPendingOrAcceptedContractsBetweenSponsorAndEvent(Sponsor sponsor, Event event) {
		return repository.getIfExitstPendingOrAcceptedContractsBetweenSponsorAndEvent(sponsor.getId(), event.getId());
	}
	
	public Contract getExistingContractBetweenSponsorAndEvent(Sponsor sponsor, Event event) {
		Contract original = repository.getExistingContractBetweenSponsorAndEvent(event.getId(), sponsor.getId());
		// Comprobamos que el evento siga en venta y el estado sea pendiente
		if(servicesUtils.getEventStatus(original.getEvent()) != EventStatus.SALE
				&& original.getState().equals("PENDING")) {
			// Actualizamos el estado del contrato
			original.setState("DENIED");
		}
		
		return original;
	}
	
	public Collection<Contract> getContractsForSponsor(Sponsor sponsor) {
		// Primero, vamos a obtener los contratos que tengan eventos fuera de eventa y est�n pendientes
		Collection<Contract> contracts = repository.getPendingContractsFromSponsorThatAreInvalid(sponsor.getId(), new Date());
		for(Contract contract : contracts) {
			// Le asignamos el estado de denegado, al pasar el tiempo de aceptaci�n
			contract.setState("DENIED");
		}
		// Finalmente, retornamos los contratos del sponsor como tal
		return repository.getContractsFromSponsor(sponsor.getId());
	}
	
	public Collection<Contract> getContractsForEvent(Event event) {
		Collection<Contract> contracts;
		// Primero, vemos si el evento est� en fase de venta
		if(servicesUtils.getEventStatus(event) == EventStatus.SALE) {
			// En cuyo caso, devolvemos los contratos sin m�s
			contracts = event.getContracts();
		} else {
			// Primero, vemos si existen contratos pendindientes.
			Collection<Contract> invalids = repository.getPendingContractsFromEventThatAreInvalid(event.getId(), new Date());
			for(Contract contract : invalids) {
				contract.setState("DENIED");
			}
			// Despues, retornamos los contratos
			contracts = repository.getContractsFromEvent(event.getId());
		}
		
		return contracts;
	}
	
	public Collection<Image> getImagesFromAcceptedContractsOfEvent(Event event) {
		return repository.getImagesFromAcceptedContractsOfEvent(event.getId());
	}
	
	// Other methods --------------------------------------
	
	public ContractForm construct(Contract contract) {
		ContractForm contractForm = new ContractForm();
		contractForm.setEvent(contract.getEvent());
		contractForm.setOfferedPayment(contract.getOfferedPayment());
		contractForm.setTermsAndConditions(contract.getTermsAndConditions());
		
		return contractForm;
	}
	
	public Contract reconstruct(ContractForm contractForm, BindingResult binding) {
		Contract contract = create();
		
		// Asignaci�n de valores immutables
		contract.setState("PENDING");
		contract.setSponsor(servicesUtils.getRegisteredUser(Sponsor.class));
		
		// Asignaci�n de valores del formulario
		contract.setEvent(contractForm.getEvent());
		contract.setOfferedPayment(contractForm.getOfferedPayment());
		contract.setTermsAndConditions(contractForm.getTermsAndConditions());
		
		validator.validate(contract, binding);
		
		return contract;
	}
	
	// Usado por el controlador de tarjeta de cr�dito al efectuar el correcto guardado de una tarjeta de cr�dito
	public void updatePendingPayment() {
		Sponsor sponsor = servicesUtils.getRegisteredUser(Sponsor.class);
		// Obtenemos los contratos en modo de pago pendiente de este usuario
		Collection<Contract> contracts = repository.getPaymentPendingContractsOfSponsor(sponsor.getId());
		// Para cada uno de los contratos
		for(Contract contract : contracts) {
			// Le asignamos el estado a aceptado
			contract.setState("ACCEPTED");
			// TODO Comprobar que con esto es suficiente
		}
	}

}
