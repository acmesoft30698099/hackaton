package services;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.InvitationRepository;
import domain.Event;
import domain.Invitation;
import domain.Purchase;
import domain.User;
import domain.utils.EventStatus;
import forms.InvitationForm;

@Service
@Transactional
public class InvitationService extends GenericService<InvitationRepository, Invitation>{
	
	// Constructor -------------------------------------
	
	public InvitationService() {
		super();
	}
	
	// Supporting services -----------------------------

	@Autowired
	private PurchaseService purchaseService;
	
	// CRUD methods ------------------------------------
	
	public Invitation create() {
		Invitation invitation = new Invitation();
		
		return invitation;
	}
	
	public Invitation reglaNegocioGeneral(Invitation invitation) {
		// Si se est� creando
		if(invitation.getId() == 0) {
			// El usuario actual tiene que ser el creador del evento y el que env�a la solicitud
			User user = servicesUtils.getRegisteredUser(User.class);
			Assert.isTrue(invitation.getEvent().getUser().getId() == user.getId());
			// No podr� invitarse a si mismo al evento, esto se asume.
			Assert.isTrue(invitation.getUser().getId() != user.getId(),"businessRule.invite.self"); // TODO A�adir a messages
		// En otro caso, se est� editando
		} else {
			// El usuario que est� registrado debe ser el que recibi� la invitaci�n
			User user = servicesUtils.getRegisteredUser(User.class);
			Assert.isTrue(invitation.getUser().getId() == user.getId());
		}
		
		return invitation;
	}
	
	// Queries ------------------------------------------
	
	public Boolean getIfUserReceivedInvitationFromEvent(User user, Event event) {
		return repository.getIfUserReceivedInvitationFromEvent(user.getId(), event.getId());
	}
	
	// Other methods ------------------------------------
	
	public void acceptInvitation(Invitation invitation) {
		// Comprobamos los permisos del usuario
		User user = servicesUtils.getRegisteredUser(User.class);
		// Comprobamos que el usuario es el mismo que la invitaci�n
		Assert.isTrue(invitation.getUser().getId() == user.getId());
		// Comprobamos el evento
		Event event = invitation.getEvent();
		EventStatus status = servicesUtils.getEventStatus(event);
		// Quizas sea demasiada comprobaci�n
		Assert.isTrue(status == EventStatus.SALE || status == EventStatus.PROMOTIONAL, "businessRule.incorrect.status.invitation");
		// Una vez comprobado la validez de la solicitud, se acepta la invitaci�n.
		invitation.setState("ACCEPTED");
		// Eliminamos la compra correspondiente
		// Primero obtenemos la compra
		Purchase purchase = purchaseService.getPurchaseOfUserToEvent(invitation.getUser(), invitation.getEvent());
		// Si existe...
		if(purchase != null) {
			// Eliminamos la compra
			purchaseService.deleteForInvitation(purchase);
		}
	}
	
	public void denyInvitation(Invitation invitation) {
		// Comprobamos los permisos del usuario
		User user = servicesUtils.getRegisteredUser(User.class);
		// Comprobamos que el usuario es el mismo que la invitaci�n
		Assert.isTrue(invitation.getUser().getId() == user.getId());
		// Comprobamos el evento
		Event event = invitation.getEvent();
		EventStatus status = servicesUtils.getEventStatus(event);
		// Quizas sea demasiada comprobaci�n
		Assert.isTrue(status == EventStatus.SALE || status == EventStatus.PROMOTIONAL, "businessRule.incorrect.status.invitation");
		// Una vez comprobado la validez de la solicitud, se niega la invitaci�n.
		invitation.setState("DENIED");	
	}
	
	public InvitationForm construct(Invitation invitation) {
		InvitationForm invitationForm = new InvitationForm();
		invitationForm.setUser(invitation.getUser());
		invitationForm.setText(invitation.getText());
		invitationForm.setEvent(invitation.getEvent());
		
		return invitationForm;
	}
	
	public Invitation reconstruct(InvitationForm invitationForm, BindingResult binding) {
		Invitation invitation = create();
		
		// Comprobamos los permisos del usuario
		User user = servicesUtils.getRegisteredUser(User.class);
		Assert.isTrue(invitationForm.getEvent().getUser().getId() == user.getId());
		Assert.isTrue(!getIfUserReceivedInvitationFromEvent(user,invitationForm.getEvent()));
		Assert.isTrue(invitationForm.getEvent().getStartingDate().after(new Date())
						&& invitationForm.getEvent().getPublicationDate().before(new Date()));
		
		// Siempre se crear� la invitaci�n
		invitation.setId(0);
		invitation.setText(invitationForm.getText());
		invitation.setEvent(invitationForm.getEvent());
		invitation.setUser(invitationForm.getUser());
		invitation.setSentDate(new Date(new Date().getTime() - 1000));
		invitation.setState("PENDING");
		
		validator.validate(invitation, binding);
		
		return invitation;
	}
	
}
