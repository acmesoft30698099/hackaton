package services.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import domain.Actor;
import domain.CreditCard;
import domain.Event;
import domain.utils.EventStatus;

/**
 * Clase de utilidades de los servicios
 * @author Ignacio Jos� Al�s L�pez
 */
@Service
@Transactional
public class ServicesUtils {
	
	// Services
	
	@Autowired
	private ActorService actorService;
	
	// Methods
	
	/**
	 * Compruba que el Iterable aportado contiene ids correctos.
	 * @param ids El Iterable a comprobar
	 * @throws IllegalArgumentException Si se encuentra alg�n id incorrecto
	 */
	public void checkIterable(Iterable<Integer> ids) {
		Assert.notNull(ids);
		Iterator<Integer> iter = ids.iterator();
		Assert.isTrue(iter.hasNext());
		while(iter.hasNext()) {
			Integer id = iter.next();
			checkId(id);
		}
	}
	
	/**
	 * Comprueba que el Integer aportado es un id correcto.
	 * @param id El id a comprobar.
	 * @throws IllegalArgumentException Si el id es incorrecto.
	 */
	public void checkId(Integer id) {
		Assert.notNull(id);
		Assert.isTrue(id > 0);
	}
	
	/**
	 * Comprueba que el usuario registrado tenga cuenta en el sistema
	 * @throws IllegalArgumentException Cuando el usuario no est� registrado.
	 */
	public Actor checkUser() {
		UserAccount user = LoginService.getPrincipal();
		// Como getPrincipal lanza excepci�n si no se encuentra el objeto, no es necesario tratarlo.
		Actor a = actorService.findActorByUserAccount(user);
		Assert.notNull(a);
		return a;
	}
	
	/**
	 * Retorna el actor convertido a la clase aportada. Lanzar� excepci�n si el actor no es de ese tipo.
	 * @param classObj La clase del objecto esperado
	 * @param a El actor a convertir
	 * @return El actor convertido
	 * @throws IllegalArgumentException Si el objeto no se puede convertir a la clase.
	 */
	public <T extends Actor> T checkRegistered(Class<T> classObj, Actor a) {
		Assert.isTrue(a.getClass().isAssignableFrom(classObj));
		@SuppressWarnings("unchecked")
		T result = (T) a;
		return result;
	}
	
	public <T extends Actor> T getRegisteredUser(Class<T> classObj) {
		Actor a = checkUser();
		return checkRegistered(classObj, a);
	}
	
	public String encryptPassword(String password) {
		Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
		return md5PasswordEncoder.encodePassword(password, null);
	}
	
	/**
	 * Comprueba que el UserAccount de entrada sea correcto.
	 * @param ua El UserAccount a comprobar.
	 * @param binding El binding del formulario.
	 */
	public void validarUserAccount(UserAccount ua, BindingResult binding) {
		if (ua.getUsername().isEmpty())
			binding.rejectValue("userAccount.username",
					"org.hibernate.validator.constraints.NotBlank.message");
		if (ua.getPassword().isEmpty())
			binding.rejectValue("userAccount.password",
					"org.hibernate.validator.constraints.NotBlank.message");
	}
	
	/**
	 * Retorna el estado del evento, que ser� de la siguiente forma
	 */
	public EventStatus getEventStatus(Event event) {
		Date now = new Date();
		if(event.getPublicationDate().after(now)) {
			return EventStatus.DRAFT;
		} else if(event.getSaleStartDate().after(now) && event.getPublicationDate().before(now)){
			return EventStatus.PROMOTIONAL;
		} else if(event.getStartingDate().after(now) && event.getSaleStartDate().before(now)) {
			return EventStatus.SALE;
		} else if(event.getEndingDate().after(now) && event.getStartingDate().before(now)) {
			return EventStatus.IN_PROGRESS;
		} else {
			return EventStatus.ENDED;
		}
	}
	
	/**
	 * Comprueba la validez del n�mero con el algoritmo de Luhn.
	 * @param number El n�mero de la CreditCard.
	 * @return Boolean indicando si es v�lido o no.
	 */
	public boolean validNumber(String number) {
		Boolean res = false;
		if(number != null) {
			Integer sum = 0;
			int max = number.length()-1;
			Integer iter = 0;
			for(Integer i = max; i >= 0; i--) {
				int num = Character.getNumericValue(number.charAt(i));
				if(iter%2 != 0) {
					num = num*2;
					if(num > 9) {
						num = num - 9;
					}
				}
				iter++;
				sum += num;
			}
			res = sum%10 == 0;
		}
		return res;
	}
	
	/**
	 * Comprueba la validez de la fecha de caducidad de la CreditCard que en el dominio 
	 * de la empresa es posterior a la fecha actual en 7 d�as.
	 * @param month El mes de la tarjeta
	 * @param year El a�o de la tarjeta, se admite 2 o 4 cifras.
	 * @return Boolean que indica si es v�lido o no.
	 */
	public boolean validDate(int month, int year) {
		Calendar cardDate = Calendar.getInstance();
		cardDate.clear();
		if(year <= 100) {
			year = 2000 + year;
		}
		cardDate.set(year, month-1, 1);
		Calendar actualDate = Calendar.getInstance();
		actualDate.add(Calendar.DAY_OF_YEAR,7);
		return cardDate.after(actualDate);
	}
	
	/**
	 * Comprueba la validez de la creditCard
	 * @param creditCard El credit Card
	 * @return Boolean que indica si es v�lido o no.
	 */
	public boolean validCreditCard(CreditCard creditCard){
		boolean res = false;
		if(creditCard != null){
			res = validNumber(creditCard.getNumber())
					&& validDate(creditCard.getExpirationMonth(), creditCard.getExpirationYear());
		}
		return res;
	}
	
	public String getMaskedCreditCardNumber(String creditCardNumber){
		int numberCharacters = creditCardNumber.length();
		String visibleNumber = creditCardNumber.substring(numberCharacters-4);
		String result = new String();
		for(int i = 0 ; i < (numberCharacters - 4) ; i++) {
			result = result.concat("*");
		}
		result = result.concat(visibleNumber);
		return result;
	}
	
}
