package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.TopicRepository;
import domain.Actor;
import domain.Administrator;
import domain.Comment;
import domain.Event;
import domain.Topic;
import domain.utils.AlertStatus;
import forms.CommentForm;
import forms.TopicForm;
import forms.TopicRow;

@Service
@Transactional
public class TopicService extends GenericService<TopicRepository, Topic> {
	
	// Constructor -------------------------------------

	public TopicService() {
		super();
	}

	// Services ----------------------------------------

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private AlertService alertService;

	// CRUD methods ------------------------------------

	public Topic create(Event event) {
		Topic topic = new Topic();
		topic.setEvent(event);
		return topic;
	}

	public Topic reglaNegocioSave(Topic topic) {
		// Comprobamos que el evento sea visible
		Assert.isTrue(topic.getEvent().getPublicationDate().before(new Date()));
		
		if(topic.getId() == 0) {
			topic.setClosed(false);
		} else {
			// El �nico que podr� editar el topic es un administrador
			servicesUtils.getRegisteredUser(Administrator.class);
		}
		
		return topic;
	}

	// Other methods -----------------------------------

	public Collection<TopicRow> getTopicsOfEvent(int eventId) {
		return repository.getTopicsOfEvent(eventId);
	}
	
	public void cerrarTopic(int topicId) {
		Topic topic = findOneForEdit(topicId);
		servicesUtils.getRegisteredUser(Administrator.class);
		topic.setClosed(true);
		save(topic);
		alertService.generateAlert(repository.getTopicCreator(topicId),
				AlertStatus.TOPIC_CLOSED, null);
	}

	public void abrirTopic(int topicId) {
		Topic topic = this.findOneForEdit(topicId);
		servicesUtils.getRegisteredUser(Administrator.class);
		topic.setClosed(false);
		save(topic);
		alertService.generateAlert(repository.getTopicCreator(topicId),
				AlertStatus.TOPIC_OPENED, null);
	}

	public Actor getTopicCreator(int topicId) {
		return repository.getTopicCreator(topicId);
	}

	public TopicForm construct(Topic topic) {
		Assert.isTrue(topic.getId() == 0);
		TopicForm form = new TopicForm();
		form.setId(topic.getId());
		form.setTitle(topic.getTitle());
		form.setEvent(topic.getEvent());
		return form;
	}

	// Este reconstruct se ha hecho para cuando se crea el topic
	public Topic createReconstruct(TopicForm form, BindingResult binding) {
		Assert.isTrue(form.getId() == 0);
		
		Topic topic = create(form.getEvent());
		topic.setClosed(false);
		topic.setComments(new ArrayList<Comment>());
		topic.setTitle(form.getTitle());
		
		CommentForm commentForm = form.getComments().get(0);
		Comment comment = commentService.create(null, topic);
		comment.setActor(servicesUtils.checkUser());
		comment.setContentText(commentForm.getContentText());
		comment.setLastEdition(null);
		comment.setModifiedByAdministrator(false);
		comment.setPublicationDate(new Date());
		
		topic.getComments().add(comment);
		
		validator.validate(topic, binding);
		
		return topic;
	}

}
