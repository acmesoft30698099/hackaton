package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.EventRepository;
import domain.Event;
import domain.Feedback;
import domain.GeoCoordinate;
import domain.Image;
import domain.Invitation;
import domain.Ticket;
import domain.Topic;
import domain.User;
import domain.utils.EventStatus;
import forms.EventForm;
import forms.TicketFormCreate;

@Service
@Transactional
public class EventService extends GenericService<EventRepository, Event>{
	
	// Constructor --------------------------------------

	public EventService() {
		super();
	}
	
	// Supporting services ------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private TicketService ticketService;
	
	// CRUD methods ------------------------------------------------
	
	public Event create() {
		Event event = new Event();
		event.setCelebrationCoordinate(new GeoCoordinate());
		event.setEndingDate(new Date());
		event.setFeedbacks(new ArrayList<Feedback>());
		event.setInvitations(new ArrayList<Invitation>());
		event.setPictures(new ArrayList<Image>());
		event.setPublicationDate(new Date());
		event.setSaleStartDate(new Date());
		event.setStartingDate(new Date());
		event.setTickets(new ArrayList<Ticket>());
		event.setTopics(new ArrayList<Topic>());
		
		return event;
	}
	
	public Event reglaNegocioGeneral(Event event) {
		// Comprobamos que el usuario registrado es un Usuario
		User user = servicesUtils.getRegisteredUser(User.class);
		// Comprobamos que al usuario registrado le pertenece el Evento
		Assert.isTrue(user.getId() == event.getUser().getId());
		
		return event;
	}
	
	public Event reglaNegocioSave(Event event) {
		// Comprobamos que el evento est� en la fecha de promoci�n, en el caso de que ya exista en la BD
		if(event.getId() > 0) {
			Assert.isTrue(event.getSaleStartDate().after(new Date()),"businessRule.event.promotion_state");
		}
		
		return event;
	}
	
	public Event reglaNegocioDelete(Event event) {
		// Comprobamos que el evento original est� en el estado de promoci�n o draft:
		Event original = repository.findOne(event.getId());
		Assert.isTrue(!original.getSaleStartDate().before(new Date()),"businessRule.event.promotion_state");
		
		return event;
	}
	
	public Event reglaNegocioEdit(Event event) {
		// Comprobamos que el evento original est� en el estado de promoci�n o draft:
		Event original = repository.findOne(event.getId());
		Assert.isTrue(!original.getSaleStartDate().before(new Date()),"businessRule.event.promotion_state");
		
		return event;
	}

	// Other methods ------------------------------------------------
	
	
	// Queries ------------------------------------------------------
	
	public Boolean getIfUserHasPurchaseTicketForEvent(User user, Event event) {
		return repository.getIfUserHasPurhcasedTicketFromEvent(event.getId(), user.getId());
	}
	
	public Collection<Event> getFinishedEvents() {
		return repository.getFinishedEvents(new Date());
	}
	
	public Collection<Event> getFinishedEventsWithCategory(String categoryName) {
		return repository.getFinishedEventsWithCategory(new Date(), categoryName);
	}
	
	public Collection<Event> getInProgressEvents() {
		return repository.getInProgressEvents(new Date());
	}
	
	public Collection<Event> getInProgressEventsWithCategory(String categoryName) {
		return repository.getInProgressEventsWithCategory(new Date(), categoryName);
	}
	
	public Collection<Event> getSaleEvents() {
		return repository.getSaleEvents(new Date());
	}
	
	public Collection<Event> getSaleEventsWithCategory(String categoryName) {
		return repository.getSaleEventsWithCategory(new Date(), categoryName);
	}
	
	public Collection<Event> getEventsByKeywordAndAddress(String keyword, String address) {
		if(!(keyword == null && address == null)) {
			return repository.getEventsByKeywordAndAddress(new Date(), keyword, address);
		} else {
			return new ArrayList<Event>(); // No aparecer�n eventos si no introduce ning�n dato.
		}
	}
	
	public Collection<Event> getInProgressEventsAndWithinSevenDays() {
		Date now = new Date();
		Date sevenDays = new Date(now.getTime() + 1000*60*60*24*7);
		return repository.getInProgressEventsAndWithinSevenDays(now,sevenDays);
	}
	
	public Integer getEventsPublishedWithin7Days() {
		Date now = new Date();
		Date sevenDays = new Date(now.getTime() - 1000*60*60*24*7);
		
		return repository.getEventsPublicatedInSevenDays(now, sevenDays);
	}
	
	public Integer getFinishedEventsWithMoreThanTenAssistants() {
		Date now = new Date();
		
		return repository.getFinishedEventsWithMoreThanTenAssistants(now);
	}
	
	public Map<Event,Integer> getTop5FinishedEventsWithMoreAssistants() {
		Map<Event,Integer> result = new HashMap<Event,Integer>();
		Collection<Object[]> query =  repository.getFinishedEventsWithMoreAssistants(new Date());
		for(Object[] objs : query) {
			result.put((Event) objs[0], (Integer) objs[1]);
		}
		
		return result;
	}
	
	public Integer getFinishedEventsWithFeedback5OrHigher() {
		return repository.getFinishedEventsWithFeedback5OrMore(new Date());
	}
	
	public Double getRatioUsersWithEvents() {
		return repository.getRatioUsersWithEvents();
	}
	
	public Collection<Event> getEventsWithContractsValuedHigherThan75Percent() {
		return repository.getEventsWithContractsValuedHigherThan75Percent();
	}
	
	// Construct ----------------------------------------------------
	
	public EventForm construct(Event event) {
		EventForm eventForm = new EventForm();
		eventForm.setId(event.getId());
		eventForm.setName(event.getName());
		eventForm.setPictures(new ArrayList<Image>(event.getPictures()));
		eventForm.setPublicationDate(event.getPublicationDate());
		eventForm.setSaleStartDate(event.getSaleStartDate());
		eventForm.setStartingDate(event.getStartingDate());
		eventForm.setCelebrationAddress(event.getCelebrationAddress());
		eventForm.setCelebrationCoordinate(event.getCelebrationCoordinate());
		eventForm.setDescription(event.getDescription());
		eventForm.setEndingDate(event.getEndingDate());
		eventForm.setTickets(new ArrayList<TicketFormCreate>());
		eventForm.setCategory(event.getCategory());
		
		return eventForm;
	}
	
	// Reconstruct -------------------------------------------------
	
	public Event reconstruct(EventForm eventForm, BindingResult binding) {
		Event event = create();
		Date now = new Date(new Date().getTime() - 1000*60*3); // Fecha actual menos tres minuto, ser� suficiente
		
		event.setCategory(eventForm.getCategory());
		event.setCelebrationAddress(eventForm.getCelebrationAddress());
		event.setCelebrationCoordinate(eventForm.getCelebrationCoordinate());
		event.setDescription(eventForm.getDescription());
		event.setEndingDate(eventForm.getEndingDate());
		event.setName(eventForm.getName());
		event.setPictures(eventForm.getPictures());
		event.setPublicationDate(eventForm.getPublicationDate());
		event.setSaleStartDate(eventForm.getSaleStartDate());
		event.setStartingDate(eventForm.getStartingDate());
		
		if(eventForm.getId() > 0) {
			Event original = findOneForEdit(eventForm.getId());
			
			if(servicesUtils.getEventStatus(original) == EventStatus.PROMOTIONAL) {
				event.setPublicationDate(original.getPublicationDate());
			} else {
				if(event.getPublicationDate().before(now)) {
					binding.rejectValue("publicationDate", "businessRule.date_before_now");
					eventForm.setPublicationDate(original.getPublicationDate());
				}
			}
			
			event.setId(original.getId());
			event.setVersion(original.getVersion());
			event.setFeedbacks(new ArrayList<Feedback>(original.getFeedbacks()));
			event.setInvitations(new ArrayList<Invitation>(original.getInvitations()));
			event.setTickets(new ArrayList<Ticket>(original.getTickets()));
			event.setTopics(new ArrayList<Topic>(original.getTopics()));
			event.setUser(original.getUser());
		} else {
			User user = servicesUtils.getRegisteredUser(User.class);
			
			if(event.getPublicationDate().before(now)) {
				binding.rejectValue("publicationDate", "businessRule.date_before_now");
				eventForm.setPublicationDate(now);
			}
			
			event.setUser(user);
			if(eventForm.getTickets() != null) {
				for(TicketFormCreate ticketForm : eventForm.getTickets()) {
					Ticket ticket = ticketService.reconstruct(ticketForm,binding);
					ticket.setEvent(event);
					event.getTickets().add(ticket);
				}
			}
		}
		
		if(event.getPublicationDate() != null && event.getSaleStartDate() != null
				&& event.getEndingDate() != null){
			if(event.getSaleStartDate().before(now)) {
				binding.rejectValue("saleStartDate", "businessRule.date_before_now");
			}
			if(event.getStartingDate().before(now)) {
				binding.rejectValue("startingDate", "businessRule.date_before_now");
			}
			if(event.getEndingDate().before(now)) {
				binding.rejectValue("endingDate", "businessRule.date_before_now");
			}
			// La fecha de publicaci�n no puede ser posterior estricto a la fecha de inicio de venta de entradas
			if(event.getPublicationDate().after(event.getSaleStartDate())){
				binding.rejectValue("publicationDate", "businessRule.event.publication_before_sale");
			}
			// La fecha de inicio no podr� ser anterior o igual a la fecha de inicio de compra
			if(event.getStartingDate().before(event.getSaleStartDate())) {
				binding.rejectValue("startingDate", "businessRule.event.start_before_sale");
			}
			// La fecha de inicio no podr� ser posterior o igual a la fecha de fin
			if(event.getStartingDate().after(event.getEndingDate())) {
				binding.rejectValue("startingDate", "businessRule.event.end_before_start");
			}
		}
		
		validator.validate(event, binding);
		
		return event;
	}

}
