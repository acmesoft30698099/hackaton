package services;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.FeedbackRepository;
import services.utils.ServicesUtils;
import domain.Feedback;
import domain.User;
import domain.utils.EventStatus;
import forms.FeedbackForm;

@Service
@Transactional
public class FeedbackService extends GenericService<FeedbackRepository, Feedback>{
	
	// Constructor -----------------------------------
	
	public FeedbackService() {
		super();
	}
	
	// Services --------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private InvitationService invitationService;
	
	@Autowired
	private Validator validator;
	
	// CRUD methods ----------------------------------
	
	public Feedback create() {
		Feedback feedback = new Feedback();
		
		return feedback;
	}
	
	public Feedback reglaNegocioGeneral(Feedback feedback) {
		// El usuario que gestione Feedback ser� el poseedor del Feedback
		User user = servicesUtils.getRegisteredUser(User.class);
		Assert.isTrue(feedback.getUser().getId() == user.getId());
	
		return feedback;
	}
	
	public Feedback reglaNegocioSave(Feedback feedback) {
		// El evento que se realiza Feedback ser� uno que est� en estado finalizado
		EventStatus status = servicesUtils.getEventStatus(feedback.getEvent());
		Assert.isTrue(status == EventStatus.ENDED);
		// Tambi�n el usuario que guarda el feedback no podr� ser el poseedor del evento
		User user = servicesUtils.getRegisteredUser(User.class);
		Assert.isTrue(feedback.getEvent().getUser().getId() != user.getId());
		// Comprobamos que el usuario no haya realizado un feedback al evento
		if(feedback.getId() == 0) {
			Assert.isTrue(repository.getIdOfFeedbackOfUserForEvent(user.getId(), feedback.getEvent().getId()) == null);
		}
		// Finalmente, el usuario actual tendr� una entrada comprada del evento o ser� invitado
		Assert.isTrue(purchaseService.getIfUserHasPurchasedTicketFromEvent(user, feedback.getEvent()) 
				|| invitationService.getIfUserReceivedInvitationFromEvent(user, feedback.getEvent()));
		
		return feedback;
	}
	
	// Queries --------------------------------------------
	
	public Integer getIdOfFeedbackOfUserForEvent(User user, Integer eventId) {
		return repository.getIdOfFeedbackOfUserForEvent(user.getId(), eventId);
	}
	
	// Other methods --------------------------------------
	
	public FeedbackForm construct(Feedback feedback) {
		FeedbackForm feedbackForm = new FeedbackForm();
		feedbackForm.setEvent(feedback.getEvent());
		feedbackForm.setId(feedback.getId());
		feedbackForm.setScore(feedback.getScore());
		feedbackForm.setText(feedback.getText());
		
		return feedbackForm;
	}
	
	public Feedback reconstruct(FeedbackForm feedbackForm, BindingResult binding) {
		Feedback feedback = create();
		
		if(feedbackForm.getId() > 0) {
			Feedback original = findOneForEdit(feedbackForm.getId());
			
			feedback.setId(original.getId());
			feedback.setVersion(original.getVersion());
			feedback.setEvent(original.getEvent());
			feedback.setVersion(original.getVersion());
			feedback.setPublicationDate(original.getPublicationDate());
			feedback.setUser(original.getUser());
		} else {
			User user = servicesUtils.getRegisteredUser(User.class);
			
			feedback.setUser(user);
			feedback.setEvent(feedbackForm.getEvent());
			feedback.setPublicationDate(new Date(new Date().getTime() - 1000));
		}
		
		feedback.setScore(feedbackForm.getScore());
		feedback.setText(feedbackForm.getText());
		
		validator.validate(feedback,binding);
		
		return feedback;
	}
}
