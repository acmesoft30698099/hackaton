package services;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.CommentRepository;
import domain.Actor;
import domain.Administrator;
import domain.Comment;
import domain.Topic;
import forms.CommentForm;

@Service
@Transactional
public class CommentService extends GenericService<CommentRepository, Comment> {
	
	// Constructor -------------------------------------------
	
	public CommentService() {
		super();
	}
	
	// Services ----------------------------------------------

	// CRUD methods ------------------------------------------

	public Comment create(Comment quoted, Topic topic) {
		Comment comment = new Comment();
		comment.setQuoted(quoted);
		if (quoted != null) {
			comment.setTopic(quoted.getTopic());
		} else {
			comment.setTopic(topic);
		}
		
		return comment;
	}
	
	@Override
	protected Comment reglaNegocioSave(Comment comment) {
		// Si se est� editando
		if (comment.getId() > 0) {
			reglaNegocioEdit(comment);
		}
		// Si se est� creando
		else {
			// Comprobamos que el usuario est� registrado
			Actor self = servicesUtils.checkUser();
			
			// El usuario debe ser administrador o el topic no est� cerrado
			Assert.isTrue(self instanceof Administrator || !comment.getTopic().isClosed());
		}
		
		
		return comment;
	}
	
	@Override
	protected Comment reglaNegocioEdit(Comment comment) {
		Actor self = servicesUtils.checkUser();
		// El usuario podr� ser administrador o
		Assert.isTrue(self instanceof Administrator ||
				// se trata del creador, el topic no est� cerrado y el comentario no ha sido
				// modificado por un administrador
				(self.getId() == comment.getActor().getId() && !comment.getTopic().isClosed() 
				&& !comment.isModifiedByAdministrator()));

		return comment;
	}
	// Queries ------------------------------------------------

	public Collection<Comment> getCommentsQuoting(int quotedId) {
		return repository.getCommentsQuoting(quotedId);
	}
	
	public Collection<Comment> getCommentsOfTopic(Topic topic) {
		return repository.getCommentsOfTopic(topic.getId());
	}

	// Other methods ------------------------------------------

	public CommentForm construct(Comment comment) {
		CommentForm form = new CommentForm();
		form.setId(comment.getId());
		form.setContentText(comment.getContentText());
		form.setQuoted(comment.getQuoted());
		form.setTopic(comment.getTopic());
		
		return form;
	}

	public Comment reconstruct(CommentForm form, BindingResult binding) {
		Comment comment = create(form.getQuoted(), form.getTopic());
		
		// Si se est� creando
		if (form.getId() == 0) {
			comment.setActor(servicesUtils.checkUser());
			comment.setModifiedByAdministrator(false);
			comment.setPublicationDate(new Date(new Date().getTime() - 1000));
			comment.setLastEdition(null);
		// Si se est� editando
		} else {
			Comment original = findOneForEdit(form.getId());
			
			comment.setId(original.getId());
			comment.setVersion(original.getVersion());
			comment.setActor(original.getActor());
			comment.setPublicationDate(original.getPublicationDate());
			comment.setLastEdition(new Date(new Date().getTime() - 1000));
			comment.setQuoted(original.getQuoted());
			comment.setTopic(original.getTopic());
			comment.setModifiedByAdministrator(original.isModifiedByAdministrator());
			
			if (servicesUtils.checkUser() instanceof Administrator) {
				comment.setModifiedByAdministrator(true);
			}
		}
		
		comment.setContentText(form.getContentText());
		
		validator.validate(comment, binding);
		
		return comment;
	}

}
