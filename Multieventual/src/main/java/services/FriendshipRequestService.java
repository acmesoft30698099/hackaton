package services;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import domain.Actor;
import domain.FriendshipRequest;
import forms.FriendshipRequestForm;

import repositories.FriendshipRequestRepository;

@Service
@Transactional
public class FriendshipRequestService extends GenericService<FriendshipRequestRepository, FriendshipRequest>{
	
	// Constructor -----------------------------------
	
	public FriendshipRequestService() {
		super();
	}
	
	// Supporting services ---------------------------
	
	@Autowired
	private ActorService actorService;
	
	// CRUD methods ----------------------------------
	
	public FriendshipRequest create() {
		FriendshipRequest friendshipRequest = new FriendshipRequest();
		
		return friendshipRequest;
	}
	
	public FriendshipRequest reglaNegocioSave(FriendshipRequest friendshipRequest) {
		Actor actor = servicesUtils.checkUser();
		// Si se est� creando
		if(friendshipRequest.getId() == 0) {
			// Comprobamos que el usuario actual es el que registra la petici�n
			Assert.isTrue(friendshipRequest.getSender().getId() == actor.getId());
			// Comprobamos que el usuario receptor no es el emisor
			Assert.isTrue(friendshipRequest.getReceiver().getId() != actor.getId());
			// Comprobamos que los usuarios no tengan una petici�n
			Assert.isTrue(!repository.getIfExistsFriendshipRequestBetweenUsers(actor.getId(), friendshipRequest.getReceiver().getId()));
			// Comprobamos que la configuraci�n del usuario est� puesta a ninguna:
			if(!friendshipRequest.getReceiver().isAllowFriendshipRequests()) {
				// Si es el caso, autom�ticamente guardamos como denegada
				friendshipRequest.setState("DENIED");
			}
		} else {
			// Se est� editando, el usuario que puede editarlo es el receptor
			Assert.isTrue(friendshipRequest.getReceiver().getId() == actor.getId());
		}
		
		return friendshipRequest;
	}
	
	public FriendshipRequest reglaNegocioEdit(FriendshipRequest friendshipRequest) {
		Actor actor = servicesUtils.checkUser();
		// El usuario que podr� editar la petici�n ser� el receptor
		Assert.isTrue(friendshipRequest.getReceiver().getId() == actor.getId());
		// S�lo se podr� editar para modificar el estado, por lo tanto �ste debe estar como PENDING
		Assert.isTrue(friendshipRequest.getState().equals("PENDING"));
		
		return friendshipRequest;
	}
	
	// Queries ---------------------------------------
	
	public Boolean getIfExistsFriendshipRequestBetweenUsers(Actor actor1, Actor actor2) {
		return repository.getIfExistsFriendshipRequestBetweenUsers(actor1.getId(), actor2.getId());
	}
	
	public Collection<FriendshipRequest> getPendingRequestsFromUser(Actor actor) {
		return repository.getPendingRequestsFromUser(actor.getId());
	}
	
	public Collection<FriendshipRequest> getAcceptedRequestsFromUser(Actor actor) {
		return repository.getPendingRequestsFromUser(actor.getId());
	}
	
	public Collection<Actor> getUsersThatAreNotFriendsWith(Actor actor) {
		Collection<Actor> actors = actorService.findAll();
		actors.removeAll(repository.getUsersThatReceivedARequestFromUser(actor.getId()));
		actors.removeAll(repository.getUsersThatSentARequestToUser(actor.getId()));
		
		return actors;
	}
	
	// Other methods  --------------------------------
	
	public FriendshipRequestForm construct(FriendshipRequest friendshipRequest) {
		FriendshipRequestForm friendshipRequestForm = new FriendshipRequestForm();
		friendshipRequestForm.setReceiver(friendshipRequest.getReceiver());
		friendshipRequestForm.setText(friendshipRequest.getText());
		
		return friendshipRequestForm;
	}
	
	public FriendshipRequest reconstruct(FriendshipRequestForm friendshipRequestForm, BindingResult binding) {
		FriendshipRequest friendshipRequest = create();
		
		Actor actor = servicesUtils.checkUser();
		friendshipRequest.setSender(actor);
		friendshipRequest.setSentDate(new Date(new Date().getTime() - 1000));
		friendshipRequest.setState("PENDING");
		
		friendshipRequest.setReceiver(friendshipRequestForm.getReceiver());
		friendshipRequest.setText(friendshipRequestForm.getText());
		
		validator.validate(friendshipRequest, binding);
		
		return friendshipRequest;
	}

}
