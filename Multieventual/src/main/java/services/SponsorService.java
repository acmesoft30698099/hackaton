package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.SponsorRepository;
import security.Authority;
import security.UserAccountService;
import domain.Alert;
import domain.Contract;
import domain.FriendshipRequest;
import domain.Message;
import domain.Sponsor;
import forms.ActorConfigurationForm;
import forms.SponsorForm;
import forms.SponsorRegistrationForm;

@Service
@Transactional
public class SponsorService extends GenericService<SponsorRepository, Sponsor> {
	
	// Supporting services ----------------------------------------------------

	@Autowired
	private UserAccountService userAccountService;

	// Constructor ------------------------------------------------------------

	public SponsorService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public Sponsor create() {
		Sponsor result = new Sponsor();
		result.setAlerts(new ArrayList<Alert>());
		result.setReceivedFriendshipsRequests(new ArrayList<FriendshipRequest>());
		result.setReceivedMessages(new ArrayList<Message>());
		result.setSentFriendshipsRequests(new ArrayList<FriendshipRequest>());
		result.setAllowFriendshipRequests(true);
		result.setMessagesConfiguration("EVERYBODY");
		result.setUserAccount(userAccountService.create());
		
		result.setContracts(new ArrayList<Contract>());
		
		return result;
	}
	
	public Sponsor create(Sponsor original) {
		Sponsor result = new Sponsor();		
		result.setId(original.getId());
		result.setVersion(original.getVersion());
		
		result.setName(original.getName());
		result.setSurname(original.getSurname());
		result.setEmail(original.getEmail());
		result.setPhone(original.getPhone());
		result.setPostalAddress(original.getPostalAddress());
		result.setProfilePicture(original.getProfilePicture());
		result.setAllowFriendshipRequests(original.isAllowFriendshipRequests());
		result.setMessagesConfiguration(original.getMessagesConfiguration());
		result.setCreditCard(original.getCreditCard());
		result.setCompanyName(original.getCompanyName());
		result.setCompanyWebsite(original.getCompanyWebsite());
		result.setBanner(original.getBanner());
		
		result.setUserAccount(original.getUserAccount());
		result.setReceivedMessages(original.getReceivedMessages());
		result.setReceivedFriendshipsRequests(original.getReceivedFriendshipsRequests());
		result.setSentFriendshipsRequests(original.getSentFriendshipsRequests());
		result.setAlerts(original.getAlerts());
		result.setContracts(original.getContracts());
		
		return result;
	}
	
	@Override
	protected Sponsor reglaNegocioSave(Sponsor sponsor) {
		Sponsor registered = null;
		try {
			registered = servicesUtils.getRegisteredUser(Sponsor.class);
		}
		catch (Throwable t) {
			// Se va a guardar un nuevo patrocinador
			sponsor.setUserAccount(userAccountService.save(sponsor.getUserAccount()));
		}
		if (registered != null) {
			// Si hay un patrocinador registrado, nos aseguramos de que sea el propio patrocinador
			Assert.isTrue(registered.getId() == sponsor.getId());
		}
		
		return sponsor;
	}
	
	@Override
	protected Sponsor reglaNegocioDelete(Sponsor sponsor) {
		return findOneForEdit(sponsor.getId());
	}
	
	// Queries ------------------------------------------------
	
	public Map<Sponsor,Long> getTop5SponsorsWithMoreAcceptedContracts() {
		Map<Sponsor, Long> map = new HashMap<Sponsor, Long>();
		Collection<Object[]> query = repository.getSponsorsWithMoreAcceptedContracts();
		int max = 5;
		if(query.size() < max) {
			max = query.size();
		}
		for(Object[] objs : query) {
			if(max == 0) {
				break;
			}
			map.put((Sponsor) objs[0], (Long) objs[1]);
			max--;
		}
		
		return map;
	}	

	// Other business methods -------------------------------------------------
	
	public SponsorForm construct(Sponsor sponsor) {
		SponsorForm form = new SponsorForm();
		form.setName(sponsor.getName());
		form.setSurname(sponsor.getSurname());
		form.setEmail(sponsor.getEmail());
		form.setPhone(sponsor.getPhone());
		form.setPostalAddress(sponsor.getPostalAddress());
		form.setProfilePicture(sponsor.getProfilePicture());
		form.setCompanyName(sponsor.getCompanyName());
		form.setCompanyWebsite(sponsor.getCompanyWebsite());
		form.setBanner(sponsor.getBanner());
		return form;
	}
	
	public SponsorRegistrationForm constructRegistration(Sponsor sponsor) {
		SponsorRegistrationForm form = new SponsorRegistrationForm();
		form.setName(sponsor.getName());
		form.setSurname(sponsor.getSurname());
		form.setEmail(sponsor.getEmail());
		form.setPhone(sponsor.getPhone());
		form.setPostalAddress(sponsor.getPostalAddress());
		form.setProfilePicture(sponsor.getProfilePicture());
		form.setCompanyName(sponsor.getCompanyName());
		form.setCompanyWebsite(sponsor.getCompanyWebsite());
		form.setBanner(sponsor.getBanner());
		form.setUserAccount(userAccountService.construct(sponsor.getUserAccount()));
		return form;
	}
	
	public Sponsor reconstruct(SponsorForm form, BindingResult binding) {
		Sponsor result = create(servicesUtils.getRegisteredUser(Sponsor.class));
		result.setName(form.getName());
		result.setSurname(form.getSurname());
		result.setEmail(form.getEmail());
		result.setPhone(form.getPhone());
		result.setPostalAddress(form.getPostalAddress());
		result.setProfilePicture(form.getProfilePicture());
		result.setCompanyName(form.getCompanyName());
		result.setCompanyWebsite(form.getCompanyWebsite());
		result.setBanner(form.getBanner());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Sponsor reconstruct(SponsorRegistrationForm form, BindingResult binding) {
		Sponsor result = create();
		Authority authority = new Authority();
		authority.setAuthority(Authority.SPONSOR);
		result.getUserAccount().addAuthority(authority);
		result.getUserAccount().setUsername(form.getUserAccount().getUsername());
		result.getUserAccount().setPassword(form.getUserAccount().getPassword());
		result.setName(form.getName());
		result.setSurname(form.getSurname());
		result.setEmail(form.getEmail());
		result.setPhone(form.getPhone());
		result.setPostalAddress(form.getPostalAddress());
		result.setProfilePicture(form.getProfilePicture());
		result.setCompanyName(form.getCompanyName());
		result.setCompanyWebsite(form.getCompanyWebsite());
		result.setBanner(form.getBanner());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Sponsor reconstruct(ActorConfigurationForm form, BindingResult binding) {
		Sponsor result = create(servicesUtils.getRegisteredUser(Sponsor.class));
		
		result.setAllowFriendshipRequests(Boolean.valueOf(form.getAllowFriendshipRequests()));
		result.setMessagesConfiguration(form.getMessagesConfiguration());
		
		validator.validate(result, binding);
		
		return result;
	}
	
}
