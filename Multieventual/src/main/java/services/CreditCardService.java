package services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import services.utils.ServicesUtils;

import domain.Actor;
import domain.CreditCard;
import domain.Sponsor;
import domain.User;

@Service
@Transactional
public class CreditCardService {
	
	// Constructor -------------------------------
	
	public CreditCardService() {
		super();
	}
	
	// Services ----------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// CRUD methods ------------------------------
	
	public CreditCard create() {
		CreditCard creditCard = new CreditCard();
		
		return creditCard;
	}
	
	public CreditCard save(CreditCard creditCard) {
		// Obtenemos el usuario
		Actor actor = servicesUtils.checkUser();
		// Comprobamos la validez de la tarjeta de cr�dito
		Assert.isTrue(servicesUtils.validCreditCard(creditCard), "businessRule.invalid.creditCard");
		// Si el usuario es del tipo User
		if(actor instanceof User) {
			User user = (User) actor;
			// "Guardamos" el objeto
			user.setCreditCard(creditCard);
		} else {
			Sponsor sponsor = (Sponsor) actor;
			// "Guardamos" el objeto
			sponsor.setCreditCard(creditCard);
		}
		return creditCard;
	}
	
	public void delete() {
		// Obtenemos el usuario
		Actor actor = servicesUtils.checkUser();
		if(actor instanceof User) {
			User user = (User) actor;
			// "Eliminamos" la tarjeta de cr�dito
			user.setCreditCard(null);
		} else {
			Sponsor sponsor = (Sponsor) actor;
			sponsor.setCreditCard(null);
		}
	}
	
	// Other methods ---------------------------

}
