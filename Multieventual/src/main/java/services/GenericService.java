package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.util.Assert;
import org.springframework.validation.Validator;

import services.utils.ServicesUtils;
import domain.DomainEntity;

/**
 * Clase gen�rica de servicios, si es necesario modificar uno de los servicios por reglas de negocio,
 * usar @Override. 
 * @author Student
 *
 * @param <T> Repositorio
 * @param <G> Objeto del dominio
 */
public abstract class GenericService<T extends JpaRepository<G, Integer>, G extends DomainEntity> {
	
	// Managed repository -----------------------------------------------------
	
	@Autowired
	protected T repository;
	
	// Supporting services ----------------------------------------------------
	
	@Autowired
	protected ServicesUtils servicesUtils;
	
	@Autowired
	protected Validator validator;
	
	// Constructors -----------------------------------------------------------
	
	public GenericService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public G findOne(Integer id) {
		servicesUtils.checkId(id);
		G result = repository.findOne(id);
		Assert.notNull(result);
		return result;
	}
	
	public G findOneForEdit(Integer id){
		G obj = findOne(id);
		obj = reglaNegocioGeneral(obj);
		obj = reglaNegocioEdit(obj);
		return obj;
	}
	
	public Collection<G> findAll() {
		return repository.findAll();
	}
	
	public Collection<G> findAll(Iterable<Integer> ids) {
		servicesUtils.checkIterable(ids);
		return repository.findAll(ids);
	}
	
	public G save(G obj) {
		obj = reglaNegocioGeneral(obj);
		obj = reglaNegocioSave(obj);
		return repository.save(obj);
	}
	
	public void delete(G obj) {
		reglaNegocioDelete(obj);
		repository.delete(obj.getId());
	}
	
	public void flush() {
		repository.flush();
	}
	
	// Ancillary methods ------------------------------------------------------
	
	protected G reglaNegocioGeneral(G obj){
		return obj;
	}
	
	protected G reglaNegocioSave(G obj){
		return obj;
	}
	
	protected G reglaNegocioDelete(G obj){
		return obj;
	}
	
	protected G reglaNegocioEdit(G obj){
		return obj;
	}

}