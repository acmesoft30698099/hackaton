<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="now" class="java.util.Date" />

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="invitation/edit.do" method="post" modelAttribute="invitation">
	<form:hidden path="event" />
	
	<acme:textarea code="invitation.text" path="text"/>
	
	<acme:select items="${users}" itemLabel="name" code="invitation.user" path="user"/>
		
	<acme:submit name="save" code="invitation.save"/>
</form:form>

<acme:cancel code="cancel" url="/invitation/listSent.do?eventId=${invitation.event.id}" />