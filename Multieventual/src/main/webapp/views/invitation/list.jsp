<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="now" class="java.util.Date" />

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:if test="${sent}">
	<a href="invitation/create.do?eventId=${eventId}"><spring:message code="invitation.create" /></a>
</jstl:if>

<display:table id="invitation" sort="list" pagesize="5" name="${invitations}" requestURI="${requestUri}">
	
	<jstl:if test="${received}">
		<spring:message code="invitation.event" var="eventTitle" />
		<display:column title="${eventTitle}">
			<a href="event/profile.do?eventId=${invitation.event.id}"><jstl:out value="${invitation.event.name}" /></a>
		</display:column>
	</jstl:if>
	
	<spring:message code="invitation.status" var="statusTitle" />
	<display:column title="${statusTitle}" sortable="true">
		<jstl:choose>
			<jstl:when test="${invitation.state == 'ACCEPTED'}">
				<spring:message code="invitations.status.accepted" />
			</jstl:when>
			<jstl:when test="${invitation.state == 'PENDING'}">
				<spring:message code="invitations.status.pending" />
			</jstl:when>
			<jstl:otherwise>
				<spring:message code="invitations.status.denied" />
			</jstl:otherwise>
		</jstl:choose>
	</display:column>
	
	<spring:message code="invitation.text" var="textTitle" />
	<display:column property="text" title="${textTitle}" sortable="true" />
	
	<spring:message code="invitation.sentDate" var="sentDateTitle" />
	<display:column property="sentDate" title="${sentDateTitle}" format="{0,date,dd/MM/yyyy HH:mm}" sortable="true"/>
	
	<jstl:if test="${sent}">
		<spring:message code="invitation.user" var="userTitle" />
		<display:column title="${userTitle}">
			<a href="actor/profile.do?userId=${invitation.user.id}"><jstl:out value="${invitation.user.name}" /></a>
		</display:column>
	</jstl:if>
	
	<jstl:if test="${received}">
		<display:column>
			<jstl:if test="${invitation.state == 'PENDING'}">
				<a href="invitation/accept.do?invitationId=${invitation.id}"><spring:message code="invitation.accept" /></a><br/>
				<a href="invitation/deny.do?invitationId=${invitation.id}"><spring:message code="invitation.deny" /></a>
			</jstl:if>
		</display:column>
	</jstl:if>
</display:table>

<jstl:if test="${sent}">
	<acme:cancel url="/event/profile.do?eventId=${eventId}" code="cancel"/>
</jstl:if>