<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<form:form action="comment/edit.do" method="post" modelAttribte="commentForm" commandName="commentForm">
	
	<form:hidden path="id" />
	<form:hidden path="topic" />
	<form:hidden path="quoted" />
	
	<acme:textarea path="contentText" code="comment.contenttext" />
	
	<acme:submit name="save" code="comment.save" />
	
</form:form>

<acme:cancel url="${cancelURL}" code="comment.cancel" />