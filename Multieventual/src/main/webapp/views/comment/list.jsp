<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authorize access="isAuthenticated()">
	<jstl:if test="${not isClosed}">
		<a href="comment/create-from-topic.do?topicId=${topicId}"><spring:message code="comment.create" /></a>
	</jstl:if>
</security:authorize>

<display:table list="${comments}" id="comment" pagesize="5" requestURI="${requestURI}">
	<spring:message code="comment.actor" var="actorTitle" />
	<display:column title="${actorTitle}">
		<jstl:if test="${not empty comment.actor.profilePicture}">
			<img src="<jstl:out value="${comment.actor.profilePicture}" />" class="avatar_list" /><br/>
		</jstl:if>
		<a href="actor/profile.do?id=${comment.actor.id}"><jstl:out value="${comment.actor.name.concat(' ').concat(comment.actor.surname)}" /></a>
	</display:column>
	
	<display:column title="${comment.topic.title}">
		<span class="smallText"><fmt:formatDate value="${comment.publicationDate}" pattern="dd/MM/yyyy HH:mm" /></span><hr/>
		<jstl:if test="${comment.quoted != null}">
			<fieldset>
				<legend><span class="smallText"><spring:message code="quote_of" />: <a href="actor/profile.do?id=${comment.quoted.actor.id}"><jstl:out value="${comment.quoted.actor.name.concat(' ').concat(comment.quoted.actor.surname)}" /></a> <spring:message code="at" /> <fmt:formatDate value="${comment.quoted.publicationDate}" pattern="dd/MM/yyyy HH:mm" /></span></legend>
				<jstl:out value="${comment.quoted.contentText}" />
			</fieldset>
		</jstl:if>
		<jstl:out value="${comment.contentText}" />
		<jstl:if test="${comment.lastEdition != null}">
			<hr/><span class="smallText"><spring:message code="comment.lastedition" />: <fmt:formatDate value="${comment.lastEdition}" pattern="dd/MM/yyyy HH:mm" /></span>
			<jstl:if test="${comment.modifiedByAdministrator}">
				<span class="smallText">(<spring:message code="comment.editedbyadmin" />)</span>
			</jstl:if>
		</jstl:if>
	</display:column>
	
	<security:authorize access="isAuthenticated()">
		<jstl:if test="${isAdmin or (not isClosed)}">
			<display:column>
				<jstl:if test="${isAdmin or ((not isClosed) and (comment.actor == actor) and (not comment.modifiedByAdministrator))}">
					<a href="comment/edit.do?commentId=${comment.id}"><spring:message code="comment.edit" /></a><br/><hr/>
				</jstl:if>
				<a href="comment/create-from-quoted.do?quotedId=${comment.id}"><spring:message code="comment.quote" /></a>
			</display:column>
		</jstl:if>
	</security:authorize>
	
</display:table>

<acme:cancel url="${cancelURL}" code="cancel"/>