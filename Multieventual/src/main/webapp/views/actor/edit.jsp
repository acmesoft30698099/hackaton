<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="actor/configuration/edit.do" id="configuration" modelAttribute="configuration">
	
	<fieldset>
        <legend><spring:message code="userAccount.login_details" /></legend>
		<acme:textbox code="userAccount.username" path="userAccount.username"/>
		<acme:password code="userAccount.password" path="userAccount.password"/>
		<acme:password code="userAccount.confirmPassword" path="userAccount.repeatPassword"/>
	</fieldset>
	<input type="submit" name="save_ua" value="<spring:message code="actor.save"/>">
	
	<fieldset>
        <legend><spring:message code="userAccount.configuration" /></legend>
		<acme:radio value1="true" code1="yes" value2="false" code2="no"
		code="actor.allowFriendshipRequests" path="allowFriendshipRequests" />
		<acme:radio value1="NOBODY" code1="nobody" value2="ONLY_FRIENDS" code2="only_friends"
		value3="EVERYBODY" code3="everybody" code="actor.messagesConfiguration" path="messagesConfiguration" />
	</fieldset>
	<input type="submit" name="save_config" value="<spring:message code="actor.save"/>">
	
	<input type="submit" name="delete" value="<spring:message code="actor.delete"/>" onclick="return confirm('<spring:message code="actor.delete.confirm"/>')">
</form:form>

<acme:cancel url="actor/profile.do" code="actor.cancel"/>
