<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<div class="profileAdministrador">
	<acme:edit code="actor.edit" path="administrator/edit.do" edit="${edit}"/>
	<acme:edit code="actor.edit_ua" path="actor/configuration/edit.do" edit="${edit}"/>
	
	<fieldset>
		<legend><spring:message code="actor.profile.info" /></legend>
		<div class="profile">
			<jstl:if test="${not empty administrator.profilePicture}">
				<div class="profile_avatar_pos">
					<p><img src="<jstl:out value="${administrator.profilePicture}" />" class="avatar"/></p>
				</div>
			</jstl:if>
			<div class="profile_info_pos">
				<p><spring:message code="actor.name"/>: <jstl:out value="${administrator.name}" /></p>
				<p><spring:message code="actor.surname"/>: <jstl:out value="${administrator.surname}" /></p>
				<p><spring:message code="actor.email"/>: <jstl:out value="${administrator.email}" /></p>
				<jstl:if test="${not empty administrator.phone}">
					<p><spring:message code="actor.phone"/>: <jstl:out value="${administrator.phone}" /></p>
				</jstl:if>
				<jstl:if test="${not empty administrator.postalAddress}">
					<p><spring:message code="actor.postalAddress"/>: <jstl:out value="${administrator.postalAddress}" /></p>
				</jstl:if>
			</div>
		</div>
	</fieldset>

</div>
