<%--
 * index.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<style>
	#map {
        height: 400px;
        width: 100%;
       }
</style>

<p id="marker_profile_text" hidden="hidden"><spring:message code="event.marker.profile" /></p>

<script>
	function initMap() {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			var jsonResponse = null;
			if(this.readyState == 4 && this.status == 200) {
				jsonResponse = this.responseText;
				var events = JSON.parse(jsonResponse);
				var map = new google.maps.Map(document.getElementById('map'), {
				      zoom: 3,
				      center: { lat: 37.379311,lng:-6.016708 }
				    });
				var profileText = document.getElementById("marker_profile_text").innerHTML;
				for(var i = 0;i < events.length;i++) {
					var event = events[i];
					var marker = new google.maps.Marker({
						position : { lat:event.celebrationCoordinate.latitude, lng:event.celebrationCoordinate.longitude},
						map: map,
						title : String(event.name)
					});
					google.maps.event.addListener(marker, 'click', (function(map, marker, event) {
						return function() {
							var infoWin = new google.maps.InfoWindow({
								content: '<h1>'+String(event.name)+'</h1>'+
										'<span>'+String(event.description)+'</span><br/>'+'<a href=event/profile.do?eventId='+event.id+'>'+profileText+'</a>'
							});
							infoWin.open(map,marker);
						};
					})(map, marker, event));
				}
			}
		};
		xhttp.open("GET", "rest/event/all.do", true);
		xhttp.send();
	}
</script>

<script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXuN1yiKa31U6lQqb4yPykjeGyieH0wh4&callback=initMap" ></script>

<div id="map">

</div> 
