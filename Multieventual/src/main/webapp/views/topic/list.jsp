<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authorize access="isAuthenticated()">
	<a href="topic/create.do?eventId=${eventId}"><spring:message code="topic.create" /></a>
</security:authorize>

<display:table list="${topics}" id="topic" pagesize="5" requestURI="topic/list.do" sort="list">
	
	<display:column>
		<jstl:choose>
			<jstl:when test="${topic.closed}">
				<img src="images/forum-closed.png" alt="<spring:message code="topic.closed" />" />
			</jstl:when>
			<jstl:otherwise>
				<img src="images/forum-default.png" alt="<spring:message code="topic.opened" />" />
			</jstl:otherwise>
		</jstl:choose>
	</display:column>
	
	<spring:message code="topic.title_creator" var="topicTitle" />
	<display:column title="${topicTitle}">
		<jstl:out value="${topic.title}" /><br/><span class="smallText"><spring:message code="topic.creator" /> <a href="actor/profile.do?id=${topic.idCreator}"><jstl:out value="${topic.nameCreator}" /></a></span>
	</display:column>
	
	<spring:message code="topic.lastComment" var="topicLastComment" />
	<display:column title="${topicLastComment}">
		<fmt:formatDate value="${topic.lastCommentDate}" pattern="dd/MM/yyyy HH:mm" /><br/><span class="smallText"><spring:message code="topic.by" /> <a href="actor/profile.do?id=${topic.idLastComment}"><jstl:out value="${topic.nameLastComment}" /></a></span>
	</display:column>
	
	<display:column>
		<a href="comment/list.do?topicId=${topic.id}"><spring:message code="topic.see_comments"/></a>
	</display:column>
	
	<security:authorize access="hasRole('ADMIN')" >
		<display:column>
			<jstl:choose>
				<jstl:when test="${!topic.closed}">
					<a href="topic/close.do?topicId=${topic.id}"><spring:message code="topic.close" /></a>
				</jstl:when>
				<jstl:otherwise>
					<a href="topic/open.do?topicId=${topic.id}"><spring:message code="topic.open" /></a>
				</jstl:otherwise>
			</jstl:choose>
		</display:column>
	</security:authorize>
	
</display:table>

<acme:cancel url="event/profile.do?eventId=${eventId}" code="cancel"/>