<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="topic/edit.do" method="post" modelAttribute="topicForm" commandName="topicForm">
	
	<form:hidden path="id" />
	<form:hidden path="event" />
	
	<acme:textbox path="title" code="topic.title" />
	<acme:textarea path="comments[0].contentText" code="topic.contentText" />
	
	<acme:submit name="save" code="topic.save" />
	
</form:form>

<acme:cancel url="topic/list.do?eventId=${eventId}" code="topic.cancel" />