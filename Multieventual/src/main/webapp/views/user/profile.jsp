<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<div class="profileUser">
	<jstl:if test="${user.ban == true}">
		<div class="error"><spring:message code="user.isBanned" /></div>
		<acme:edit code="user.button.unban" path="user/ban.do?id=${user.id}" edit="${showBanButton}" />
	</jstl:if>
	<jstl:if test="${user.ban == false}">
		<acme:edit code="user.button.ban" path="user/ban.do?id=${user.id}" edit="${showBanButton}" />
	</jstl:if>
	<acme:edit code="actor.edit" path="user/edit.do" edit="${edit}"/>
	<acme:edit code="actor.edit_ua" path="actor/configuration/edit.do" edit="${edit}"/>
	
	<fieldset>
		<legend><spring:message code="actor.profile.info" /></legend>
		<div class="profile">
			<jstl:if test="${not empty user.profilePicture}">
				<div class="profile_avatar_pos">
					<img src="<jstl:out value="${user.profilePicture}" />" class="avatar"/>
				</div>
			</jstl:if>
			<div class="profile_info_pos">
				<p><spring:message code="actor.name"/>: <jstl:out value="${user.name}" /></p>
				<p><spring:message code="actor.surname"/>: <jstl:out value="${user.surname}" /></p>
				<p><spring:message code="actor.email"/>: <jstl:out value="${user.email}" /></p>
				<jstl:if test="${not empty user.phone}">
					<p><spring:message code="actor.phone"/>: <jstl:out value="${user.phone}" /></p>
				</jstl:if>
				<jstl:if test="${not empty user.postalAddress}">
					<p><spring:message code="actor.postalAddress"/>: <jstl:out value="${user.postalAddress}" /></p>
				</jstl:if>
			</div>
		</div>
	</fieldset>

</div>