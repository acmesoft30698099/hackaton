<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:if test="${requestUri == 'user/list.do'}">
	<fieldset>
		<legend><spring:message code="user.finder" /></legend>
		<form action="user/list.do" method="get">
			<label for="name"><spring:message code="actor.name" />: </label>
			<input id="name" name="name" value="${name}" />
			
			<label for="surname"><spring:message code="actor.surname" />: </label>
			<input id="surname" name="surname" value="${surname}" />
			
			<label for="address"><spring:message code="actor.postalAddress" />: </label>
			<input id="address" name="address" value="${address}" />
			
			<input name="submit" type="submit" value="<spring:message code="user.find" />" />
		</form>
	</fieldset>
</jstl:if>

<display:table id="user" sort="list" pagesize="5" name="${users}" requestURI="${requestUri}">
	<spring:message code="actor.name" var="nameTitle" />
	<display:column title="${nameTitle}" property="name" sortable="true" />
	
	<spring:message code="actor.surname" var="surnameTitle" />
	<display:column title="${surnameTitle}" property="surname" sortable="true" />
	
	<spring:message code="actor.postalAddress" var="postalAddressTitle" />
	<display:column property="postalAddress" title="${postalAddressTitle}" />
	
	<spring:message code="actor.profilePicture" var="pictureTitle" />
	<display:column title="${pictureTitle}">
		<jstl:if test="${not empty user.profilePicture}">
			<img src="<jstl:out value="${user.profilePicture}" />" class="avatar_list" />
		</jstl:if>
		<jstl:if test="${empty user.profilePicture}">
			<spring:message code="actor.profilePicture.empty" />
		</jstl:if>
	</display:column>
	
	<display:column>
		<a href="actor/profile.do?id=${user.id}"><spring:message code="user.profile" /></a>
	</display:column>
</display:table>