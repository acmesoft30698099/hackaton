<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:if test="${canEdit}">
	<a href="ticket/create.do?eventId=${event.id}"><spring:message code="ticket.create" /></a>
</jstl:if>

<display:table id="ticket" pagesize="5" sort="list" name="tickets" requestURI="${requestUri}">
	<spring:message code="ticket.name" var="nameTitle" />
	<display:column property="name" title="${nameTitle}" sortable="true" />
	
	<spring:message code="ticket.price" var="priceTitle" />
	<display:column title="${priceTitle}" sortable="true">
		<jstl:out value="${ticket.price}" />$
	</display:column>
	
	<spring:message code="ticket.remainingUnits" var="remainingUnitsTitle" />
	<display:column title="${remainingUnitsTitle}" sortable="true">
		<jstl:choose>
			<jstl:when test="${ticket.remainingUnits == 0}">
				<spring:message code="ticket.units.zero" />
			</jstl:when>
			<jstl:when test="${ticket.remainingUnits == null}">
				<spring:message code="ticket.no.remainingUnits" />
			</jstl:when>
			<jstl:otherwise>
				<jstl:out value="${ticket.remainingUnits}" />
			</jstl:otherwise>
		</jstl:choose>
	</display:column>
	
	<display:column>
		<a href="ticket/profile.do?ticketId=${ticket.id}"><spring:message code="ticket.profile" /></a>
	</display:column>
	
	<security:authorize access="hasRole('USER')">
		<display:column>
			<jstl:choose>
				<jstl:when test="${ticket.remainingUnits > 0 and canBuy}">
					<acme:gotoConfirm url="purchase/purchase.do?ticketId=${ticket.id}" code="ticket.buy" text="ticket.buy.confirm" access="${true}" />
				</jstl:when>
				<jstl:when test="${ticket.remainingUnits > 0 and (ticket.price == 0 and canFreeBuy)}">
					<acme:goto url="purchase/purchase.do?ticketId=${ticket.id}" code="ticket.buy" access="${true}" />
				</jstl:when>
			</jstl:choose>	
		</display:column>
	</security:authorize>

</display:table>

<acme:cancel code="cancel" url="/event/profile.do?eventId=${ticket.event.id}" />