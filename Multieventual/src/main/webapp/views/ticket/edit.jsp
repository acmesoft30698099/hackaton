<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="ticket/edit.do" method="post" modelAttribute="ticket">
	<form:hidden path="id" />
	<form:hidden path="event" />
	
	<acme:textbox code="ticket.name" path="name"/>
	<acme:textarea code="ticket.description" path="description"/>
	<acme:textbox code="ticket.price" path="price"/>
	<acme:textbox code="ticket.remainingUnits" path="remainingUnits" />
	
	<acme:submit name="save" code="ticket.save"/>
	<jstl:if test="${ticket.id > 0 && !lastTicket}">
		<acme:submit name="delete" code="ticket.delete"/>
	</jstl:if>
</form:form>

<acme:cancel code="cancel" url="/ticket/list.do?eventId=${ticket.event.id}" />