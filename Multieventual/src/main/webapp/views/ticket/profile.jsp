<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:if test="${canEdit}">
	<a href="ticket/edit.do?ticketId=${ticket.id}"><spring:message code="ticket.edit" /></a>
</jstl:if>

<h4><jstl:out value="${ticket.name}" /></h4>

<jstl:if test="${ticket.description != ''}">
	<p><spring:message code="ticket.description" />: </p>
	<span>
		<jstl:out value="${ticket.description}" />
	</span>
</jstl:if>

<p><spring:message code="ticket.price" />: 
	<jstl:choose>
		<jstl:when test="${ticket.price == 0}">
			<spring:message code="ticket.price.free" />
		</jstl:when>
		<jstl:otherwise>
			<jstl:out value="${ticket.price}" />
		</jstl:otherwise>
	</jstl:choose>
</p>

<p><spring:message code="ticket.remainingUnits" />:
	<jstl:choose>
		<jstl:when test="${ticket.remainingUnits == null}">
			<spring:message code="ticket.no.remainingUnits" />
		</jstl:when>
		<jstl:when test="${ticket.remainingUnits == 0}">
			<spring:message code="ticket.units.zero" />
		</jstl:when>
		<jstl:otherwise>
			<jstl:out value="${ticket.remainingUnits}" />
		</jstl:otherwise>
	</jstl:choose>
</p>

<acme:cancel url="/ticket/list.do?eventId=${ticket.event.id}" code="cancel"/>
<acme:edit code="ticket.buy" path="purchase/purchase.do?ticketId=${ticket.id}" edit="${ticket.remainingUnits > 0 and (canBuy or (ticket.price == 0 && canFreeBuy))}"/>