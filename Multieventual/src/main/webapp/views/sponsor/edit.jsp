<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="sponsor/${operation}.do" id="sponsor" modelAttribute="sponsor">
	<jstl:if test="${id <= 0}">
		<fieldset>
	        <legend><spring:message code="userAccount.login_details" /></legend>
	        <acme:textbox code="userAccount.username" path="userAccount.username"/>
			<acme:password code="userAccount.password" path="userAccount.password"/>
			<acme:password code="userAccount.confirmPassword" path="userAccount.repeatPassword"/>
	    </fieldset>
	</jstl:if>
	
	<fieldset>
		<legend><spring:message code="actor.account_details" /></legend>
		<acme:textbox code="actor.name" path="name"/>
		<acme:textbox code="actor.surname" path="surname"/>
		<acme:textbox code="actor.email" path="email"/>
		<acme:textbox code="actor.phone" path="phone"/>
		<acme:textbox code="actor.postalAddress" path="postalAddress"/>
		<acme:textbox code="actor.profilePicture" path="profilePicture"/>
	</fieldset>
	
	<fieldset>
		<legend><spring:message code="sponsor.company_details" /></legend>
		<acme:textbox code="sponsor.company.name" path="companyName"/>
		<acme:textbox code="sponsor.company.website" path="companyWebsite"/>
		<acme:textbox code="sponsor.company.banner" path="banner"/>
	</fieldset>
	
	<jstl:if test="${id <= 0}">
		<acme:termAndconditions /><br/>
	</jstl:if>
	<jstl:if test="${id <= 0}">
		<acme:save code="actor.create"/>
	</jstl:if>
	<jstl:if test="${id > 0}">
		<acme:save code="actor.save"/>
	</jstl:if>
</form:form>
<jstl:if test="${id > 0}">
	<acme:cancel url="actor/profile.do" code="actor.cancel"/>
</jstl:if>