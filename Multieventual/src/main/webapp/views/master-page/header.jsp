<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>

<div>
	<img src="images/logo.png" alt="Acme., Inc." class="avatar" />
</div>

<div>
	<ul id="jMenu">
		<li><a class="fNiv"><spring:message code="master.page.contact" /></a>
			<ul>
				<li class="arrow" />
				<li><a href="contact/show.do"><spring:message code="master.page.contact.show" /></a></li>
				<li><a href="mailto:ignalelop@alum.us.es"><spring:message code="master.page.contact.send" /></a></li>
			</ul>
		</li>
		
		<li><a class="fNiv"><spring:message code="master.page.general" /></a>
			<ul>
				<li class="arrow" />
				<li><a href="user/list.do"><spring:message code="master.page.user.list" /></a></li>
				<li><a href="event/listOnSale.do"><spring:message code="master.page.events.sale" /></a></li>
				<li><a href="event/listInProgress.do"><spring:message code="master.page.events.progress" /></a></li>
				<li><a href="event/listFinished.do"><spring:message code="master.page.events.finished" /></a></li>
				<li><a href="event/finder.do"><spring:message code="master.page.events.finder" /></a></li>
			</ul>
		</li>
		
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv"><spring:message	code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="category/list.do"><spring:message code="master.page.category.list" /></a></li>
					<li><a href="dashboard/show.do"><spring:message code="master.page.show.dashboard" /></a></li>				
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('USER')">
			<li><a class="fNiv"><spring:message	code="master.page.user" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="creditCard/profile.do"><spring:message code="master.page.credit.card" /></a></li>
					<li><a href="event/listOwns.do"><spring:message code="master.page.events.owns" /></a></li>
					<li><a href="purchase/list.do"><spring:message code="master.page.purchase.list" /></a>
					<li><a href="invitation/listReceived.do"><spring:message code="master.page.invitation.list" /></a>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('SPONSOR')">
			<li><a class="fNiv"><spring:message code="master.page.sponsor" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="creditCard/profile.do"><spring:message code="master.page.credit.card" /></a></li>
					<li><a href="contract/listSent.do"><spring:message code="master.page.contracts.made" /></a></li>
				</ul>
		</security:authorize>
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="user/create.do"><spring:message code="master.page.user.register" /></a></li>
					<li><a href="sponsor/create.do"><spring:message code="master.page.sponsor.register" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
			<li>
				<a class="fNiv">
			        <spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
			        <jstl:choose>
			        	<jstl:when test="${isBanned == null}" >
			        		<% 
						        Cookie[] cookies = request.getCookies();
								for(Cookie cookie : cookies) {
									if(cookie.getName().equals("isBanned") && cookie.getValue().equals("true")) { %>
										<span class="error">[BAN]</span>
								 <% }
								}
					       	%>
						</jstl:when>
						<jstl:otherwise>
							<jstl:if test="${isBanned}">
								<span class="error">[BAN]</span>
							</jstl:if>
						</jstl:otherwise>
					</jstl:choose>
				</a>
				<ul>
					<li class="arrow"></li>
					<li><a href="actor/profile.do"><spring:message code="master.page.profile.info" /></a></li>
					<security:authorize access="hasRole('ADMIN')">
						<li><a href="administrator/edit.do"><spring:message code="master.page.profile.edit" /></a></li>
					</security:authorize>
					<security:authorize access="hasRole('SPONSOR')">
						<li><a href="sponsor/edit.do"><spring:message code="master.page.profile.edit" /></a></li>
					</security:authorize>
					<security:authorize access="hasRole('USER')">
						<li><a href="user/edit.do"><spring:message code="master.page.profile.edit" /></a></li>
					</security:authorize>
					<li><a href="actor/configuration/edit.do"><spring:message code="master.page.configuration.edit" /></a></li>					
					<li><a href="alert/listread.do"><spring:message code="master.page.alerts.read" /></a></li>	
					<li><a href="alert/listunread.do"><spring:message code="master.page.alerts.unread" /></a></li>
					<li><a href="message/list.do"><spring:message code="master.page.message.list" /></a></li>
					<li><a href="friendshipRequest/list.do"><spring:message code="master.page.friendships.pending" /></a></li>
					<li><a href="friendshipRequest/friends.do"><spring:message code="master.page.friendships.friends" /></a></li>
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>
