<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table sort="list" requestURI="${requestUri}" name="alerts" id="alert" pagesize="5">

	<spring:message code="alert.generationDate" var="generationDateTitle" />
	<display:column property="generationDate" title="${generationDateTitle}"
		format="{0,date,dd/MM/yyyy HH:mm}" sortable="true" />

	<spring:message code="alert.textInfo" var="textTitle" />
	<display:column title="${textTitle}">
		<jstl:choose>
			<jstl:when test="${alert.textInfo == 'MESSAGE_RECEIVED'}">
				<spring:message code="alert.message.received" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'FRIENDSHIP_RECEIVED'}">
				<spring:message code="alert.friendship.received" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'FRIENDSHIP_ACCEPTED'}">
				<spring:message code="alert.friendship.accepted" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'FRIENDSHIP_DENIED'}">
				<spring:message code="alert.friendship.denied" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'NEW_FRIEND_EVENT'}">
				<spring:message code="alert.new.friend.event" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'PURCHASE_FRIEND_EVENT'}">
				<spring:message code="alert.purchase.friend.event" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'FEEDBACK_FRIEND'}">
				<spring:message code="alert.feedback.friend" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'INVITATION_RECEIVED'}">
				<spring:message code="alert.invitation.received" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'INVITATION_ACCEPTED'}">
				<spring:message code="alert.invitation.accepted" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'INVITATION_DENIED'}">
				<spring:message code="alert.invitation.denied" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'CONTRACT_RECEIVED'}">
				<spring:message code="alert.contract.received" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'CONTRACT_ACCEPTED'}">
				<spring:message code="alert.contract.accepted" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'CONTRACT_DENIED'}">
				<spring:message code="alert.contract.denied" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'TOPIC_CLOSED'}">
				<spring:message code="alert.topic.closed" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'TOPIC_OPENED'}">
				<spring:message code="alert.topic.opened" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'COMMENT_EDITED'}">
				<spring:message code="alert.comment.edited" />
			</jstl:when>
			<jstl:when test="${alert.textInfo == 'BANNED'}">
				<spring:message code="alert.banned" />
			</jstl:when>
			<jstl:otherwise>
			</jstl:otherwise>
		</jstl:choose>
	</display:column>
	
	<spring:message code="alert.link" var="linkTitle" />
	<display:column title="${linkTitle}">
		<jstl:if test="${not empty alert.link}">
			<a href="<jstl:out value="${alert.link}" />"><jstl:out value="${alert.link}" /></a>
		</jstl:if>
	</display:column>

	<display:column>
		<jstl:if test="${read}">
			<acme:goto url="/alert/remove.do?alertId=${alert.id}" code="alert.remove" />
		</jstl:if>
		<jstl:if test="${!read}">
			<acme:goto url="/alert/read.do?alertId=${alert.id}" code="alert.read" />
		</jstl:if>
	</display:column>

</display:table>
