<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form modelAttribute="messageForm" method="post" action="message/edit.do">
	<acme:textbox code="message.title" path="title" />
	<acme:textarea code="message.text" path="text"/>

	<p>
		<spring:message code="message.receiver" />
		<form:select path="receiver">
			<form:options itemLabel="name" items="${actors}" itemValue="id"/>
		</form:select>
	</p>
			
	<br/><acme:submit name="save" code="message.save" />
	
</form:form>

<acme:cancel code="message.cancel" url="message/list.do" />