<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<p>
	<acme:goto code="message.create" url="message/create.do" />
</p>

<display:table sort="list" requestURI="message/list.do" name="messages" id="_message" pagesize="5">

	<spring:message code="message.sentDate" var="sentDateTitle" />
	<display:column property="sentDate" title="${sentDateTitle}"
		format="{0,date,dd/MM/yyyy HH:mm}" sortable="true"/>
	
	<spring:message code="message.title" var="subjectTitle" />
	<display:column property="title" title="${subjectTitle}" sortable="true" />

	<spring:message code="message.text" var="textTitle" />
	<display:column property="text" title="${textTitle}" sortable="true" />
	
	<spring:message code="message.repliedText" var="repliedTextTitle" />
	<display:column property="repliedText" title="${repliedTextTitle}" sortable="true" />
	
	<spring:message code="message.sender" var="senderTitle" />
	<display:column title="${senderTitle}">
		<a href="actor/profile.do?actorId=${_message.sender.id}"><jstl:out
				value="${_message.sender.name} ${_message.sender.surname}" /></a>
	</display:column>

	<display:column>
		<jstl:if test="${_message.repliedText == null}">	
			<acme:goto url="message/reply.do?messageId=${_message.id}" code="message.reply" />
		</jstl:if>
	</display:column>
	
	<display:column>
		<acme:goto url="message/profile.do?messageId=${_message.id}" code="message.profile" />
	</display:column>

</display:table>
