<%--
 * profile.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<h3><jstl:out value="${_message.title}" /></h3>

<p><spring:message code="message.sender" />: 
	<a href="actor/profile.do?actorId=${_message.sender.id}" >
		<jstl:out value="${_message.sender.name} ${_message.sender.surname}" />
	</a>
</p>

<p><spring:message code="message.receiver" />: 
	<a href="actor/profile.do?actorId=${_message.receiver.id}" >
		<jstl:out value="${_message.receiver.name} ${_message.receiver.surname}" />
	</a>
</p>

<fieldset>
	<legend><spring:message code="message.text" /></legend>
	<p><jstl:out value="${_message.text}" /></p>
</fieldset>

<jstl:if test="${_message.repliedText!=null}">
	<fieldset>
		<legend><spring:message code="message.repliedText" /></legend>
		<p><jstl:out value="${_message.repliedText}" /></p>
	</fieldset>
</jstl:if>

<fieldset>
	<legend><spring:message code="message.options" /></legend>
	<jstl:if test="${_message.repliedText==null}">
		<acme:goto url="message/reply.do?messageId=${_message.id}" code="message.reply"/>
	</jstl:if>
	<acme:gotoConfirm url="message/delete.do?messageId=${_message.id}" code="message.delete" text="message.delete.confirm"/>
</fieldset>