<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="now" class="java.util.Date" />

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="event/edit.do" method="post" modelAttribute="event">
	<form:hidden path="id" />
	
	<acme:textbox code="event.name" path="name"/>
	<acme:textarea code="event.description" path="description"/>
	<acme:textbox code="event.celebrationAddress" path="celebrationAddress"/>
	
	<fieldset>
		<legend><spring:message code="event.coordinate" /></legend>
		<acme:textbox code="event.coordinate.latitude" path="celebrationCoordinate.latitude"/>
		<acme:textbox code="event.coordinate.longitude" path="celebrationCoordinate.longitude"/>
	</fieldset>
	
	<jstl:choose>
		<jstl:when test="${!(event.publicationDate < now && event.id != 0)}">
			<acme:textbox code="event.publicationDate" path="publicationDate"/>
		</jstl:when>
		<jstl:otherwise>
			<form:hidden path="publicationDate" />
		</jstl:otherwise>
	</jstl:choose>
	
	<acme:textbox code="event.saleStartDate" path="saleStartDate"/>
	<acme:textbox code="event.startingDate" path="startingDate"/>
	<acme:textbox code="event.endingDate" path="endingDate"/>
	
	<acme:select items="${categories}" itemLabel="name" code="event.category" path="category"/>
	
	<fieldset>
		<legend><spring:message code="event.pictures" /></legend>
		<jstl:forEach items="${event.pictures}" var="picture" varStatus="status">
			<acme:textbox code="event.pictures.url" path="pictures[${status.index}].url"/>
			<button type="submit" name="delete_picture" formaction="event/pictures/removePicture.do?index=${status.index}">
				<spring:message code="event.delete.picture" />
			</button>
			<hr/>
		</jstl:forEach>
		<button type="submit" name="add_picture" formaction="event/pictures/addPicture.do">
			<spring:message code="event.add.picture" />
		</button>
	</fieldset>

	<jstl:if test="${event.id == 0}">	
		<fieldset>
			<legend><spring:message code="event.tickets" /></legend>
			<jstl:forEach begin="0" end="${fn:length(event.tickets)-1}" var="index">
				<acme:textbox code="ticket.name" path="tickets[${index}].name" />
				<acme:textarea code="ticket.description" path="tickets[${index}].description" />
				<acme:textbox code="ticket.price" path="tickets[${index}].price" />
				<acme:textbox code="ticket.remainingUnits" path="tickets[${index}].remainingUnits" />
				
				<jstl:if test="${fn:length(event.tickets) != 1}">
					<br/>
					<button type="submit" name="delete_ticket" formaction="event/tickets/removeTicket.do?index=${index}">
						<spring:message code="event.delete.ticket" />
					</button>
					<hr/>
				</jstl:if>
			</jstl:forEach>
			<button type="submit" name="add_ticket" formaction="event/tickets/addTicket.do">
				<spring:message code="event.add.ticket" />
			</button>
		</fieldset>
	</jstl:if>
	
	<acme:submit name="save" code="event.save"/>
	
	<jstl:if test="${event.id > 0}">
		<acme:submit name="delete" code="event.delete"/>
	</jstl:if>
</form:form>

<jstl:choose>
	<jstl:when test="${event.id == 0}">
		<acme:cancel code="cancel" url="/event/listOwns.do" />
	</jstl:when>
	<jstl:otherwise>
		<acme:cancel code="cancel" url="/event/profile.do?eventId=${event.id}" />
	</jstl:otherwise>
</jstl:choose>