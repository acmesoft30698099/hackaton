<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="now" class="java.util.Date" />

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<security:authorize access="hasRole('USER')">
	<a href="event/create.do"><spring:message code="event.create" /></a>
</security:authorize>

<jstl:if test="${canFilterCategory}">
	<fieldset>
		<legend><spring:message code="event.category.filter" /></legend>
		<form action="${requestUri}">
			<label for="category">
				<spring:message code="event.category" />:
			</label>
			<input name="category" id="category" value="${category}" />
			
			<input value="<spring:message code="event.search"/>" type="submit" />
		</form>
	</fieldset>
</jstl:if>

<jstl:if test="${canSearch}">
	<fieldset>
		<legend><spring:message code="event.search" /></legend>
		<form action="${requestUri}">
			<label for="keyword">
				<spring:message code="event.keyword" />:
			</label>
			<input name="keyword" id="keyword" value="${keyword}" />
			<label for="address">
				<spring:message code="event.address" />:
			</label>
			<input name="address" id="address" value="${address}" />
			
			<input value="<spring:message code="event.search"/>" type="submit" />
		</form>
	</fieldset>
</jstl:if>

<display:table id="event" sort="list" pagesize="5" name="${events}" requestURI="${requestUri}">
	<spring:message code="event.name" var="nameTitle" />
	<display:column property="name" title="${nameTitle}" sortable="true" />
	
	<spring:message code="event.category" var="categoryTitle" />
	<display:column property="category.name" title="${categoryTitle}" sortable="true" />
	
	<spring:message code="event.status" var="statusTitle" />
	<display:column title="${statusTitle}">
		<jstl:choose>
			<jstl:when test="${event.publicationDate > now}">
				<spring:message code="event.status.not_visible" />
			</jstl:when>
			<jstl:when test="${event.saleStartDate > now and event.publicationDate < now }">
				<spring:message code="event.status.published" />
			</jstl:when>
			<jstl:when test="${event.startingDate > now and event.saleStartDate < now }">
				<spring:message code="event.status.on_sale" />
			</jstl:when>
			<jstl:when test="${event.endingDate > now and event.startingDate < now }">
				<spring:message code="event.status.in_progress" />
			</jstl:when>
			<jstl:otherwise>
				<spring:message code="event.status.ended" />
			</jstl:otherwise>
		</jstl:choose>
	</display:column>
	
	<spring:message code="event.address" var="addressTitle" />
	<display:column property="celebrationAddress" title="${addressTitle}" sortable="true"/>
	
	<spring:message code="event.pictures" var="picturesTitle" />
	<display:column title="${picturesTitle}">
		<jstl:forEach items="${event.pictures}" var="picture">
			<img src="<jstl:out value="${picture.url}"/>" class="avatar_list" />
		</jstl:forEach>
	</display:column>
	
	<display:column>
		<a href="event/profile.do?eventId=${event.id}"><spring:message code="event.profile" /></a>
	</display:column>

</display:table>