<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<jsp:useBean class="java.util.Date" id="now" />

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
	#map {
        height: 325px;
        width: 45%;
        padding: 5px;
       }
</style>

<h3><jstl:out value="${event.name}" /></h3>

<jstl:if test="${canEdit}">
	<a href="event/edit.do?eventId=${event.id}"><spring:message code="event.edit" /></a>
</jstl:if>

<p id="marker_profile_text" hidden="hidden"><spring:message code="event.marker.profile" /></p>
<p id="marker_name" hidden="hidden"><jstl:out value="${event.name}" /></p>
<p id="marker_description" hidden="hidden"><jstl:out value="${event.description}" /></p>

<script>
function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
	     zoom: 6,
	     center: { lat: ${event.celebrationCoordinate.latitude},lng:${event.celebrationCoordinate.longitude} }
	});
	var profileText = document.getElementById("marker_profile_text").innerHTML;
	var name = document.getElementById("marker_name").innerHTML;
	var description = document.getElementById("marker_description").innerHTML;
	var infoWin = new google.maps.InfoWindow({
		content: '<h1>'+String(name)+'</h1>'+
			'<span>'+String(description)+'</span><br/>'+'<a href=event/profile.do?eventId='+${event.id}+'>'+profileText+'</a>'
	});
	var marker = new google.maps.Marker({
		position : { lat: ${event.celebrationCoordinate.latitude}, lng: ${event.celebrationCoordinate.longitude}},
		map: map,
		title : String(name)
	});
	marker.addListener('click', function() {
		infoWin.open(map,marker);
	});
}
</script>

<script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXuN1yiKa31U6lQqb4yPykjeGyieH0wh4&callback=initMap" ></script>

<div id="map">

</div>

<fieldset>
	<legend><spring:message code="event.information" /></legend>
	<p><spring:message code="event.description" />: <jstl:out value="${event.description}" /></p>
	<p><spring:message code="event.celebrationAddress" />: <jstl:out value="${event.celebrationAddress}" /></p>
	<p><spring:message code="event.publicationDate" />: <fmt:formatDate value="${event.publicationDate}" pattern="dd/MM/yyyy HH:mm" /></p>
	<p><spring:message code="event.saleStartDate" />: <fmt:formatDate value="${event.saleStartDate}" pattern="dd/MM/yyyy HH:mm" /></p>
	<p><spring:message code="event.startingDate" />: <fmt:formatDate value="${event.startingDate}" pattern="dd/MM/yyyy HH:mm" /></p>
	<p><spring:message code="event.endingDate" />: <fmt:formatDate value="${event.endingDate}" pattern="dd/MM/yyyy HH:mm" /></p>
	
	<jstl:if test="${canInvite}">
		<a href="invitation/listSent.do?eventId=${event.id}"><spring:message code="event.invitation.list" /></a><br/>
	</jstl:if>
	
	<a href="ticket/list.do?eventId=${event.id}"><spring:message code="event.list.tickets" /></a><br/>
	
	<a href="topic/list.do?eventId=${event.id}"><spring:message code="event.topics" /></a><br/>
	
	<jstl:if test="${event.endingDate < now}">
		<a href="feedback/list.do?eventId=${event.id}"><spring:message code="event.list.feedbacks" /></a><br/>
	</jstl:if>
	
	<fieldset>
		<legend><spring:message code="event.gallery" /></legend>
		<jstl:forEach items="${event.pictures}" var="picture">
			<img src="<jstl:out value="${picture.url}" />" alt="img" class="avatar" />
		</jstl:forEach>
	</fieldset>
</fieldset>
<fieldset>
	<legend><spring:message code="event.sponsors" /></legend>
	<security:authorize access="hasRole('SPONSOR')">
		<jstl:if test="${canCreateContract}">
			<a href="contract/create.do?eventId=${event.id}"><spring:message code="event.create.contract" /></a><br/>
		</jstl:if>
	</security:authorize>
	
	<jstl:if test="${isOwner}">
		<a href="contract/listEvent.do?eventId=${event.id}"><spring:message code="event.contracts" /></a><br/>
	</jstl:if>
	
	<jstl:forEach items="${sponsors}" var="sponsor">
		<img src="<jstl:out value="${sponsor}" />" class="bannerImg" />
	</jstl:forEach>
</fieldset>
