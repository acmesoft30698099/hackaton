<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="query.categoryAndNumberEvents" />: </p>
<fieldset>
	<ul>
		<jstl:forEach items="${categoryAndNumberEvents}" var="value" >
			<li><jstl:out value="${value.key.name}" />: <jstl:out value="${value.value}" /></li>
		</jstl:forEach>
	</ul>
</fieldset>

<p><spring:message code="query.numberEventsPublishedRecently" />: <jstl:out value="${numberEventsPublishedRecently}" /></p>

<p><spring:message code="query.numberFinishedEventsWith10Assistants" />: <jstl:out value="${numberFinishedEventsWith10Assistants}" /></p>

<p><spring:message code="query.top5FinishedEventsByNumberAssistants" />: </p>
<fieldset>
	<ol>
		<jstl:forEach items="${top5FinishedEventsByNumberAssistants}" var="value">
			<li><a href="event/profile.do?eventId=${value.key.id}"><jstl:out value="${value.key.name}" /></a>: <jstl:out value="${value.value}" /></li>
		</jstl:forEach>
	</ol>
</fieldset>

<p><spring:message code="query.numberFinishedEventsWithFeedback5OrHigher" />: <jstl:out value="${numberFinishedEventsWithFeedback5OrHigher}" /></p>

<p><spring:message code="query.ratioUsersWithOneOrMoreEvents" />: <jstl:out value="${ratioUsersWithOneOrMoreEvents}" /></p>

<p><spring:message code="query.top5UsersByEventsPublishedAndTheNumber" />: </p>
<fieldset>
	<ol>
		<jstl:forEach items="${top5UsersByEventsPublishedAndTheNumber}" var="value" >
			<li><a href="actor/profile.do?actorId=${value.key.id}"><jstl:out value="${value.key.name}" /></a>: <jstl:out value="${value.value}" /></li>
		</jstl:forEach>
	</ol>
</fieldset>

<p><spring:message code="query.top5SponsorsByNumberOfAcceptedContracts" />: </p>
<fieldset>
	<ol>
		<jstl:forEach items="${top5SponsorsByNumberOfAcceptedContracts}" var="value" >
			<li><a href="actor/profile.do?actorId=${value.key.id}"><jstl:out value="${value.key.name}" /></a>: <jstl:out value="${value.value}" /></li>
		</jstl:forEach>
	</ol>
</fieldset>

<p><spring:message code="query.numberAndRatioOfBannedUsers" />: <jstl:out value="${numberAndRatioOfBannedUsers.objA}" />, <jstl:out value="${numberAndRatioOfBannedUsers.objB}" /></p>

<p><spring:message code="query.averageRatioOfMoneyGainedForEventsInCategories" />: </p>
<fieldset>
	<ul>
		<jstl:forEach items="${averageRatioOfMoneyGainedForEventsInCategories}" var="value" >
			<li><a href="category/profile.do?categoryId=${value.key.id}" ><jstl:out value="${value.key.name}" /></a>: <jstl:out value="${value.value}" /></li>
		</jstl:forEach>
	</ul>
</fieldset> 

<p><spring:message code="query.usersThatSpentMoreThan60PercentInTickets" />: </p>
<fieldset>
	<ul>
		<jstl:forEach items="${usersThatSpentMoreThan60PercentInTickets}" var="value">
			<li><a href="actor/profile.do?actorId=${value.id}" ><jstl:out value="${value.name}" /></a></li>
		</jstl:forEach>
	</ul>
</fieldset>

<p><spring:message code="query.averageEventsOnSaleOrInProgressInCategories" />: </p>
<fieldset>
	<ul>
		<jstl:forEach items="${averageEventsOnSaleOrInProgressInCategories}" var="value">
			<li><a href="category/profile.do?categoryId=${value.key.id}" ><jstl:out value="${value.key.name}" /></a>: <jstl:out value="${value.value}" /></li>
		</jstl:forEach>
	</ul>
</fieldset>

<p><spring:message code="query.eventsWhoseSumContractsAreHigherThan75Percent" />: </p>
<fieldset>
	<ul>
		<jstl:forEach items="${eventsWhoseSumContractsAreHigherThan75Percent}" var="value" >
			<li><a href="event/profile.do?eventId=${value.id}"><jstl:out value="${value.name}" /></a></li>
		</jstl:forEach>
	</ul>
</fieldset>