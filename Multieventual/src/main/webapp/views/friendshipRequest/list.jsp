<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<a href="friendshipRequest/create.do"><spring:message code="friendship.create" /></a>

<display:table pagesize="5" sort="list" list="${friendships}" requestURI="friendshipRequest/list.do" id="friendship" >

	<spring:message code="friendship.sentDate" var="sentDateTitle" />
	<display:column property="sentDate" title="${sentDateTitle}" format="{0,date,dd/MM/yyyy HH:mm}" sortable="true" />
	
	<spring:message code="friendship.text" var="textTitle" />
	<display:column property="text" title="${textTitle}" sortable="true" />
	
	<spring:message code="friendship.receiver" var="receiverTitle" />
	<display:column title="${receiverTitle}">
		<a href="actor/profile.do?actorId=${friendship.receiver.id}"><jstl:out value="${friendship.receiver.name}" /></a>
	</display:column>
	
	<spring:message code="friendship.sender" var="senderTitle" />
	<display:column title="${senderTitle}" >
		<a href="actor/profile.do?actorId=${friendship.sender.id}"><jstl:out value="${friendship.sender.name}" /></a>
	</display:column>
	
	<spring:message code="friendship.state" var="stateTitle"  />
	<display:column title="${stateTitle}" sortable="true" >
		<jstl:choose>
			<jstl:when test="${friendship.state == 'ACCEPTED'}">
				<spring:message code="friendship.accepted" />
			</jstl:when>
			<jstl:when test="${friendship.state == 'PENDING'}">
				<spring:message code="friendship.pending" />
			</jstl:when>
			<jstl:otherwise>
				<spring:message code="friendship.denied" />
			</jstl:otherwise>
		</jstl:choose>
	</display:column>

	<display:column>
		<jstl:if test="${actorId == friendship.receiver.id and friendship.state == 'PENDING'}">
			<a href="friendshipRequest/accept.do?requestId=${friendship.id}"><spring:message code="friendship.accept" /></a>
			<a href="friendshipRequest/deny.do?requestId=${friendship.id}"><spring:message code="friendship.deny" /></a>
		</jstl:if>
	</display:column>
</display:table>