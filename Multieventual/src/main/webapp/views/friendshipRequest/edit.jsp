<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form method="post" action="friendshipRequest/create.do" id="friendship" modelAttribute="friendship">
	
	<acme:textarea code="friendship.text" path="text"/>
	
	<acme:select items="${actors}" itemLabel="name" code="friendship.receiver" path="receiver"/>
	
	<acme:submit name="save" code="friendship.save" />
</form:form>

<acme:cancel code="cancel" url="friendshipRequest/list.do" />