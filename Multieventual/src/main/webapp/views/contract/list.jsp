<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table id="contract" sort="list" pagesize="5" name="${contracts}" requestURI="${requestUri}">
	<security:authorize access="hasRole('SPONSOR')">
		<spring:message code="contract.event" var="eventTitle" />
		<display:column title="${eventTitle}">
			<a href="event/profile.do?eventId=${contract.event.id}" ><jstl:out value="${contract.event.name}" /></a>
		</display:column>
	</security:authorize>
	
	<spring:message code="contract.state" var="stateTitle" />
	<display:column title="${stateTitle}" sortable="true">
		<jstl:choose>
			<jstl:when test="${contract.state == 'PENDING'}">
				<spring:message code="contract.pending" />
			</jstl:when>
			<jstl:when test="${contract.state == 'ACCEPTED'}">
				<spring:message code="contract.accepted" />
			</jstl:when>
			<jstl:when test="${contract.state == 'PAYMENT_PENDING'}">
				<spring:message code="contract.payment_pending" />
			</jstl:when>
			<jstl:otherwise>
				<spring:message code="contract.denied" />
			</jstl:otherwise>
		</jstl:choose>
	</display:column>
	
	<spring:message code="contract.term" var="termTitle" />
	<display:column property="termsAndConditions" title="${termTitle}" />

	<spring:message code="contract.offeredPayment" var="offeredPriceTitle" />
	<display:column property="offeredPayment" title="${offeredPriceTitle}" sortable="true"/>
		
	<security:authorize access="hasRole('USER')">
		<display:column>
			<jstl:if test="${contract.state == 'PENDING'}">
				<a href="contract/accept.do?contractId=${contract.id}"><spring:message code="contract.accept" /></a>
				<a href="contract/deny.do?contractId=${contract.id}"><spring:message code="contract.deny" /></a>
			</jstl:if>
		</display:column>
	</security:authorize>

</display:table>