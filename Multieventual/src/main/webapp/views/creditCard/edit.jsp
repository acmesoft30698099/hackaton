<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="creditCard/edit.do" method="post" modelAttribute="creditCard" >
	<acme:textbox path="holder" code="creditcard.holder" />
	<acme:textbox path="brand" code="creditcard.brand" />
	<acme:textbox path="number" code="creditcard.number" />
	<acme:textbox path="expirationMonth" code="creditcard.expirationMonth" />
	<acme:textbox path="expirationYear" code="creditcard.expirationYear" />
	<acme:textbox path="cvv" code="creditcard.cvv" />
	
	<acme:submit name="save" code="creditcard.save" />
</form:form>

<acme:cancel url="creditCard/profile.do" code="cancel" />