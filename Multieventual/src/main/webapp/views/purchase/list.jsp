<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="now" class="java.util.Date" />

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table requestURI="/purchase/list.do" id="purchase" sort="list" pagesize="5" name="${purchases}" >
	<spring:message code="purchase.event" var="eventTitle" />
	<display:column title="${eventTitle}">
		<a href="event/profile.do?eventId=${purchase.ticket.event.id}"><jstl:out value="${purchase.ticket.event.name}" /></a>
	</display:column>
	
	<spring:message code="purchase.ticket" var="ticketTitle" />
	<display:column title="${ticketTitle}">
		<a href="ticket/profile.do?ticketId=${purchase.ticket.id}"><jstl:out value="${purchase.ticket.name}" /></a>
	</display:column>
	
	<spring:message code="purchase.amount" var="amountTitle" />
	<display:column title="${amountTitle}">
		<jstl:out value="${purchase.ticket.price}" />
	</display:column>	
	
	<spring:message code="purchase.purchaseDate" var="dateTitle" />
	<display:column property="purchaseDate" title="${dateTitle}" format="{0,date,dd/MM/yyyy HH:mm}" sortable="true"/>
	
	<display:column>
		<jstl:if test="${purchase.ticket.event.startingDate > now}">
			<a href="purchase/delete.do?purchaseId=${purchase.id}"><spring:message code="purchase.delete" /></a>
		</jstl:if>
	</display:column>

</display:table>
