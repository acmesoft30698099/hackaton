<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<a href="category/create.do"><spring:message code="category.create" /></a>

<display:table pagesize="5" sort="list" requestURI="category/list.do" list="${categories}" id="category" >

	<spring:message code="category.name" var="nameTitle" />
	<display:column property="name" title="${nameTitle}" sortable="true" />
	
	<spring:message code="category.description" var="descriptionTitle" />
	<display:column property="description" title="${descriptionTitle}" />
	
	<display:column>
		<a href="category/edit.do?categoryId=${category.id}" ><spring:message code="category.edit" /></a>
	</display:column>

</display:table>