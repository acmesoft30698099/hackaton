<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="feedback/edit.do" method="post" modelAttribute="feedback">
	<form:hidden path="id" />
	<form:hidden path="event" />
	
	<acme:textbox code="feedback.score" path="score"/>
	<acme:textarea code="feedback.text" path="text"/>
		
	<acme:submit name="save" code="feedback.save"/>
	
	<jstl:if test="${feedback.id > 0}">
		<acme:submit name="delete" code="feedback.delete"/>
	</jstl:if>
</form:form>

<acme:cancel url="/feedback/list.do?eventId=${feedback.event.id}" code="cancel"/>