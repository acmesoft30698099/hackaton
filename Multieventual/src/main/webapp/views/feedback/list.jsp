<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>
 
<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:if test="${canCreate}">
	<a href="feedback/create.do?eventId=${eventId}"><spring:message code="feedback.create" /></a>
</jstl:if>

<display:table id="feedback" sort="list" pagesize="5" name="${feedbacks}" requestURI="/feedback/list.do">
	<spring:message code="feedback.score" var="scoreTitle" />
	<display:column property="score" title="${scoreTitle}" sortable="true" />
	
	<spring:message code="feedback.publicationDate" var="publicationDateTitle" />
	<display:column title="${publicationDateTitle}" property="publicationDate" format="{0,date,dd/MM/yyyy HH:mm}" sortable="true" />
	
	<spring:message code="feedback.text" var="textTitle" />
	<display:column property="text" title="${textTitle}" />
	
	<spring:message code="feedback.author" var="authorTitle" />
	<display:column title="${authorTitle}">
		<a href="actor/profile.do?actorId=${feedback.user.id}" ><jstl:out value="${feedback.user.name}" /></a>
	</display:column>
	
	<jstl:if test="${editableId == feedback.id}">
		<display:column>
			<a href="feedback/edit.do?feedbackId=${feedback.id}"><spring:message code="feedback.edit" /></a>
		</display:column>
	</jstl:if>

</display:table>

<acme:cancel code="cancel" url="event/profile.do?eventId=${eventId}" />